unit Path;

interface

uses CommonStuff, URecords, UConstants, UInterfaces, UDCLibrary, System.Generics.Collections,
     Math, Util, EntToPntArr, CommonTypes, Constants, PlnUtil;

procedure zProfileToArray;
procedure PathEntToPntArr (ent : entity; var resultPnts : tPaths);
procedure ChainPaths (var PathsToChain : tPaths);
function ApplyZProfile (var Pnts :  TArray<PointRec>) : TArray<PointRec>;
procedure Convert2dto3dLines (var PathsToConvert : tPaths);

implementation

procedure zProfileToArray;
var
  ent : entity;
  mode : mode_type;
  adr : lgl_addr;
  i : integer;  
  ArrSize : integer;
  StartX : double;
begin
  setlength (l^.zPflArr, 100);
  ArrSize := 0;

  MyModeInit (mode);
  mode_atr (mode, dhExtZProfl);
  adr := ent_first (mode);
  if not ent_get (ent, adr) then
    exit;

  l^.zPflArr := PlnToPntArray (ent, l^.Divs.Crc, l^.Divs.PthEqDivs);

  StartX := l^.zPflArr[0].x;
  for i := 0 to ArrSize-1 do
    l^.zPflArr[i].x := l^.zPflArr[i].x - StartX;

end;   // zProfileToArray

function PlnToPntRecArray (pln : entity; bulgedivs : integer; equaldivs : boolean;
                           var Visibility : TArray<boolean>;
                           ProcessMatrix : boolean = true;
                           pvertsVisOnly : boolean = false;
                           doVisibility : boolean = true) : TArray<PointRec>;
var
  pntArr : TArray<point>;
  i : integer;
begin
  pntArr := PlnToPntArray (pln, bulgedivs, equaldivs, Visibility, ProcessMatrix, pvertsVisOnly, doVisibility);
  setlength (result, length(pntArr));
  for i := 0 to length(result)-1 do begin
    result[i].p := pntArr[i];
    result[i].twist := 0;
  end;
  setlength (pntArr, 0);
end; //PlnToPntRecArray

procedure ReversePntRecs (var pnts : TArray<PointRec>);
var
  i : integer;
  d : double;
begin
  for i := 0 to (length(pnts)-2) div 2 do begin
    SwapPnt (pnts[i].p, pnts[length(pnts)-1-i].p);
    d := pnts[i].twist;
    pnts[i].twist := pnts[length(pnts)-1-i].twist;
    pnts[length(pnts)-1-i].twist := d;
  end;
end;

procedure PathEntToPntArr (ent : entity; var resultPnts : tPaths);
var
  templyr, svlyr : lgl_addr;
  tempent : entity;
  mode : mode_type;
  adr : lgl_addr;
  numpnts, divs, i, j, incr : integer;
  angincrement : double;
  Arr : EntPntArr;
  temppnts : TList<point>;
  cntr, pnt : point;
  rad, bang, eang, ang, prevang : double;
  mat, tempmat : modmat;
  d : double;
  len, len2 : integer;
  invmat : modmat;
  Points : tList<point>;
  last : boolean;


procedure AddPntToPnts (pnt : point; visible : boolean);   overload;
begin
  if len >= length (Arr.Pnts) then begin
    setlength (Arr.Pnts, len + 128);
    setlength (Arr.visible, len + 128);
  end;
  Arr.Pnts[len].p := pnt;
  Arr.visible[len] := visible;
  len := len+1;
end;
procedure AddPntToPnts (pnt : pointrec; visible : boolean);  overload;
begin
  if len >= length (Arr.Pnts) then begin
    setlength (Arr.Pnts, len + 128);
    setlength (Arr.visible, len + 128);
  end;
  Arr.Pnts[len] := pnt;
  Arr.visible[len] := visible;
  len := len+1;
end;

procedure AddPntToPnts2 (pnt : point);
begin
  if len2 = length(Arr.Pnts2) then begin
    setlength (Arr.Pnts2, len + 128);
  end;
  Arr.Pnts2[len2] := pnt;
  len2 := len2+1;
end;

function Calc3DAngIncrement (var bang, eang : double; entdivs : integer) : double;
var
  i : integer;
begin
  while eang < bang do
    eang := eang + TwoPi;

  if (l^.Divs.Ent3D >= 3) and (l^.Divs.Ent3D <= 128) then begin
    if l^.Divs.PthEqDivs then begin
      i := ceil((eang - bang)/TwoPi * l^.Divs.Ent3D);
      result := (eang - bang)/i;
    end
    else
      result := TwoPi / l^.Divs.Ent3D;
  end
  else if PGSv^.divover then
    result := TwoPi / PGSv^.circldiv1
  else
    result := TwoPi / entdivs;
end;

procedure AdjustBase;
begin
  if zmid in l^.zTreatment then
    Arr.zb := Arr.zb + (Arr.zh - Arr.zb)/2
  else if BasePlus in l^.zTreatment then
    Arr.zb := Arr.zb + l^.ZBasePlus;
end;

begin
  setlength (Arr.visible, 0);
  setlength (Arr.Pnts2, 0);
  setIdent (Arr.Matrix);
  Arr.zb := zero;
  Arr.zh := zero;
  if ent.enttype = entlin then begin
    {$region 'Process 2d line'}
    SetLength (Arr.Pnts, 2);
    Arr.Pnts[0].p := ent.linpt1;
    Arr.Pnts[1].p := ent.linpt2;
    SetLength (Arr.visible, 2);
    Arr.visible[0] := true;
    Arr.visible[1] := true;
    Arr.zb := min (ent.linpt1.z, ent.linpt2.z);
    Arr.zh := max (ent.linpt1.z, ent.linpt2.z);
    AdjustBase;
    Arr.enttyp := entlin;
    Arr.closed := false;
    resultPnts.Add(Arr);
    {$endregion 'Process 2d line'}
  end
  else if ent.enttype = entln3 then begin
    {$region 'Process 3d line'}
    if PointsEqual (ent.ln3pt1, ent.ln3pt2, true)  then
      exit;  // do not use lines with zero length
    if RealEqual (ent.ln3pt1.z, ent.ln3pt2.z) then begin
      setident (Arr.Matrix);
    end
    else begin
      ang := arctan2 (ent.ln3pt2.y - ent.ln3pt1.y, ent.ln3pt2.x - ent.ln3pt1.x);
      if RealEqual (ang, zero) or RealEqual (ang, pi) or RealEqual (ang, twopi) then begin
        setident (mat);
      end
      else begin
        setrotrel (mat, -ang, z, ent.ln3pt1);
        xformpt (ent.ln3pt2, mat, ent.ln3pt2);
      end;
      ang := arctan2 (ent.ln3pt2.z - ent.ln3pt1.z, ent.ln3pt2.x - ent.ln3pt1.x);
      if not RealEqual (ang, zero) or RealEqual (ang, pi) or RealEqual (ang, twopi) then begin
        catrotrel (mat, ang, y, ent.ln3pt1);
        setrotrel (tempmat, ang, y, ent.ln3pt1);
        xformpt (ent.ln3pt2, tempmat, ent.ln3pt2);
      end;
      invert (mat, Arr.Matrix, d);
    end;

    SetLength (Arr.Pnts, 2);
    Arr.Pnts[0].p := ent.ln3pt1;
    Arr.Pnts[1].p := ent.ln3pt2;
    SetLength (Arr.visible, 2);
    Arr.visible[0] := true;
    Arr.visible[1] := true;
    Arr.zb := min (Arr.Pnts[0].z, Arr.Pnts[1].z);
    Arr.zh := max (Arr.Pnts[0].z, Arr.Pnts[1].z);
    Arr.enttyp := entln3;
    Arr.closed := false;
    resultPnts.Add(Arr);
    {$endregion 'Process 3d line'}
  end
  else if ent.enttype in [entcrc, entell, entarc] then begin
    {$region 'Process crc, ell, arc'}
    Arr.enttyp := ent.enttype;
    case ent.enttype of
      entell : begin
                rad := ent.ellrad.x;
                bang := ent.ellbang;
                eang := ent.elleang;
                cntr := ent.ellcent;
                Arr.zb := ent.ellBase;
                Arr.zh := ent.ellHite;
               end;
      entcrc : begin
                rad := ent.crcrad;
                bang := ent.crcbang;
                eang := ent.crceang;
                cntr := ent.crccent;
                Arr.zb := ent.crcbase;
                Arr.zh := ent.crchite;
               end;
      else begin   //entarc
                rad := ent.arcrad;
                bang := ent.arcbang;
                eang := ent.arceang;
                cntr := ent.arccent;
                Arr.zb := ent.arcbase;
                Arr.zh := ent.archite;
               end;
    end;

    AdjustBase;

    points := TList<point>.Create;
    try
      ArcToPntList (cntr, rad, bang, eang, l^.Divs.Crc, l^.Divs.PthEqDivs, points);
      setlength (Arr.Pnts, points.Count);
      setlength (Arr.visible, points.Count);

      if ent.enttype = entell then begin
        setenlrel (mat, 1, ent.ellrad.y/ent.ellrad.x, 1, cntr);
        catrotrel (mat, ent.ellang, z, cntr);
        for i := 0 to points.Count-1 do begin
          xformpt (points[i], mat, Arr.Pnts[i].p);
          Arr.visible[i] := true;
        end;
      end
      else begin
        for i := 0 to Points.Count-1 do begin
          Arr.Pnts[i].p := Points[i];
          Arr.visible[i] := true;
        end;
      end;
    finally
      points.Free;
    end;

    Arr.closed := PointsEqual(Arr.Pnts[0].p, Arr.Pnts[length(Arr.Pnts)-1].p, true);
    resultPnts.Add(Arr);
    {$endregion 'Process crc, ell, arc'}
  end
  else if ent.enttype in [entar3, entcyl] then begin
    {$region 'Process 3D arc, cyl'}
    case ent.enttype of
      entar3 : begin
                bang := ent.ar3bang;
                eang := ent.ar3eang;
                rad  := ent.ar3rad;
                divs := ent.ar3div;
                setpoint (Arr.Cntr2, zero);
               end;
      else {entcyl :} begin
                bang := ent.cylbang;
                eang := ent.cyleang;
                rad  := ent.cylrad;
                divs := ent.cyldiv;
                Arr.Cntr2.x := zero;
                Arr.Cntr2.y := zero;
                Arr.Cntr2.z := ent.cyllen;
               end;
    end;

    angincrement := Calc3DAngIncrement (bang, eang, divs);

    Arr.zb := zero;
    Arr.zh := Arr.Cntr2.z;

    AdjustBase;

    setlength (Arr.Pnts, 0);
    setlength (Arr.visible, 0);
    len := 0;
    setlength (Arr.Pnts2, 0);
    len2 := 0;

    ang := bang;
    while ang <= eang do begin
      cylind_cart (rad, ang, 0, Pnt.x, Pnt.y, Pnt.z);
      AddPntToPnts(pnt, true);
      if ent.enttype = entcyl then begin
        pnt.z := ent.cyllen;
        AddPntToPnts2(pnt);
      end;
      last :=  RealEqual (ang, eang);
      ang := ang + angincrement;
      if RealEqual (ang, eang) then begin
        ang := eang;
      end
      else if (not last) and (ang > eang) and ((not l^.Divs.PthEqDivs) or (l^.Divs.Ent3D = 0)) then
        ang := eang;
    end;

    if (ent.enttype = entcyl) and ent.cylclose then begin
      if not PointsEqual (Arr.Pnts[0].p, Arr.Pnts[Len-1].p) then begin
        AddPntToPnts (Arr.Pnts[0].p, true);
        AddPntToPnts2 (Arr.Pnts2[0]);
      end;
      Arr.closed := true;
    end
    else
      Arr.closed := PointsEqual (Arr.Pnts[0].p, Arr.Pnts[Len-1].p);

    Arr.enttyp := ent.enttype;
    Arr.Matrix := ent.Matrix;
    setlength (Arr.Pnts, len);
    setlength (Arr.visible, len);
    setlength (Arr.Pnts2, len);
    resultPnts.Add(Arr);
    {$endregion 'Process 3D arc, cyl'}
  end
  else if ent.enttype = entpln then begin
    {$region 'Process polyline'}
    // note: visibility of side indicated by pv.info field (0=visible, 1 (or anything other than 0) = invisible
    len := 0;
    setlength (Arr.Pnts2, 0);

    Arr.zb := min (ent.plbase, ent.plnhite);
    Arr.zh := max (ent.plbase, ent.plnhite);
    AdjustBase;
    Arr.enttyp := entpln;
    Arr.Matrix := ent.Matrix;

    Arr.Pnts := PlnToPntRecArray (ent, l^.Divs.Crc, l^.Divs.PthEqDivs, Arr.visible, false, l^.zTreatment = [verticals]);

    if ent.plnclose then begin
      if not PointsEqual (Arr.Pnts[0].p, Arr.Pnts[length(Arr.Pnts)-1].p, false) then begin
        SetLength(Arr.Pnts, length(Arr.Pnts)+1);
        Arr.Pnts[length(Arr.Pnts)-1] := Arr.Pnts[0];
        setlength (Arr.visible, length(Arr.Pnts));
        Arr.visible[length(Arr.visible)-1] := Arr.visible[0];
      end;
      Arr.closed := true;
    end;

    if not ent.plnclose then
      Arr.closed := PointsEqual (Arr.Pnts[0].p, Arr.Pnts[length(Arr.Pnts)-1].p, false);



    ResultPnts.Add (Arr);
  {$endregion 'Process polyline'}
  end
  else if ent.enttype in [entply, entslb] then begin
    {$region 'Process polygon, slab'}
    // Note that the logic here assumes that corresponding fields in the ply & slb records are at the same offsets
    // This is certainly true at the moment, and it looks quite intentional so I expect it to remain a valid assumption.
    Arr.enttyp := ent.enttype;
    mat := GetHorizontalMat (ent.plypnt, ent.plynpnt);
    for i := 1 to ent.plynpnt do
      xformpt (ent.plypnt[i], mat, ent.plypnt[i]);   //move all points to horizontal plane before processing anything
    if ent.enttype = entslb then begin
      xformpt (ent.slbthick, mat, Arr.Thick);
      subpnt (Arr.Thick, ent.slbpnt[1], Arr.Thick);
    end
    else
      setpoint (Arr.Thick, 0);
    invert (mat, Arr.Matrix, d);

    Arr.zb := ent.plypnt[1].z;
    Arr.zh := Arr.zb + Arr.Thick.z;
    AdjustBase;

    setlength (Arr.Pnts, ent.plynpnt+1);
    setlength (Arr.visible, ent.plynpnt);

    for i := 1 to ent.plynpnt do begin
      Arr.Pnts[i-1].p := ent.plypnt[i];
      Arr.visible[i-1] := ent.plyisln[i];
    end;
    Arr.Pnts[ent.plynpnt].p := ent.plypnt[1];
    Arr.closed := true;

    resultPnts.Add(Arr);
    {$endregion 'Process polygon, slab'}
  end
  else if ent.enttype = entblk then begin
    {$region 'Process block'}
    for i := 1 to 3 do
      tempent.plypnt[i] := ent.blkpnt[i];  // put block points into pntarr so that we can call GetHorizontalMat
    mat := GetHorizontalMat (tempent.plypnt, 3);
    Invert (mat, Arr.Matrix, d);

    setlength (Arr.Pnts, 5);
    xformpt (ent.blkpnt[1], mat, Arr.Pnts[0].p);
    Arr.Pnts[4] := Arr.Pnts[0];
    Arr.closed := true;
    xformpt (ent.blkpnt[3], mat, Arr.Pnts[1].p);
    xformpt (ent.blkpnt[2], mat, Arr.Pnts[3].p);
    SubPnt (Arr.Pnts[1].p, Arr.Pnts[0].p, pnt);
    AddPnt (Arr.Pnts[3].p, pnt, Arr.Pnts[2].p);

    setlength (Arr.Pnts2, 5);
    xformpt (ent.blkpnt[4], mat, Arr.Pnts2[0]);
    SubPnt (Arr.Pnts2[0], Arr.Pnts[0].p, pnt);
    for i := 1 to 4 do begin
      AddPnt (Arr.Pnts[i].p, pnt, Arr.Pnts2[i]);
    end;
    Arr.zb := Arr.Pnts[0].z;
    Arr.zh := Arr.Pnts2[0].z ;
    AdjustBase;
    Arr.enttyp := entblk;
    setlength (Arr.visible, 5);
    for i := 0 to 4 do
      Arr.visible[i] := true;

    ResultPnts.Add (Arr);
    {$endregion 'Process block'}
  end
  else if ent.enttype = entcon then begin
    {$region 'Process entcon'}
    Arr.enttyp := entcon;
    angincrement := Calc3DAngIncrement (ent.conbang, ent.coneang, ent.condiv);

    setlength (Arr.Pnts, 0);
    setlength (Arr.visible, 0);
    len := 0;
    setlength (Arr.Pnts2, 0);
    len2 := 0;


    ang := ent.conbang;
    while ang <= ent.coneang do begin
      if RealEqual (ang, ent.coneang) then begin
        ang := ent.coneang;
      end;
      cylind_cart (ent.conrad, ang, 0, Pnt.x, Pnt.y, Pnt.z);
      AddPntToPnts(pnt, true);
      AddPntToPnts2(ent.concent);
      if ang < ent.coneang then begin
        ang := ang + angincrement;
        if RealEqual (ang, ent.coneang) or (ang > ent.coneang) then
          ang := ent.coneang;
      end
      else
        ang := ent.coneang + 1;
    end;

    if ent.conclose and not RealEqual (ent.conbang+TwoPi, ent.coneang) then begin
      AddPntToPnts (Arr.pnts[0], true);
      AddPntToPnts2 (ent.concent);
    end;

    Arr.closed := ent.conclose or RealEqual (ent.conbang+TwoPi, ent.coneang);
    Arr.Cntr2 := ent.concent;
    Arr.zb := 0;
    Arr.zh := ent.concent.z;

    if zmid in l^.zTreatment then begin
      for i := 0 to len-1 do begin
        meanpnt (Arr.Pnts[i].p, Arr.Pnts2[i], Arr.Pnts[i].p);
      end;
      Arr.zb := Arr.Pnts[0].z;
    end
    else if BasePlus in l^.zTreatment then begin
      d := l^.zbaseplus/ent.concent.z;
      for i := 0 to len-1 do begin
        Arr.Pnts[i].p.x := Arr.Pnts[i].p.x + d*(Arr.Pnts2[i].x -Arr.Pnts[i].p.x);
        Arr.Pnts[i].p.y := Arr.Pnts[i].p.y + d*(Arr.Pnts2[i].y -Arr.Pnts[i].p.y);
      end;
      Arr.zb := l^.zbaseplus;
    end;

    setlength (Arr.Pnts, len);
    setlength (Arr.Pnts2, len2);
    Arr.Matrix := ent.Matrix;
    resultPnts.Add(Arr);
    {$endregion 'Process entcon'}
  end
  else if ent.enttype = enttrn then begin
    {$region 'Process enttrn'}
    Arr.enttyp := enttrn;

    angincrement := Calc3DAngIncrement (ent.trnbang, ent.trneang, ent.trndiv);
    setlength (Arr.Pnts, 0);
    setlength (Arr.visible, 0);
    len := 0;
    setlength (Arr.Pnts2, 0);
    len2 := 0;

    ang := ent.trnbang;
    while ang <= ent.trneang do begin
      cylind_cart (ent.trnrad1, ang, 0, Pnt.x, Pnt.y, Pnt.z);
      AddPntToPnts(pnt, true);
      cylind_cart (ent.trnrad2, ang, ent.trncent.z, Pnt.x, Pnt.y, Pnt.z);
      Pnt.x := Pnt.x + ent.trncent.x;
      Pnt.y := Pnt.y + ent.trncent.y;
      AddPntToPnts2(pnt);
      PrevAng := ang;
      ang := ang + angincrement;
      if RealEqual (ang, ent.trneang) or ((ang > ent.trneang) and (prevang < ent.trneang)) then begin
        ang := ent.trneang;
      end

    end;

    if ent.trnclose and not RealEqual (ent.trnbang+TwoPi, ent.trneang) then begin
      AddPntToPnts (Arr.pnts[0], true);
      AddPntToPnts2 (Arr.pnts2[0]);
    end;

    Arr.closed := ent.trnclose or RealEqual (ent.trnbang+TwoPi, ent.trneang);
    Arr.Cntr2 := ent.trncent;
    Arr.zb := 0;
    Arr.zh := ent.trncent.z;

    if zmid in l^.zTreatment then begin
      for i := 0 to len-1 do begin
        meanpnt (Arr.Pnts[i].p, Arr.Pnts2[i], Arr.Pnts[i].p);
      end;
      Arr.zb := Arr.Pnts[0].z;
    end
    else if BasePlus in l^.zTreatment then begin
      d := l^.zbaseplus/ent.trncent.z;
      for i := 0 to len-1 do begin
        Arr.Pnts[i].x := Arr.Pnts[i].x + d*(Arr.Pnts2[i].x -Arr.Pnts[i].x);
        Arr.Pnts[i].y := Arr.Pnts[i].y + d*(Arr.Pnts2[i].y -Arr.Pnts[i].y);
      end;
      Arr.zb := l^.zbaseplus;
    end;


    setlength (Arr.Pnts, len);
    setlength (Arr.Pnts2, len2);
    Arr.Matrix := ent.Matrix;
    resultPnts.Add(Arr);
    {$endregion 'Process enttrn'}
  end
  else if ent.enttype = enttor then begin
    {$region 'Process enttor'}
    Arr.enttyp := enttor;

    angincrement := Calc3DAngIncrement (ent.torbang1, ent.torenag1, ent.tordiv1);
    setlength (Arr.Pnts, 0);
    setlength (Arr.visible, 0);
    len := 0;
    setlength (Arr.Pnts2, 0);
    len2 := 0;

    ang := ent.trnbang;
    while ang <= ent.trneang do begin
      cylind_cart (ent.trnrad1, ang, 0, Pnt.x, Pnt.y, Pnt.z);
      AddPntToPnts(pnt, true);
      cylind_cart (ent.trnrad2, ang, ent.trncent.z, Pnt.x, Pnt.y, Pnt.z);
      AddPntToPnts2(pnt);
      PrevAng := ang;
      ang := ang + angincrement;
      if RealEqual (ang, ent.trneang) or ((ang > ent.trneang) and (prevang < ent.trneang)) then begin
        ang := ent.trneang;
      end

    end;

    if ent.trnclose and not RealEqual (ent.trnbang+TwoPi, ent.trneang) then begin
      AddPntToPnts (Arr.pnts[0], true);
      AddPntToPnts2 (Arr.pnts2[0]);
    end;

    Arr.closed := ent.trnclose or RealEqual (ent.trnbang+TwoPi, ent.trneang);
    Arr.Cntr2 := ent.trncent;
    Arr.zb := 0;
    Arr.zh := ent.trncent.z;
    setlength (Arr.Pnts, len);
    setlength (Arr.Pnts2, len2);
    Arr.Matrix := ent.Matrix;
    resultPnts.Add(Arr);
    {$endregion 'Process enttor'}
  end
  else if ent.enttype in [entbez, entbsp] then begin
    {$region 'Process these entity types by exploding'}
    if GetBspBezPnts (ent, Arr.Pnts) then begin
      Arr.closed := PointsEqual(Arr.Pnts[0].p, Arr.Pnts[length(Arr.Pnts)-1].p);

      divs := l^.Divs.Bez;
      case divs of
        1 : incr := 2;
        2 : incr := 3;
        3 : incr := 4;
        4 : incr := 6;
        5 : incr := 8
        else incr := 1
      end;

      if incr <> 1 then begin
        setlength (Arr.Pnts2, length(Arr.Pnts));
        len2 := 0;
        i := 0;
        while i < length(Arr.Pnts) do begin
          AddPntToPnts2(Arr.Pnts[i].p);
          i := i+incr;
        end;
        if not PointsEqual (Arr.Pnts2[len2-1], Arr.Pnts[length(Arr.Pnts)-1].p) then
          AddPntToPnts2(Arr.Pnts[length(Arr.Pnts)-1].p);
        for i := 0 to len2-1 do
          Arr.Pnts[i].p := Arr.Pnts2[i];
        setlength (Arr.Pnts, len2);
      end;

     setlength (Arr.Pnts2, 0);
     setlength (Arr.visible, length(Arr.Pnts));
     for i := 0 to length(Arr.visible)-1 do
       Arr.visible[i] := true;
      Arr.enttyp := ent.enttype;
      case ent.enttype of
        entell : begin
            Arr.zb := ent.ellBase;
            Arr.zh := ent.ellHite;
            Arr.Cntr2 := ent.ellcent;   // this field is used to orient vertical extrusions
          end;
        entbez : begin
            Arr.zb := ent.bezbase;
            Arr.zh := ent.bezhite;
          end;
        entbsp : begin
            Arr.zb := ent.bspbase;
            Arr.zh := ent.bsphite;
          end;
        end;
    end;
    AdjustBase;
    resultPnts.Add(Arr);

    {$endregion 'Process these entity types by exploding'}
  end
  else if ent.enttype = enttxt then begin
    svlyr := getlyrcurr;
    lyr_init (templyr);
    lyr_set (templyr);
    try
      if ent_get (ent, ent.addr) then begin
        // explode entity to temp layer
        if ent_explode (ent, templyr, 0) then begin
          // process created entities
          mode_init (mode);
          mode_lyr (mode, lyr_curr);
          adr := ent_first (mode);
          while ent_get (tempent, adr) do begin
            if RealEqual (tempent.ln3pt1.z, ent.linpt1.z) and
               RealEqual (tempent.ln3pt2.z, ent.linpt1.z) then begin
              TempEnt.enttype := entlin;
              TempEnt.linpt1 := tempent.ln3pt1;
              TempEnt.linpt2.x := tempent.ln3pt2.x;
              TempEnt.linpt2.y := tempent.ln3pt2.y;
              TempEnt.linpt2.z := ent.linpt2.z;
              PathEntToPntArr (tempent, resultPnts);
            end;

            adr := ent_next (tempent, mode);
          end;
        end;
      end;
    finally
      lyr_set (svlyr);
      lyr_clear (templyr);
      lyr_term (templyr);
    end;
  end;

end; // PathEntToPntArr

procedure ChainPaths (var PathsToChain : tPaths);
var
  i, j, k, iLen, jLen : integer;
  Pntsi, Pntsj : EntPntArr;
  changemade : boolean;
begin
  repeat
    changemade := false;
    i := 0;
    while i < PathsToChain.Count-1 do begin     // chain together appropriate entity paths
      if (PathsToChain[i].enttyp in [entlin, entarc, entln3, entar3, entell, entbez, entbsp])
         and not PathsToChain[i].closed then begin
        Pntsi := PathsToChain[i];
        j := i+1;
        while j < PathsToChain.Count do begin
          Pntsi := PathsToChain[i];
          Pntsj := PathsToChain[j];
          if (not Pntsj.closed) and
             (((Pntsi.enttyp in [entlin, entarc, entell, entbez, entbsp]) and
               (Pntsj.enttyp in [entlin, entarc, entell, entbez, entbsp]) and
               (Pntsi.zb = Pntsj.zb) and
               (Pntsi.zh = Pntsj.zh)) (*or
              ((Pntsi.enttyp in [entln3, entar3]) and               // do not chain 3d entities for time being
               (Pntsj.enttyp in [entln3, entar3]))*)) then begin
            if PointsEqual (Pntsi.Pnts[length(Pntsi.Pnts)-1].p,
                           Pntsj.Pnts[0].p, 0.01,
                           not Pntsi.enttyp in [entlin, entarc]) then begin
              iLen := length (Pntsi.Pnts);
              jLen := length (Pntsj.Pnts);
              setlength (Pntsi.Pnts, iLen+jLen-1);
              setlength (Pntsi.visible, iLen+jLen-1);
              for k := 1 to jLen-1 do begin
                Pntsi.Pnts[iLen+k-1] := Pntsj.Pnts[k];
                Pntsi.visible[iLen+k-1] := true;   // will always be true as we are not chaining polygone or polylines
              end;
  (*            if PointsEqual (Pntsi.Pnts[0], Pntsi.Pnts[length(Pntsi.Pnts)-1], not PathsToChain[i].enttyp in [entlin, entarc]) then
                Pntsi.closed := true;  *)
              PathsToChain[i] := Pntsi;
              PathsToChain.Delete(j);
              changemade := true;
            end
            else if PointsEqual (Pntsi.Pnts[length(Pntsi.Pnts)-1].p,
                                Pntsj.Pnts[length(Pntsj.Pnts)-1].p, 0.01,
                                not Pntsi.enttyp in [entlin, entarc]) then begin
              iLen := length (Pntsi.Pnts);
              jLen := length (Pntsj.Pnts);
              setlength (Pntsi.Pnts, iLen+jLen-1);
              setlength (Pntsi.Visible, iLen+jLen-1);
              for k := 1 to jLen-1 do begin
                Pntsi.Pnts[iLen+k-1] := Pntsj.Pnts[jLen-k-1];
                Pntsi.visible[iLen+k-1] := true;      // will always be true as we are not chaining polygone or polylines
              end;
  (*            if PointsEqual (Pntsi.Pnts[0], Pntsi.Pnts[length(Pntsi.Pnts)-1], not PathsToChain[i].enttyp in [entlin, entarc]) then
                Pntsi.closed := true;      *)
              PathsToChain[i] := Pntsi;
              PathsToChain.Delete(j);
              changemade := true;
            end
            else if PointsEqual (Pntsi.Pnts[0].p,
                                Pntsj.Pnts[length(Pntsj.Pnts)-1].p,  0.01,
                                not Pntsi.enttyp in [entlin, entarc]) then begin
              iLen := length (Pntsi.Pnts);
              jLen := length (Pntsj.Pnts);
              setlength (Pntsj.Pnts, iLen+jLen-1);
              setlength (Pntsj.visible, iLen+jLen-1);
              for k := 1 to iLen-1 do begin
                Pntsj.Pnts[jLen+k-1] := Pntsi.Pnts[k];
                Pntsj.visible[jLen+k-1] := true;  // will always be true as we are not chaining polygone or polylines
              end;
  (*            if PointsEqual (Pntsj.Pnts[0], Pntsj.Pnts[length(Pntsj.Pnts)-1], not PathsToChain[i].enttyp in [entlin, entarc]) then
                Pntsj.closed := true;    *)
              PathsToChain[i] := Pntsj;
              PathsToChain.Delete(j);
              changemade := true;
            end
            else if PointsEqual (Pntsi.Pnts[0].p,
                                Pntsj.Pnts[0].p,  0.01,
                                not Pntsi.enttyp in [entlin, entarc]) then begin
              iLen := length (Pntsi.Pnts);
              jLen := length (Pntsj.Pnts);
              ReversePntRecs(Pntsi.Pnts);
              setlength (Pntsi.Pnts, iLen+jLen-1);
              setlength (Pntsi.visible, iLen+jLen-1);
              for k := 1 to jLen-1 do begin
                Pntsi.Pnts[iLen+k-1] := Pntsj.Pnts[k];
                Pntsi.visible[iLen+k-1] := true;   // will always be true as we are not chaining polygone or polylines
              end;
  (*            if PointsEqual (Pntsi.Pnts[0], Pntsi.Pnts[length(Pntsi.Pnts)-1], not PathsToChain[i].enttyp in [entlin, entarc]) then
                Pntsi.closed := true;   *)
              PathsToChain[i] := Pntsi;
              PathsToChain.Delete(j);
              changemade := true;
            end
            else
              j := j+1;
          end
          else
            j := j+1;
                       
        end;  //while j < PathsToChain.Count

      end;
      i := i+1;
    end;
  until not changemade;
  for i := 0 to PathsToChain.Count-1 do begin
    Pntsi := PathsToChain[i];
    Pntsi.closed := PointsEqual (Pntsi.Pnts[0].p, Pntsi.Pnts[length(Pntsi.Pnts)-1].p, 0.01,
                                 not Pntsi.enttyp in [entlin, entarc]);
    PathsToChain[i] := Pntsi;
  end;
end;  // ChainPaths

function ApplyZProfile (var Pnts :  TArray<PointRec>) : TArray<PointRec>;
var
  PflPnts : TArray<point>;
  PathLen, ProfileLen, RunningLen, dis : double;
  LenRatio : double;
  i, NextPflNdx, NextResultNdx : integer;
begin
  {$region 'Set up (stretch or squeeze) PflPnts to same length as path'}
  PathLen := PathLength(Pnts);
  ProfileLen := l^.zPflArr[length(l^.zPflArr)-1].x;  // setup of PflArr sets first element x to zero, so x len of last element = x length
  LenRatio := PathLen/ProfileLen;
  PflPnts := Copy (l^.zPflArr);
  for i := 0 to length(PflPnts) do begin
    PflPnts[i].x := PflPnts[i].x * LenRatio;
  end;
  {$endregion 'Set up (stretch or squeeze) PflPnts to same length as path'}

  RunningLen := 0;
  setlength (result, length(Pnts) + length(PflPnts));
  result[0].p := NewPoint (Pnts[0].p, PflPnts[0].y);
  NextPflNdx := 1;
  NextResultNdx := 1;
  for i := 1 to length(Pnts)-1 do begin
    dis := Distance(Pnts[i-1].p, Pnts[i].p);
    RunningLen := RunningLen + dis;
    while PflPnts[NextPflNdx].x < RunningLen do begin
      result[NextResultNdx].x := Pnts[i].x - (Pnts[i].x - Pnts[i-1].x) *
                                        (RunningLen - PflPnts[NextPflNdx].x)/dis;
      result[NextResultNdx].y := Pnts[i].y - (Pnts[i].y - Pnts[i-1].y) *
                                        (RunningLen - PflPnts[NextPflNdx].x)/dis;
      result[NextResultNdx].z := PflPnts[NextPflNdx].y;
      NextResultNdx := NextResultNdx+1;
      NextPflNdx := NextPflNdx+1;
    end;
    if not PointsEqual (result[NextResultNdx-1].p, Pnts[i].p, true) then begin
      result[NextResultNdx].x := Pnts[i].x;
      result[NextResultNdx].y := Pnts[i].y;
      if i = (length(Pnts)-1) then
        result[NextResultNdx].z := PflPnts[length(PflPnts)-1].y
      else begin
        result[NextResultNdx].z :=
          PflPnts[NextPflNdx].y -
            (PflPnts[NextPflNdx].y - PflPnts[NextPflNdx-1].y) *
              (PflPnts[NextPflNdx].x - RunningLen)/(PflPnts[NextPflNdx].x - PflPnts[NextPflNdx-1].x);
      end;
      NextResultNdx := NextResultNdx + 1;
    end;
  end; // for i := 1 to length(Pnts)-1
  setlength (result, NextResultNdx);
end;

procedure Convert2dto3dLines (var PathsToConvert : tPaths);
var
  rec, newrec : EntPntArr;
  OrigLen, i, j : integer;
  len, runninglen, sloperatio : double;
begin
//  SlopeRatio := 1;
  OrigLen := PathsToConvert.Count;
  for i := 0 to OrigLen-1 do begin
    if PathsToConvert[i].enttyp in [entlin, entarc, entcrc] then begin
      rec := PathsToConvert[i];
      if base in l^.zTreatment then begin
        {$region 'base in l^.zTreatment'}
        {$region 'set z to base'}
        for j := 0 to length (rec.Pnts)-1 do
          rec.Pnts[j].z := rec.zb;
        rec.enttyp := entln3;
        {$endregion 'set z to base'}
        if verticals in l^.zTreatment then begin
          {$region 'verticals in l^.zTreatment'}
          {$region 'add verticals at start and end of base (in newrec)'}
          setlength(newrec.Pnts, length(rec.Pnts)+2);
          newrec.Pnts[0] := rec.Pnts[0];
          newrec.Pnts[0].z := rec.zh;
          for j := 1 to length(rec.Pnts) do
            newrec.Pnts[j] := rec.Pnts[j-1];
          newrec.Pnts[length(newrec.Pnts)-1] := rec.Pnts[length(Rec.Pnts)-1];
          newrec.Pnts[length(newrec.Pnts)-1].z := rec.zh;
          newrec.enttyp := entln3;
          newrec.closed := false;
          {$endregion 'add verticals at start and end of base (in newrec)'}
          if hite in l^.zTreatment then begin
            {$region 'add top edge (hite) to newrec'}
            setlength (newrec.Pnts, 2*length(rec.Pnts)+1);
            for j := length(rec.Pnts)-2 downto 0 do begin
              newrec.Pnts[length(newrec.Pnts) - j - 1] := rec.Pnts[j];
              newrec.Pnts[length(newrec.Pnts) - j - 1].z := rec.zh;             
            end;
            newrec.closed := true;
            {$endregion 'add top edge (hite) to newrec'}
          end;
          PathsToConvert[i] := newrec;
          {$endregion 'verticals in l^.zTreatment'}          
        end
        else begin
          PathsToConvert[i] := rec;
          if hite in l^.zTreatment then begin
            {$region 'Add hite line as separate entry in PathsToConvert'}          
            setlength (newrec.Pnts, length(rec.Pnts));
            for j := 0 to length(rec.Pnts)-1 do begin
              newrec.Pnts[j].x := rec.Pnts[j].x;
              newrec.Pnts[j].y := rec.Pnts[j].y;
              newrec.Pnts[j].z := rec.zh;
            end; 
            PathsToConvert.Add (newrec);                    
            {$endregion 'Add hite line as separate entry in PathsToConvert'}          
          end;
          
        end;          
        {$endregion 'base in l^.zTreatment'}
      end
      else if hite in l^.zTreatment then begin
        {$region 'hite in l^.zTreatment'}
        for j := 0 to length (rec.Pnts)-1 do
          rec.Pnts[j].z := rec.zh;
        rec.enttyp := entln3;
        if verticals in l^.zTreatment then begin
          {$region 'hite+verticals in l^.zTreatment'}
          setlength(newrec.Pnts, length(rec.Pnts)+2);
          newrec.Pnts[0].p := NewPoint (rec.Pnts[0].p.x, rec.Pnts[0].p.y, rec.zb);
          for j := 0 to length(rec.Pnts)-1 do begin
            newrec.Pnts[j+1] := rec.Pnts[j]
          end;
          newrec.Pnts[length(newrec.Pnts)-1].p := NewPoint(rec.Pnts[length(rec.Pnts)-1].p,
                                                         rec.zb);
          PathsToConvert[i] := newrec;
          {$endregion 'hite+verticals in l^.zTreatment'}
        end
        else
          PathsToConvert[i] := rec;
        {$endregion 'hite in l^.zTreatment'}
      end
      else if verticals in l^.zTreatment then begin
        {$region 'verticals in l^.zTreatment'}
        setlength (newrec.Pnts, 2);
        newrec.Pnts[0].p := NewPoint (rec.Pnts[length(rec.Pnts)-1].p, rec.zb);
        newrec.Pnts[1].p := NewPoint (newrec.Pnts[0].p, rec.zh);
        newrec.enttyp := entln3; 
        PathsToConvert.Add (newrec);
        setlength (rec.Pnts, 2);
        rec.Pnts[0].z := rec.zb;
        rec.Pnts[1].p := NewPoint (rec.Pnts[0].p, rec.zh);
        rec.enttyp := entln3;
        PathsToConvert[i] := rec;
        {$endregion 'verticals in l^.zTreatment'}
      end
      else if [Slope, RevSlope] * l^.zTreatment <> [] then begin
        {$region 'slope or revslope in l^.zTreatment'}
        len := PathLength (rec.Pnts);
        sloperatio := abs (rec.zh - rec.zb)/len;
        if revSlope in l^.zTreatment then begin
          rec.Pnts[0].z := rec.zh;
          sloperatio := -sloperatio;
        end
        else {slope in l^.zTreatment}
          rec.Pnts[0].z := rec.zb;
        runninglen := 0;
        for j := 1 to length(Rec.Pnts)-1 do begin
          runninglen := runninglen + distance (rec.Pnts[j-1].p, rec.Pnts[j].p);
          rec.Pnts[j].z := rec.Pnts[0].z + runninglen * sloperatio;
        end;
        rec.enttyp := entln3;
        PathsToConvert[i] := rec;
        {$endregion 'slope or revslope in l^.zTreatment'}
      end
      else if zProfile in l^.zTreatment then begin
        {$region 'ZProfile'}
        rec.Pnts := ApplyZProfile (rec.Pnts);
        rec.enttyp := entln3;
        PathsToConvert[i] := rec;
        {$endregion 'ZProfile'}
      end;
    end;  // if rec.enttyp in [entlin, entarc, entcrc]
  end;

end;


end.
