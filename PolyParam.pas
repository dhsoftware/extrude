unit PolyParam;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Samples.Spin, Vcl.StdCtrls, DcadEdit, System.Math,
  Vcl.ExtCtrls, UInterfaces, URecords, UConstants, System.UITypes, Settings;

type
  TFmPolyParam = class(TForm)
    rbCircle: TRadioButton;
    rbPolygon: TRadioButton;
    rbRectangle: TRadioButton;
    Label1: TLabel;
    dceCircRad: TDcadEdit;
    Label2: TLabel;
    dceDiam: TDcadEdit;
    Label3: TLabel;
    dcePlyRadius: TDcadEdit;
    Label4: TLabel;
    seNumSides: TSpinEdit;
    dceSideLen: TDcadEdit;
    Label5: TLabel;
    Label6: TLabel;
    dceWidth: TDcadEdit;
    Label7: TLabel;
    dceHeight: TDcadEdit;
    btnOK: TButton;
    btnCancel: TButton;
    lblShape: TLabel;
    rgPortion: TRadioGroup;
    Image1: TImage;
    procedure dceCircRadChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dceDiamChange(Sender: TObject);
    procedure seNumSidesChange(Sender: TObject);
    procedure dcePlyRadiusChange(Sender: TObject);
    procedure dceSideLenChange(Sender: TObject);
    procedure rbCircleClick(Sender: TObject);
    procedure rbPolygonClick(Sender: TObject);
    procedure rbRectangleClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure rgPortionClick(Sender: TObject);
    procedure dceWidthChange(Sender: TObject);
    procedure dceHeightChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PolyParamClick(Sender: TObject);
    procedure CircParamClick(Sender: TObject);
    procedure rectParamClick(Sender: TObject);
  private
    { Private declarations }
    firstfieldentry : boolean;
    ignoreChange : boolean;
    ImageCntrX, ImageCntrY, ImageRadius : integer;
    procedure DrawCircle;
    procedure DrawRectangle;
    procedure DrawPolygon;
    procedure DrawProfile;
  public
    { Public declarations }
  end;

var
  FmPolyParam : TFmPolyParam;

implementation

{$R *.dfm}

procedure TFmPolyParam.btnOKClick(Sender: TObject);
begin
  if rbCircle.Checked and (dceCircRad.NumValue = 0) then
    MessageDlg('Circle Radius cannot be zero', mtWarning, [mbOK], 0)
  else if rbPolygon.Checked and (dcePlyRadius.NumValue = 0) then
    MessageDlg('Polygon Radius cannot be zero', mtWarning, [mbOK], 0)
  else if rbRectangle.Checked and ((dceWidth.NumValue = 0) or (dceHeight.NumValue = 0)) then
    MessageDlg('Rectangle Width and Height must both be non-zero', mtWarning, [mbOK], 0)
  else
    ModalResult := mrOk;
end;

procedure TFmPolyParam.dceCircRadChange(Sender: TObject);
begin
  if not IgnoreChange then begin
    IgnoreChange := true;
    dceDiam.NumValue := dceCircRad.NumValue * 2;
    IgnoreChange := false;
    DrawProfile;
  end;
end;

procedure TFmPolyParam.CircParamClick(Sender: TObject);
begin
  if firstfieldentry then begin
    if rbPolygon.checked then
      dcePlyRadius.SetFocus
    else if rbRectangle.checked then
      dceWidth.SetFocus
    else
      rbCircle.Checked := true;
    firstfieldentry := false;
  end
  else
    rbCircle.Checked := true;
end;

procedure TFmPolyParam.dceDiamChange(Sender: TObject);
begin
  if not IgnoreChange then begin
    IgnoreChange := true;
    dceCircRad.NumValue := dceDiam.NumValue / 2;
    IgnoreChange := false;
    DrawProfile;
  end;
end;

procedure TFmPolyParam.dceHeightChange(Sender: TObject);
begin
  DrawProfile;
end;

procedure TFmPolyParam.dcePlyRadiusChange(Sender: TObject);
var
  svText : string;
begin
  if not ignoreChange then begin
    ignoreChange := true;
    svText := dcePlyRadius.Text;
    dceSideLen.NumValue := 2 * dcePlyRadius.NumValue * sin ( Pi / seNumSides.Value);
    dcePlyRadius.Text := svText;
    ignoreChange := false;
    DrawProfile;
  end;
end;

procedure TFmPolyParam.dceSideLenChange(Sender: TObject);
begin
  if not ignoreChange then begin
    ignoreChange := true;
    dcePlyRadius.NumValue := (1/2) * dceSideLen.NumValue * Cosecant(Pi/seNumSides.Value);
    ignoreChange := false;
    DrawProfile;
  end;
end;

procedure TFmPolyParam.dceWidthChange(Sender: TObject);
begin
    DrawProfile;
end;

procedure TFmPolyParam.FormCreate(Sender: TObject);
begin
  firstfieldentry := true;
  IgnoreChange := false;
  Image1.Canvas.Pen.Width := round (Image1.Width/200 + 0.5);
  ImageCntrX := Image1.Width div 2;
  ImageCntrY := Image1.Height div 2;
  ImageRadius := round(Image1.Width * 0.45);
  seNumSidesChange (Sender);
  SetFormPos (TForm(self));
end;

procedure TFmPolyParam.FormResize(Sender: TObject);
begin
  Image1.Picture.Bitmap.Width := Image1.Width;
  Image1.Picture.Bitmap.Height := Image1.Height;
  Image1.Canvas.Refresh;
  ImageCntrX := Image1.Width div 2;
  ImageCntrY := Image1.Height div 2;
  ImageRadius := round(min (Image1.Height, Image1.Width) * 0.45);
  DrawProfile;
end;

procedure TFmPolyParam.DrawCircle;
begin
  with Image1 do begin
    case rgPortion.ItemIndex of
      0 : Canvas.Ellipse(ImageCntrX-ImageRadius, ImageCntrY-ImageRadius,
                         ImageCntrX+ImageRadius, ImageCntrY+ImageRadius);
      1 : Canvas.Arc (ImageCntrX-ImageRadius, ImageCntrY-ImageRadius,
                      ImageCntrX+ImageRadius, ImageCntrY+ImageRadius,
                      ImageCntrX+ImageRadius, ImageCntrY,
                      ImageCntrX-ImageRadius, ImageCntrY);
      2 : Canvas.Arc (ImageCntrX-ImageRadius, ImageCntrY-ImageRadius,
                      ImageCntrX+ImageRadius, ImageCntrY+ImageRadius,
                      ImageCntrX-ImageRadius, ImageCntrY,
                      ImageCntrX+ImageRadius, ImageCntrY);
      3 : Canvas.Arc (ImageCntrX-ImageRadius, ImageCntrY-ImageRadius,
                      ImageCntrX+ImageRadius, ImageCntrY+ImageRadius,
                      ImageCntrX, ImageCntrY-ImageRadius,
                      ImageCntrX, ImageCntrY+ImageRadius);
      4 : Canvas.Arc (ImageCntrX-ImageRadius, ImageCntrY-ImageRadius,
                      ImageCntrX+ImageRadius, ImageCntrY+ImageRadius,
                      ImageCntrX, ImageCntrY+ImageRadius,
                      ImageCntrX, ImageCntrY-ImageRadius);
    end;
  end;
end;

procedure TFmPolyParam.DrawRectangle;
var
  ratio : double;
  w,h : integer;
begin
  ratio := dceWidth.NumValue / dceHeight.NumValue;
  if ratio < 1 then begin
    h := ImageRadius * 2;
    w := round (h * ratio);
  end
  else if ratio > 1 then begin
    w := ImageRadius * 2;
    h := round (w / ratio);
  end
  else begin
    w := ImageRadius * 2;
    h := w;
  end;

  with Image1 do begin
    Canvas.Rectangle (ImageCntrX - (w div 2), ImageCntrY - (h div 2),
                      ImageCntrX - (w div 2) + w, ImageCntrY - (h div 2) + h);
    Canvas.Pen.Color := clWhite;
    case rgPortion.ItemIndex of
      1 : Canvas.Rectangle (0, ImageCntrY, Width-1, Height-1);
      2 : Canvas.Rectangle (0, ImageCntrY, Width-1, 0);
      3 : Canvas.Rectangle (width-1, 0, ImageCntrX, Height-1);
      4 : Canvas.Rectangle (0, 0, ImageCntrX, Height-1);
    end;
  end;
end;

procedure TFmPolyParam.DrawPolygon;
var
  ang : double;
  StartPnt, Pnt : point;
  i : integer;
begin
  with Image1 do begin
    ang := HalfPi - Pi/seNumSides.Value;
    cylind_cart (ImageRadius, ang, 0, StartPnt.x, StartPnt.y, StartPnt.z);
    StartPnt.x := StartPnt.x + Width div 2;
    StartPnt.y := StartPnt.y + Height div 2;
    Canvas.MoveTo(round(StartPnt.x), round(StartPnt.y));
    for i := 1 to seNumSides.Value - 1 do begin
      ang := ang + TwoPi/seNumSides.Value;
      cylind_cart (ImageRadius, ang, 0, Pnt.x, Pnt.y, Pnt.z);
      Pnt.x := Pnt.x + Width div 2;
      Pnt.y := Pnt.y + Height div 2;
      Canvas.LineTo (round(Pnt.x), round(Pnt.y));
    end;
    Canvas.LineTo (round(StartPnt.x), round(StartPnt.y));

    Canvas.Pen.Color := clWhite;
    case rgPortion.ItemIndex of
      1 : Canvas.Rectangle (0, ImageCntrY, Width-1, Height-1);
      2 : Canvas.Rectangle (0, ImageCntrY, Width-1, 0);
      3 : Canvas.Rectangle (width-1, 0, ImageCntrX, Height-1);
      4 : Canvas.Rectangle (0, 0, ImageCntrX, Height-1);
    end;
  end;
end;

procedure TFmPolyParam.DrawProfile;
begin
  with Image1 do begin
    Canvas.Pen.Color := clWhite;
    Canvas.Brush.Color := clWhite;
    Canvas.Rectangle(0,0, Width-1, height-1);
    Canvas.Pen.Color := clBlack;

    if rbCircle.Checked and (dceCircRad.NumValue > 0) then
      DrawCircle
    else if rbRectangle.Checked and (dceWidth.NumValue > 0) and (dceHeight.NumValue > 0) then
      DrawRectangle
    else if rbPolygon.Checked and (dcePlyRadius.NumValue > 0) then
      DrawPolygon;


    Canvas.Pen.Color := clRed;
    Canvas.MoveTo (ImageCntrX-Canvas.Pen.Width*2, ImageCntrY-Canvas.Pen.Width*2);
    Canvas.LineTo (ImageCntrX+Canvas.Pen.Width*2, ImageCntrY+Canvas.Pen.Width*2);
    Canvas.MoveTo (ImageCntrX-Canvas.Pen.Width*2, ImageCntrY+Canvas.Pen.Width*2);
    Canvas.LineTo (ImageCntrX+Canvas.Pen.Width*2, ImageCntrY-Canvas.Pen.Width*2);

  end;

end;

procedure TFmPolyParam.rbCircleClick(Sender: TObject);
begin
{  dceCircRad.Enabled := true;
  dceDiam.Enabled := true;
  dcePlyRadius.Enabled := false;
  seNumSides.Enabled := false;
  dceSideLen.Enabled := false;
  dceWidth.Enabled := false;
  dceHeight.Enabled := false; }
  dceCircRad.TabStop := true;
  seNumSides.TabStop := false;
  dcePlyRadius.TabStop := false;
  dceWidth.TabStop := false;
  dceHeight.TabStop := false;

  DrawProfile;
end;

procedure TFmPolyParam.rbPolygonClick(Sender: TObject);
begin
{  dceCircRad.Enabled := false;
  dceDiam.Enabled := false;
  dcePlyRadius.Enabled := True;
  seNumSides.Enabled := true;
  dceSideLen.Enabled := true;
  dceWidth.Enabled := false;
  dceHeight.Enabled := false; }
  dceCircRad.TabStop := false;
  seNumSides.TabStop := true;
  dcePlyRadius.TabStop := true;
  dceWidth.TabStop := false;
  dceHeight.TabStop := false;
  DrawProfile;
end;

procedure TFmPolyParam.rbRectangleClick(Sender: TObject);
begin
{  dceCircRad.Enabled := false;
  dceDiam.Enabled := false;
  dcePlyRadius.Enabled := false;
  seNumSides.Enabled := false;
  dceSideLen.Enabled := false;
  dceWidth.Enabled := true;
  dceHeight.Enabled := true;  }
  dceCircRad.TabStop := false;
  seNumSides.TabStop := false;
  dcePlyRadius.TabStop := false;
  dceWidth.TabStop := true;
  dceHeight.TabStop := true;
  DrawProfile;
end;


procedure TFmPolyParam.rectParamClick(Sender: TObject);
begin
  rbRectangle.Checked := true;
end;

procedure TFmPolyParam.rgPortionClick(Sender: TObject);
begin
  DrawProfile;
end;

procedure TFmPolyParam.seNumSidesChange(Sender: TObject);
begin
  ignoreChange := true;
  //dceSideLen.NumValue := 2 * dcePlyRadius.NumValue * sin ( 2 * Pi / seNumSides.Value);
  //dcePlyRadius.NumValue := (1/2) * dceSideLen.NumValue * Cosecant(Pi/seNumSides.Value);
  dceSideLen.NumValue := 2 * dcePlyRadius.NumValue * sin ( Pi / seNumSides.Value);
  ignoreChange := false;
  case seNumSides.Value of
    3 : lblShape.Caption := '(Triangle)';
    4 : lblShape.Caption := '(Square)';
    5 : lblShape.Caption := '(Pentagon)';
    6 : lblShape.Caption := '(Hexagon)';
    7 : lblShape.Caption := '(Heptagon)';
    8 : lblShape.Caption := '(Octagon)';
    9 : lblShape.Caption := '(Nonagon)';
    10 : lblShape.Caption := '(Decagon)';
    11 : lblShape.Caption := '(Undecagon)';
    12 : lblShape.Caption := '(Dodecagon)';
    13 : lblShape.Caption := '(Tridecagon)';
    14 : lblShape.Caption := '(Tetradecagon)';
    15 : lblShape.Caption := '(Pentadecagon)';
    16 : lblShape.Caption := '(Hexakaidecagon)';
    17 : lblShape.Caption := '(Heptadecagon)';
    18 : lblShape.Caption := '(Octakaidecagon)';
    19 : lblShape.Caption := '(Enneadecagon)';
    20 : lblShape.Caption := '(Icosagon)'
    else lblShape.Caption := '';
  end;
  DrawProfile;
end;

procedure TFmPolyParam.PolyParamClick(Sender: TObject);
begin
  rbPolygon.Checked := true;
end;



end.
