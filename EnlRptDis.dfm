object fmEnlRptDis: TfmEnlRptDis
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Repeat Distance'
  ClientHeight = 80
  ClientWidth = 126
  Color = clBtnFace
  Constraints.MaxHeight = 119
  Constraints.MinHeight = 119
  Constraints.MinWidth = 140
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    126
    80)
  PixelsPerInch = 96
  TextHeight = 13
  object dceRptDis: TDcadEdit
    Left = 5
    Top = 5
    Width = 118
    Height = 21
    Hint = 'Length of each repeat'
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
  end
  object Button1: TButton
    Left = 5
    Top = 28
    Width = 118
    Height = 18
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Reset to Pfl Width'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 56
    Top = 52
    Width = 67
    Height = 25
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Button3: TButton
    Left = 5
    Top = 52
    Width = 45
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
