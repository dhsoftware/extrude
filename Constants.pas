unit Constants;

interface

const
  Ver = 'Extrude Macro v1.2.0.8';
  Copyright = '  (c)David Henderson 2023';

  // attribute names
  dhExtProfile = 'dhExtProfile';
  dhExtPflEqDv = 'dhExtPflEqDv';
  dhExtPflDivs = 'dhExtPflDivs';
  dhExtPflBDiv = 'dhExtPflBDiv';
  dhExtEqDivs = 'dhExtEqDivs';
  dhExtCrcDivs = 'dhExtCrcDivs';
  dhExtBezDivs = 'dhExtBezDivs';
  dhExt3DDivs = 'dhExt3DDivs';
  dhExtEnlEqDv = 'dhExtEnlEqDv';
  dhExtEnlDivs = 'dhExtEnlDivs';
  dhExtProfSel = 'dhExtProfSel';
  dhExtPathSel = 'dhExtPathSel';
  dhExtSymPath = 'dhExtSymPath';
  dhExtProfTyp = 'dhExtProfTyp';
  dhExtCircRad = 'dhExtCircRad';
  dhExtPlyRad = 'dhExtPlyRad';
  dhExtProfMod = 'dhExtProfMod';
  dhExtPlyNSid = 'dhExtPlyNSid';
  dhExtRWidth = 'dhExtRWidth';
  dhExtRHeight ='dhExtRHeight';
  dhExtProfEnt = 'dhExtProfEnt';
  dhExtProfSym = 'dhExtProfSym';
  dhExtRefPnt = 'dhExtRefPnt';
  dhExtPortion = 'dhExtPortion';
  dhExtShwOptn = 'dhExtShwOptn';
  dhExtEnlStrt = 'dhExtEnlStrt';
  dhExtEnlEnd = 'dhExtEnlEnd';
  dhExtEnlProf = 'dhExtEnlProf';
  dhExtEnlRpt = 'dhExtEnlRpt';
  dhExtEnlXY = 'dhExtEnlXY';
  dhExtZTrtmt = 'dhExtZTrtmt';
  dhExtZProfl = 'dhExtZProfl';
  dhExtShelTp = 'dhExtShelTp';
  dhExtShelTk = 'dhExtShelTk';
  dhExtCapEnd = 'dhExtCapEnd';
  dhExtVertEnd = 'dhExtVertEnd';
  dhExtTwistTy = 'dhExtTwistTy';
  dhExtTwistNu = 'dhExtTwistNu';
  dhExtTwistDi = 'dhExtTwistDi';
  dhExtTwistDv = 'dhExtTwistDv';
  dhExttrZtype = 'dhExttrZtype';
  dhExtTraceZ = 'dhExtTraceZ';
  dhExtSPflWdw = 'dhExtSPflWdw';
  dhExtEPflWdw = 'dhExtEPflWdw';
  ExtRegPrompt = 'ExtRegPrompt';
  dhZBsPlusDis = 'dhZBsPlusDis';
  dhEnlPRptDis = 'dhEnlPRptDis';
  dhExtClkRot = 'dhExtClkRot';
  dhExtStrtRot = 'dhExtStrtRot';
  dhExtLicVer = 'dhExtLicVer';
  dhExtFltRad = 'dhExtFltRad';
  dhExtOfByPnt = 'dhExtOfByPnt';

  // states
  _Exiting = 0;
  _Profile = 2;
  _Profile2 = 3;
  _ProfileRef = 4;
  _Path = 5;
  _Path2 = 6;
  _Z = 7;
  _ZProfile = 8;
  _ProfDivs = 10;
  _PathDivs = 11;
  _Enl = 12;
  _EnlStart = 13;
  _EnlEnd = 14;
  _EnlBoth = 15;
  _EnlProfile = 16;
  _EnlProfileRef = 17;
  _ExtrThick = 18;
  _Twist = 19;
  _TwistNum = 20;
  _TwistDis = 21;
  _TwistDivs = 22;
  _Trace = 23;
  _Trace2 = 24;
  _TraceZ = 25;
  _User1 = 26;
  _User2 = 27;
  _zBasePlus = 28;
  _StartRot = 29;
  _StartRotPfl = 30;
  _OffsetDis = 31;
  _KeyOffsDis = 32;
  _FilletRad = 33;
  _Esc = 100;
  _NoPrevState = -12;


  // indexes of WindowFlags array
  _WindowProfile = 0;
  _WindowEnlarge = 1;
  _WindowZProfile = 2;
  // byte values for WindowFlags elements
  _ToTopWithMacroAction = 0;
  _AlwaysOnTop = 1;
  _NothingSpecial = 2;
  _DoNotShow = 3;

  //Licence Agreement
  LicParagraph1 = 'This software is distributed free of charge and on an "AS IS" basis. To the extent ' +
                  'permitted by law, I disclaim all warranties of any kind, either express or implied, ' +
                  'including but not limited to, warranties of merchantability, fitness for a particular purpose, ' +
                  'and non-infringement of third-party rights.';
  LicParagraph2 = 'I will not be liable for any direct, indirect, actual, exemplary or any other damages ' +
                  'arising from the use or inability to use this software, even if I have been advised of the ' +
                  'possibility of such damages.';
  LicParagraph3 = 'Whilst I do solicit contributions toward the cost of developing and distributing my ' +
                  'software, such contributions are entirely at your discretion and in no way constitute a ' +
                  'payment for the software.';
  LicParagraph4 = 'You may distribute this software to others provided that you distribute the complete ' +
                  'unaltered zip file provided by me at the dhsoftware.com.au web site, and that you do ' +
                  'so free of charge. This includes not charging for any other software, service or ' +
                  'product that you associate with this software in such a way as to imply that a purchase ' +
                  'is required in order to obtain this software (without limitation, examples of unacceptable ' +
                  'charges would be charging for distribution media or for any accompanying software that is ' +
                  'on the same media or contained in the same download or distribution file).  If you wish ' +
                  'to make any charge at all you need to obtain specific permission from me.';
  LicParagraph5 = 'Whilst it is free (or because of this), I would like and expect that if you can think of ' +
                  'any improvements or spot any bugs (or even spelling or formatting errors in the ' +
                  'documentation) that you would let me know. Your feedback will help with future development ' +
                  'of the macro.';

  ENL_WARN = 'You selected an enlargement, but it is disabled on both X & Y axis.' +  sLineBreak + sLineBreak +
             'Do you wish to proceed without enlargment?';

  EnlXLbl = 'Enlrg X';
  EnlXHint = 'Enlarge X-axis of section profile';
  EnlYLbl = 'Enlrg Y';
  EnlYHint = 'Enlarge Y-axis of section profile';
  EnlPflLbl = 'Enlrg Pfl';
  EnlPflHint = 'Specify an enlargement profile to use along the path';
  HelpAboutLbl = 'Help/About';

implementation

end.
