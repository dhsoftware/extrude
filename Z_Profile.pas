unit Z_Profile;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Extents, UInterfaces, URecords,
  UInterfacesRecords, UConstants, Math;

type
  TfmZProfile = class(TForm)
    imgZProf: TImage;
    lblStretch: TLabel;
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    pZprofile : ^entaddr;
  public
    { Public declarations }
    procedure SetZProfile (pZProfileAdr : pointer);
    procedure ShowZProfile;
  end;


implementation

{$R *.dfm}

procedure TfmZProfile.SetZProfile (pZProfileAdr : pointer);
begin
  pZprofile := pZProfileAdr;
end;


procedure TfmZProfile.FormResize(Sender: TObject);
begin
  ShowZProfile;
end;

procedure TfmZProfile.ShowZProfile;
var
  ent : entity;
  minpnt, maxpnt : point;
  topmargin, leftmargin : integer;
  topstr, btmstr : str80;
  XMultiplier, YMultiplier : double;
  adr : lgl_addr;
  pv : polyvert;
  cent : point;
  rad, bang, eang : double;
  ccw : boolean;

  procedure DrawArc (cntr : point; radx, rady, bang, eang : double; ccw : boolean);
  var
    centX, centY : integer;
    bangpnt, eangpnt : point;
    bangX, bangY, eangX, eangY : integer;
    w, h : integer;
  begin
    w := round(radx*XMultiplier);
    h := round(rady*YMultiplier);
    CentX := leftmargin + round((cntr.x-MinPnt.x)*XMultiplier);
    CentY := imgZProf.Height-round((cntr.y-MinPnt.y)*YMultiplier) - topmargin;
    cylind_cart (radx, bang, 0, bangpnt.x, bangpnt.y, bangpnt.z);
    cylind_cart (radx, eang, 0, eangpnt.x, eangpnt.y, eangpnt.z);
    addpnt (cntr, bangpnt, bangpnt);
    addpnt (cntr, eangpnt, eangpnt);
    if not ccw then
      SwapPnt (bangpnt, eangpnt);

    BangX := leftmargin + round((bangpnt.x-MinPnt.x)*XMultiplier);
    BangY := imgZProf.Height-round((bangpnt.y-MinPnt.y)*YMultiplier) - topmargin;
    EangX := leftmargin + round((eangpnt.x-MinPnt.x)*XMultiplier);
    EangY := imgZProf.Height-round((eangpnt.y-MinPnt.y)*YMultiplier) - topmargin;

    imgZProf.Canvas.Arc(centX-w, centY-h, centX+w, centY+h, bangX, bangY, eangX, eangY);
  end;

begin
  if ent_get (ent, pZprofile^) then begin
    // clear any existing image
    imgZProf.Picture.Bitmap.Width := imgZProf.Width;
    imgZProf.Picture.Bitmap.Height := imgZProf.Height;
    imgZProf.Canvas.Pen.Color := clWhite;
    imgZProf.Canvas.Brush.Color := clWhite;
    imgZProf.Canvas.Rectangle(0,0, imgZProf.Width, imgZProf.height);


    EntExtent(ent, minpnt, maxpnt);
    cvdisst (minpnt.y, btmstr);
    cvdisst (maxpnt.y, topstr);
    imgZProf.Canvas.Font := lblStretch.Font;
    imgZProf.Canvas.Pen.Color := clSilver;
    imgZProf.Canvas.Font.Color := clSilver;

    topmargin := imgZProf.Canvas.TextHeight(string(topstr));
    leftmargin := Math.max (imgZProf.Canvas.TextWidth(string(topstr)),
                            imgZProf.Canvas.TextWidth(string(btmstr)));
    XMultiplier := (imgZProf.width - leftmargin)/(maxpnt.x - minpnt.x);
    YMultiplier := (imgZProf.height - topmargin)/(maxpnt.y - minpnt.y);

    imgZProf.Canvas.TextOut(1, 0, string(topstr));
    imgZProf.Canvas.MoveTo (leftmargin, topMargin div 2);
    imgZProf.Canvas.LineTo (imgZProf.Width, topMargin div 2);

    imgZProf.Canvas.TextOut(1, imgZProf.height-topmargin, string(btmstr));
    imgZProf.Canvas.MoveTo (leftmargin, imgZProf.Height- (topmargin - topmargin div 2));
    imgZProf.Canvas.LineTo (imgZProf.Width, imgZProf.Height- (topmargin - topmargin div 2));

    topmargin := topmargin - topmargin div 2;

    imgZProf.Canvas.Pen.Color := clBlack;

    adr := ent.plnfrst;
    while polyvert_get (pv, adr, ent.plnfrst) do begin
      adr := pv.Next;
      if not pv.last then begin
        if pv.shape = pv_vert then begin
          imgZProf.Canvas.MoveTo(leftmargin + round((pv.pnt.x-MinPnt.x)*XMultiplier), imgZProf.Height-round((pv.pnt.y - MinPnt.y)*YMultiplier) - topmargin);
          imgZProf.Canvas.LineTo(leftmargin + round((pv.nextpnt.x-MinPnt.x)*XMultiplier), imgZProf.Height-round((pv.nextpnt.y - Minpnt.y)*YMultiplier) - topmargin);
        end
        else begin
          bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, cent, rad, bang, eang, ccw);
          DrawArc (cent, rad, rad, bang, eang, ccw);
        end;
      end;
    end;



  end;
end;

end.
