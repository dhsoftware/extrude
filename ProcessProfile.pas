unit ProcessProfile;

interface

uses CommonStuff, CommonTypes, Constants, Offset, Util, PlnUtil, EntToPntArr,
     System.Generics.Collections, URecords, UConstants, UInterfaces;

procedure ProfileToArray (doRotateFlip : boolean);    overload;

procedure ProfileToArray;   overload;

procedure ReverseProfileArray (var ProfileArray : tProfileArray);


implementation


procedure ProfileToArray (doRotateFlip : boolean);
var
  isThick : boolean;
  len : integer;
  ProfileItem : tProfile;
  sym : sym_type;
  d, bang, eang : double;
  deletej, changemade, wasclosed, visible : boolean;
  ent : entity;
  adr : lgl_addr;
  i, j, k, origlen, addedlen : integer;
  ProfileArray : tProfileArray;
  StartAngle : double;
  mat : modmat;
  temppnt : point;
  OffsetArray  : TArray<tProfileArrayItem>;
  points, points1 : TList<point>;
  atr : attrib;

  procedure AddToProfileArray (pnt : point; var len : integer; visible, thicknessLine : boolean);
  begin
    if length(ProfileArray.points) <= len then begin
      setlength (ProfileArray.points, len+256);
    end;
    ProfileArray.points[len].p := pnt;
    ProfileArray.points[len].visible := visible;
    ProfileArray.points[len].thicknessLine := thicknessLine;
    len := len+1;
  end;


  procedure ProcessEntProfile (const ent : entity);
  var
    i : integer;
    cntr : point;
    rad, bang, eang : double;
    TempArr : TArray<Point>;
    TempVisArr : TArray<boolean>;

  begin
    ProfileArray.enttyp := ent.enttype;
    if ent.color > 255 then
      ProfileArray.colour := 0                   // colour is used in chain logic (to only chain together items of the same colour)
    else                                         // (only used when there is a thickness as there is no need to chain profile
      ProfileArray.colour := byte (ent.color);   // lines otherwise)
    case ent.enttype of
      {$region 'entlin'}
      entlin: begin
                setlength (ProfileArray.points, 2);
                SubPnt (ent.linpt1, l^.profile.refpnt, ProfileArray.points[0].p);
                ProfileArray.points[0].visible := true;
                ProfileArray.points[1].thicknessLine := false;
                SubPnt (ent.linpt2, l^.profile.refpnt, ProfileArray.points[1].p);
                ProfileArray.points[1].visible := true;
                ProfileArray.points[1].thicknessLine := false;
                ProfileArray.closed := false;
                ProfileArray.colour := byte (ent.color);
                l^.ProfileArrays.Add(ProfileArray);
              end;
      {$endregion 'entlin'}
      {$region 'entply'}
      entply: begin
                ProfileArray.closed := true;
                setlength (ProfileArray.points, ent.plynpnt);
                ProfileArray.colour := byte(ent.color);
                for i := 1 to ent.plynpnt do begin
                  SubPnt (ent.plypnt[i], l^.profile.refpnt, ProfileArray.points[i-1].p);
                  ProfileArray.points[i-1].visible := ent.plyisln[i];
                  ProfileArray.points[i-1].thicknessLine := false;

                end;

                l^.ProfileArrays.Add(ProfileArray);
              end;
      {$endregion 'entply'}
      {$region 'entcrc, entarc'}
      entell,
      entcrc,
      entarc: begin
          case ent.enttype of
            entell : begin
                      rad := ent.ellrad.x;
                      bang := ent.ellbang;
                      eang := ent.elleang;
                      cntr := ent.ellcent;
                     end;
            entcrc : begin
                      rad := ent.crcrad;
                      bang := ent.crcbang;
                      eang := ent.crceang;
                      cntr := ent.crccent;
                     end;
            else begin   //entarc
                      rad := ent.arcrad;
                      bang := ent.arcbang;
                      eang := ent.arceang;
                      cntr := ent.arccent;
                     end;
          end;

          points := TList<point>.Create;
          try
            ArcToPntList (cntr, rad, bang, eang, l^.Divs.PflCrc, l^.Divs.PflEqDivs, points);
            setlength (ProfileArray.points, points.Count);
            if ent.enttype = entell then begin
              setenlrel (mat, 1, ent.ellrad.y/ent.ellrad.x, 1, cntr);
              catrotrel (mat, ent.ellang, z, cntr);
            end;
            for i := 0 to points.Count-1 do begin
              if ent.enttype = entell then begin
                xformpt (points[i], mat, temppnt);
                points[i] := temppnt;
              end;
              SubPnt (points[i], l^.profile.refpnt, ProfileArray.points[i].p);
              ProfileArray.points[i].visible := true;
              ProfileArray.points[i].thicknessLine := false;
            end;
            ProfileArray.closed := (ent.enttype = entcrc) or RealEqual(eang, bang+TwoPi);
            l^.ProfileArrays.Add(ProfileArray);
          finally
            points.Free;
          end;
        end;
      {$endregion 'entcrc, entarc'}
      {$region 'entpln'}
      entpln: begin
          TempArr := PlnToPntArray (ent, l^.Divs.PflCrc, l^.Divs.PflEqDivs, TempVisArr);
          setlength (ProfileArray.points, length(TempArr));
          for i := 0 to length(TempArr)-1 do begin
            subpnt (TempArr[i], l^.profile.refpnt, ProfileArray.points[i].p);
            ProfileArray.points[i].visible := TempVisArr[i];
            ProfileArray.points[i].thicknessLine := false;
          end;
          ProfileArray.closed := ent.plnclose;

          l^.ProfileArrays.Add (ProfileArray);
        end;
      {$endregion 'entpln'}
      {$region 'entell, entbez, entbsp'}
      entbez,
      entbsp: begin
          if GetBspBezPnts (ent, TempArr) then begin
            len := length(TempArr);
            setlength(ProfileArray.points, len);
            for i := 0 to len-1 do begin
              subpnt (TempArr[i], l^.profile.refpnt, ProfileArray.points[i].p);
              ProfileArray.points[i].visible := true;
              ProfileArray.points[i].thicknessLine := false;
            end;
            setlength (TempArr, 0);

            ProfileArray.closed := PointsEqual (ProfileArray.points[0].p, ProfileArray.points[len-1].p);
            if ProfileArray.closed and (abs (l^.ExtrThick) <= abszero) then begin
              len := len-1;
            end;

            setlength (ProfileArray.points, len);
            l^.ProfileArrays.Add(ProfileArray);
          end;
        end;
      {$endregion 'entell, entbez, entbsp'}
    end;


  end; // ProcessEntProfile

  procedure RemoveBottomHalf (var pnts : TList<point>);
  var
    i : integer;
    thispnt, nextpnt, prevpnt : point;
  begin
    for i := 0 to pnts.Count-1 do
      if RealEqual (pnts[i].y, zero) then begin
        thispnt := pnts[i];
        thispnt.y := zero;
        pnts[i] := thispnt;
      end;

    i := 0;
    while i < pnts.Count do begin
      thispnt := pnts[i];
      if RealEqual (thispnt.y, zero) then
        thispnt.y := zero;
      if thispnt.y < zero then begin
        prevpnt := pnts[(i+pnts.Count-1) mod pnts.count];
        nextpnt := pnts[(i+1) mod pnts.Count];
        if (prevpnt.y <= zero) and (nextpnt.y <= zero) then begin
          pnts.Delete(i);
          i := i-1;  // so that increment at the end of the loop doesn't skip an element
        end
        else if (prevpnt.y > zero) and (nextpnt.y > zero) then begin
          prevpnt.x := prevpnt.x - (prevpnt.x - thispnt.x) * prevpnt.y / (prevpnt.y - thispnt.y);
          prevpnt.y := zero;
          nextpnt.x := nextpnt.x - (nextpnt.x - thispnt.x) * nextpnt.y / (nextpnt.y - thispnt.y);
          nextpnt.y := zero;
          pnts[i] := prevpnt;
          pnts.Insert(i+1, nextpnt);
          i := i+1;   // no need to process this element in next iteration
        end
        else if (prevpnt.y > zero) then begin
          prevpnt.x := prevpnt.x - (prevpnt.x - thispnt.x) * prevpnt.y / (prevpnt.y - thispnt.y);
          prevpnt.y := zero;
          pnts[i] := prevpnt;
        end
        else if (nextpnt.y > zero) then begin
          nextpnt.x := nextpnt.x - (nextpnt.x - thispnt.x) * nextpnt.y / (nextpnt.y - thispnt.y);
          nextpnt.y := zero;
          pnts[i] := nextpnt;
        end
      end;
      i := i+1;
    end;
    i := pnts.Count;
    while ((pnts[0].y > zero) or (pnts[1].y = zero)) and (i > 0) do begin
      pnts.Move(0, pnts.Count-1);
      i := i-1;
    end;
  end;

  procedure RemoveTopHalf (var pnts : TList<point>);
  var
    i : integer;
    thispnt, nextpnt, prevpnt : point;
  begin
    if PointsEqual (pnts[0], pnts[pnts.Count-1]) then
      pnts.Delete(pnts.Count-1);
    while pnts[0].y < zero do
      pnts.Move(0, pnts.Count-1);

    i := 0;
    while i < pnts.Count do begin
      thispnt := pnts[i];
      if thispnt.y >= zero then begin
        prevpnt := pnts[(i+pnts.Count-1) mod pnts.count];
        nextpnt := pnts[(i+1) mod pnts.Count];
        if (prevpnt.y >= zero) and (nextpnt.y >= zero) then begin
            pnts.Delete(i);
          i := i-1;  // so that increment at the end of the loop doesn't skip an element
        end
        else if thispnt.y = zero then
           { do nothing }
        else if (prevpnt.y < zero) and (nextpnt.y < zero) then begin
          prevpnt.x := prevpnt.x - (prevpnt.x - thispnt.x) * prevpnt.y / (prevpnt.y - thispnt.y);
          prevpnt.y := zero;
          nextpnt.x := nextpnt.x - (nextpnt.x - thispnt.x) * nextpnt.y / (nextpnt.y - thispnt.y);
          nextpnt.y := zero;
          pnts[i] := prevpnt;
          pnts.Insert(i+1, nextpnt);
          i := i+1;   // no need to process this element in next iteration
        end
        else if (prevpnt.y < zero) then begin
          prevpnt.x := prevpnt.x - (prevpnt.x - thispnt.x) * prevpnt.y / (prevpnt.y - thispnt.y);
          prevpnt.y := zero;
          pnts[i] := prevpnt;
        end
        else if (nextpnt.y < zero) then begin
          nextpnt.x := nextpnt.x - (nextpnt.x - thispnt.x) * nextpnt.y / (nextpnt.y - thispnt.y);
          nextpnt.y := zero;
          pnts[i] := nextpnt;
        end
      end;
      i := i+1;
    end;
    i := pnts.Count;
    while ((pnts[0].y > zero) or (pnts[1].y = zero)) and (i > 0) do begin
      pnts.Move(0, pnts.Count-1);
      i := i-1;
    end;
  end;

  procedure RemoveRightHalf (var pnts : TList<point>);
  var
    i : integer;
    thispnt, nextpnt, prevpnt : point;
  begin
    for i := 0 to pnts.Count-1 do
      if RealEqual (pnts[i].x, zero) then begin
        thispnt := pnts[i];
        thispnt.x := zero;
        pnts[i] := thispnt;
      end;

    i := 0;
    while i < pnts.Count do begin
      thispnt := pnts[i];
      if thispnt.x > zero then begin
        prevpnt := pnts[(i+pnts.Count-1) mod pnts.count];
        nextpnt := pnts[(i+1) mod pnts.Count];
        if (prevpnt.x >= zero) and (nextpnt.x >= zero) then begin
          pnts.Delete(i);
          i := i-1;  // so that increment at the end of the loop doesn't skip an element
        end
        else if (prevpnt.x < zero) and (nextpnt.x < zero) then begin
          prevpnt.x := prevpnt.x - (prevpnt.x - thispnt.x) * prevpnt.x / (prevpnt.x - thispnt.x);
          prevpnt.y := zero;
          nextpnt.x := nextpnt.x - (nextpnt.x - thispnt.x) * nextpnt.x / (nextpnt.x - thispnt.x);
          nextpnt.y := zero;
          pnts[i] := prevpnt;
          pnts.Insert(i+1, nextpnt);
          i := i+1;   // no need to process this element in next iteration
        end
        else if (prevpnt.x < zero) then begin
          prevpnt.y := prevpnt.y - (prevpnt.y - thispnt.y) * prevpnt.x / (prevpnt.x - thispnt.x);
          prevpnt.x := zero;
          pnts[i] := prevpnt;
        end
        else if (nextpnt.x < zero) then begin
          nextpnt.y := nextpnt.y - (nextpnt.y - thispnt.y) * nextpnt.x / (nextpnt.x - thispnt.x);
          nextpnt.x := zero;
          pnts[i] := nextpnt;
        end
      end;
      i := i+1;
    end;
    i := pnts.Count;
    while ((pnts[0].x > zero) or (pnts[1].x = zero)) and (i > 0) do begin
      pnts.Move(0, pnts.Count-1);
      i := i-1;
    end;
  end;

  procedure RemoveLeftHalf (var pnts : TList<point>);
  var
    i : integer;
    thispnt, nextpnt, prevpnt : point;
  begin
    for i := 0 to pnts.Count-1 do
      if RealEqual (pnts[i].x, zero) then begin
        thispnt := pnts[i];
        thispnt.x := zero;
        pnts[i] := thispnt;
      end;

    i := 0;
    while i < pnts.Count do begin
      thispnt := pnts[i];
      if thispnt.x < zero then begin
        prevpnt := pnts[(i+pnts.Count-1) mod pnts.count];
        nextpnt := pnts[(i+1) mod pnts.Count];
        if (prevpnt.x <= zero) and (nextpnt.x <= zero) then begin
          pnts.Delete(i);
          i := i-1;  // so that increment at the end of the loop doesn't skip an element
        end
        else if (prevpnt.x > zero) and (nextpnt.x > zero) then begin
          prevpnt.y := prevpnt.y - (prevpnt.y - thispnt.y) * prevpnt.x / (prevpnt.x - thispnt.x);
          prevpnt.x := zero;
          nextpnt.y := nextpnt.y - (nextpnt.y - thispnt.y) * nextpnt.x / (nextpnt.x - thispnt.x);
          nextpnt.x := zero;
          pnts[i] := prevpnt;
          pnts.Insert(i+1, nextpnt);
          i := i+1;   // no need to process this element in next iteration
        end
        else if (prevpnt.x > zero) then begin
          prevpnt.y := prevpnt.y - (prevpnt.y - thispnt.y) * prevpnt.x / (prevpnt.x - thispnt.x);
          prevpnt.x := zero;
          pnts[i] := prevpnt;
        end
        else if (nextpnt.x > zero) then begin
          nextpnt.y := nextpnt.y - (nextpnt.y - thispnt.y) * nextpnt.x / (nextpnt.x - thispnt.x);
          nextpnt.x := zero;
          pnts[i] := nextpnt;
        end
      end;
      i := i+1;
    end;
    i := pnts.Count;
    while ((pnts[0].x > zero) or (pnts[1].x = zero)) and (i > 0) do begin
      pnts.Move(0, pnts.Count-1);
      i := i-1;
    end;
  end;


begin
  l^.ProfileArrays.Clear;
  isThick := abs (l^.ExtrThick) > abszero;
  {$region 'Add each profile to l^.ProfileArrays'}
  for ProfileItem in l^.profile.Items do begin
    case ProfileItem.ProfileType of
      {$region 'p_circle'}
      p_circle : begin
          ProfileArray.enttyp := 0;
          case ProfileItem.cportion of
            tophalf: begin
                bang := 0;
                eang := pi;
              end;
            bottomhalf: begin
                bang := pi;
                eang := twopi;
              end;
            lefthalf: begin
                bang := halfpi;
                eang := pi32;
              end;
            righthalf: begin
                bang := pi32;
                eang := halfpi;
              end
            else begin   // complete
                bang := 0;
                eang := twopi;
              end;
          end;

          points := TList<point>.Create;
          points1 := TList<point>.Create;
          try
            ArcToPntList (NewPoint (0,0,0), ProfileItem.radius, bang, eang, l^.Divs.PflCrc, l^.Divs.PflEqDivs, points);
            setlength (ProfileArray.points, points.Count);
            for i := 0 to points.Count-1 do
              ProfileArray.points[i] := NewProfileArrayItem(points[i]);

            if isThick then begin
              ArcToPntList (NewPoint (0,0,0), ProfileItem.radius - l^.ExtrThick, bang, eang, l^.Divs.PflCrc, l^.Divs.PflEqDivs, points1);
              points1.Reverse;
              setlength (ProfileArray.points, points.Count + points1.Count);
              for i := 0 to points1.Count-1 do begin
                ProfileArray.points[points.Count+i] := NewProfileArrayItem(points1[i], true, true);
              end;
              ProfileArray.closed := true;
              if ProfileItem.cportion = complete then begin
                ProfileArray.points[points.Count-1].visible := false;
                ProfileArray.points[length(ProfileArray.points)-1].visible := false;
              end;
            end
            else
              ProfileArray.closed := (ProfileItem.cportion = complete);
          finally
            points.Free;
            points1.Free;
          end;

          l^.ProfileArrays.Add(ProfileArray);
        end;
      {$endregion 'p_circle'}
      {$region 'p_polygon'}
      p_polygon : begin
          points := TList<point>.Create;
          points1 := TList<point>.Create;
          try
            StartAngle := -halfpi + Pi/ProfileItem.numsides;

            ArcToPntList (NewPoint (0,0,0), ProfileItem.plyradius,
                            StartAngle, StartAngle+TwoPi, ProfileItem.numsides, true, points);
            for i := 0 to points.Count-1 do begin
              temppnt := points[i];
              if RealEqual (TempPnt.x, zero) then
                TempPnt.x := zero;
              if RealEqual (TempPnt.y, zero) then
                TempPnt.y := zero;
            end;

            if isThick then begin
              ArcToPntList (NewPoint (0,0,0), ProfileItem.plyradius - l^.ExtrThick/sin(Pi/ProfileItem.numsides),
                            StartAngle, StartAngle+TwoPi, ProfileItem.numsides, true, points1);
            end;

            case ProfileItem.pportion of
              tophalf : begin
                  RemoveBottomHalf (points);
                  if isThick then
                    RemoveBottomHalf (points1);
                end;
              bottomhalf : begin
                  RemoveTopHalf (points);
                  if isThick then
                    RemoveTopHalf (points1);
                end;
              lefthalf : begin
                  RemoveRightHalf (points);
                  if abs (l^.ExtrThick) > abszero then
                    RemoveRightHalf (points1);
                end;
              righthalf : begin
                  RemoveLeftHalf (points);
                  if isThick then
                    RemoveLeftHalf (points1);
                end
            end;
            ProfileArray.enttyp := 0;
            ProfileArray.closed := (ProfileItem.pportion = complete) or isThick;
            setlength (ProfileArray.points, points.Count);
            for i := 0 to points.Count-1 do
              ProfileArray.points[i] :=
                  NewProfileArrayItem (points[i], (i < (points.Count-1)) or (ProfileArray.closed and (ProfileItem.pportion <> complete)),
                                       (i=(points.Count-1)));
            if isThick then begin
              points1.Reverse;
              setlength (ProfileArray.points, points.Count+points1.Count);
              for i :=  0 to points1.Count-1 do
                ProfileArray.points[points.Count+i] :=
                    NewProfileArrayItem (points1[i], (i < (points1.count-1)) or (ProfileArray.Closed and (ProfileItem.pportion <> complete)), true);
            end;
            l^.ProfileArrays.Add(ProfileArray);
          finally
            points.Free;
            points1.Free;
          end;
        end;
      {$endregion 'p_polygon'}
      {$region 'p_rectangle'}
      p_rectangle : begin
          ProfileArray.enttyp := 0;
          case ProfileItem.rportion of
            complete : begin
                ProfileArray.closed := true;
                if isThick then
                  setlength (ProfileArray.points, 10)
                else
                  setlength (ProfileArray.points, 4);
                ProfileArray.points[0] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, ProfileItem.height/2, 0), true);
                ProfileArray.points[1] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, -ProfileItem.height/2, 0), true);
                ProfileArray.points[2] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, -ProfileItem.height/2, 0), true);
                ProfileArray.points[3] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, ProfileItem.height/2, 0), true);

                if isThick then begin
                  ProfileArray.points[4] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, ProfileItem.height/2, 0), false, true);
                  ProfileArray.points[5] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, ProfileItem.height/2-l^.ExtrThick, 0), true, true);
                  ProfileArray.points[6] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, l^.ExtrThick-ProfileItem.height/2, 0), true, true);
                  ProfileArray.points[7] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, l^.ExtrThick-ProfileItem.height/2, 0), true, true);
                  ProfileArray.points[8] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, ProfileItem.height/2-l^.ExtrThick, 0), true, true);
                  ProfileArray.points[9] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, ProfileItem.height/2-l^.ExtrThick, 0), false, true);
                end;

              end;
            tophalf : begin
                ProfileArray.closed := isThick;
                if isThick then
                  setlength (ProfileArray.points, 8)
                else
                  setlength (ProfileArray.points, 4);
                ProfileArray.points[0] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, zero, zero), true);
                ProfileArray.points[1] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, ProfileItem.height/2, 0), true);
                ProfileArray.points[2] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, ProfileItem.height/2, 0), true);
                ProfileArray.points[3] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, zero, zero), isThick);

                if isThick then begin
                  ProfileArray.points[4] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, zero, zero), true, true);
                  ProfileArray.points[5] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, ProfileItem.height/2-l^.ExtrThick, 0), true, true);
                  ProfileArray.points[6] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, ProfileItem.height/2-l^.ExtrThick, 0), true, true);
                  ProfileArray.points[7] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, zero, zero), true, true);
                end;
              end;
            bottomhalf : begin
                ProfileArray.closed := isThick;
                if isThick then
                  setlength (ProfileArray.points, 8)
                else
                  setlength (ProfileArray.points, 4);
                ProfileArray.points[0] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, zero, zero), true);
                ProfileArray.points[1] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, -ProfileItem.height/2, 0), true);
                ProfileArray.points[2] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, -ProfileItem.height/2, 0), true);
                ProfileArray.points[3] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, zero, zero), isThick);

                if isThick then begin
                  ProfileArray.points[4] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, zero, zero), true, true);
                  ProfileArray.points[5] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, l^.ExtrThick-ProfileItem.height/2, 0), true, true);
                  ProfileArray.points[6] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, l^.ExtrThick-ProfileItem.height/2, 0), true, true);
                  ProfileArray.points[7] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, zero, zero), true, true);
                end;
              end;
            lefthalf : begin
                ProfileArray.closed := isThick;
                if isThick then
                  setlength (ProfileArray.points, 8)
                else
                  setlength (ProfileArray.points, 4);
                ProfileArray.points[0] := NewProfileArrayItem (Newpoint(zero, ProfileItem.height/2, zero), true);
                ProfileArray.points[1] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, ProfileItem.height/2, 0), true);
                ProfileArray.points[2] := NewProfileArrayItem (Newpoint(-ProfileItem.width/2, -ProfileItem.height/2, 0), true);
                ProfileArray.points[3] := NewProfileArrayItem (Newpoint(zero, -ProfileItem.height/2, zero), isThick);

                if isThick then begin
                  ProfileArray.points[4] := NewProfileArrayItem (Newpoint(zero, l^.ExtrThick-ProfileItem.height/2, zero), true, true);
                  ProfileArray.points[5] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, l^.ExtrThick-ProfileItem.height/2, zero), true, true);
                  ProfileArray.points[6] := NewProfileArrayItem (Newpoint(l^.ExtrThick-ProfileItem.width/2, ProfileItem.height/2-l^.ExtrThick, 0), true, true);
                  ProfileArray.points[7] := NewProfileArrayItem (Newpoint(zero, ProfileItem.height/2-l^.ExtrThick, 0), true, true);
                end;
              end;
            righthalf : begin
                ProfileArray.closed := isThick;
                if isThick then
                  setlength (ProfileArray.points, 8)
                else
                  setlength (ProfileArray.points, 4);
                ProfileArray.points[0] := NewProfileArrayItem (Newpoint(zero, -ProfileItem.height/2, zero), true);
                ProfileArray.points[1] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, -ProfileItem.height/2, 0), true);
                ProfileArray.points[2] := NewProfileArrayItem (Newpoint(ProfileItem.width/2, ProfileItem.height/2, 0), true);
                ProfileArray.points[3] := NewProfileArrayItem (Newpoint(zero, ProfileItem.height/2, zero), isThick);

                if isThick then begin
                  ProfileArray.points[4] := NewProfileArrayItem (Newpoint(zero, ProfileItem.height/2-l^.ExtrThick, zero), true, true);
                  ProfileArray.points[5] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, ProfileItem.height/2-l^.ExtrThick, zero), true, true);
                  ProfileArray.points[6] := NewProfileArrayItem (Newpoint(ProfileItem.width/2-l^.ExtrThick, l^.ExtrThick-ProfileItem.height/2, 0), true, true);
                  ProfileArray.points[7] := NewProfileArrayItem (Newpoint(zero, l^.ExtrThick-ProfileItem.height/2, 0), true, true);
                end;
              end;
          end;
          l^.ProfileArrays.Add(ProfileArray);
        end;
      {$endregion 'p_rectangle'}
      {$region 'p_entity'}
      p_entity :
        if ent_get (ent, ProfileItem.adr) then
          ProcessEntProfile (ent);
      {$endregion 'p_entity'}
      {$region 'p_symbol'}
      p_symbol : if symread (ProfileItem.name, ProfileItem.name, sym) = fl_ok then begin
          adr := sym.frstent;
          if atr_symfind (sym, dhExtShelTk, atr) and (atr.atrtype = atr_dis) then
            l^.ExtrThick := atr.dis;
          while ent_get (ent, adr) do begin
            ProcessEntProfile (ent);
            if addr_equal(adr, sym.lastent) then
              setnil (adr)  // ensure loop stops at last entity in symbol (probably not necessary, but not well documented)
            else
              adr := ent.Next;
          end;
        end;
      {$endregion 'p_symbol'}
    end;
  end;   // for ProfileItem in l^.profile.Items
  {$endregion 'Add each profile to l^.ProfileArrays'}

  {$region 'chain eligible profiles together'}
  repeat
    changemade := false;
    i := 0;
    while i < l^.ProfileArrays.Count-1 do begin
      if l^.ProfileArrays[i].closed or (l^.ProfileArrays[i].enttyp = 0) then begin
        i := i+1;
        continue;
      end;
      ProfileArray := l^.ProfileArrays[i];
      j := i+1;
      while (j < l^.ProfileArrays.Count) and not ProfileArray.closed do begin
        if l^.ProfileArrays[j].closed or (l^.ProfileArrays[j].enttyp = 0) or (l^.ProfileArrays[j].colour <> ProfileArray.colour) then begin
          j := j+1;
          continue;
        end;
        origlen := length (ProfileArray.points);
        deletej := false;
        if PointsEqual (ProfileArray.points[0].p, l^.ProfileArrays[j].points[0].p) then begin
          {$region 'First points of arrays equal'}
          addedlen := length (l^.ProfileArrays[j].points) - 1;
          setlength (ProfileArray.points, origlen + addedlen);
          for k := origlen-1 downto 0 do begin
            ProfileArray.points[addedlen + k] := ProfileArray.points[k];
          end;
          for k := 0 to addedlen-1 do begin
            ProfileArray.points[k] := l^.ProfileArrays[j].points[addedlen-k]
          end;
          deletej := true;
          {$endregion 'First points of arrays equal'}
        end
        else if PointsEqual (ProfileArray.points[0].p, l^.ProfileArrays[j].points[length(l^.ProfileArrays[j].points)-1].p) then begin
          {$region 'First point of Array equal to last point of j array'}
          addedlen := length (l^.ProfileArrays[j].points) - 1;
          setlength (ProfileArray.points, length(ProfileArray.points) + addedlen);
          for k := origlen-1 downto 0 do begin
            ProfileArray.points[addedlen + k] := ProfileArray.points[k];
          end;
          for k := 0 to addedlen-1 do begin
            ProfileArray.points[k] := l^.ProfileArrays[j].points[k]
          end;
          deletej := true;
          {$endregion 'First point of Array equal to last point of j array'}
        end
        else if PointsEqual (ProfileArray.points[length(ProfileArray.points)-1].p, l^.ProfileArrays[j].points[0].p) then begin
          {$region 'Last point of Array equal to first point of j array'}
          addedlen := length (l^.ProfileArrays[j].points) - 1;
          setlength (ProfileArray.points, length(ProfileArray.points) + addedlen);
          for k := 0 to addedlen-1 do begin
            ProfileArray.points[length(ProfileArray.points) - addedlen + k] := l^.ProfileArrays[j].points[k+1];
          end;
          deletej := true;
          {$endregion 'Last point of Array equal to first point of j array'}
        end
        else if PointsEqual (ProfileArray.points[length(ProfileArray.points)-1].p, l^.ProfileArrays[j].points[length(l^.ProfileArrays[j].points)-1].p) then begin
          {$region 'Last points of Arrays equal'}
          addedlen := length (l^.ProfileArrays[j].points) - 1;
          setlength (ProfileArray.points, length(ProfileArray.points) + addedlen);
          for k := 0 to addedlen-1 do begin
            ProfileArray.points[length(ProfileArray.points) - addedlen + k] := l^.ProfileArrays[j].points[addedlen-k-1];
          end;
          deletej := true;
          {$endregion 'Last points of Arrays equal'}
        end;
        if deletej then begin
          ProfileArray.enttyp := entlin;  // always complete combined entities as lines
          l^.ProfileArrays.Delete(j);
          if PointsEqual (ProfileArray.points[0].p, ProfileArray.points[length(ProfileArray.points)-1].p) then begin
            ProfileArray.closed := true;
            setlength (ProfileArray.points, length(ProfileArray.points)-1);  // remove last point as subsequent close logic will return outline to first point
          end;
          changemade := true;
        end
        else begin
          j := j+1;
        end;
      end;
      l^.ProfileArrays[i] := ProfileArray;
      i := i+1;
    end;
  until not changemade;
  {$endregion 'chain eligible profiles together'}

 {$region 'Process Thickness'}
  if abs(l^.ExtrThick) > abszero then begin
    for i := 0 to l^.ProfileArrays.Count-1 do begin
      if l^.ProfileArrays[i].enttyp = 0 then begin
        continue;   // these circle/regular polygon profiles have already had thickness processed
      end;
      ProfileArray := l^.ProfileArrays[i];

      len := length(ProfileArray.points);
      wasclosed := ProfileArray.closed;
      if wasclosed then begin
        AddToProfileArray(ProfileArray.points[0].p, len, false, true);
      end
      else
        ProfileArray.points[length(ProfileArray.points)-1].thicknessLine := true;

      ProfileArray.closed := true;

      OffsetProfileArray (ProfileArray.points, len, OffsetArray, wasclosed, l^.ExtrThick);

      if wasclosed then
        AddToProfileArray (OffsetArray[0].p, len, true, true);
      for j := length(OffsetArray)-1 downto 0 do begin
        if j = 0 then
          visible := OffsetArray[length(OffsetArray)-1].visible
        else
          visible := OffsetArray[j-1].visible;
        visible := visible and ((j>0) or not wasclosed);
        AddToProfileArray (OffsetArray[j].p, len, visible, true)
      end;

      setlength (ProfileArray.points, len);
      l^.ProfileArrays[i] := ProfileArray;
    end;
  end;
  {$endregion 'Process Thickness'}

  {$region 'Apply starting rotation and flip modifiers'}

  if doRotateFlip and (HorizFlip in l^.profile.Modifier) then for i := 0 to l^.ProfileArrays.Count-1 do begin
    ProfileArray := l^.ProfileArrays[i];
    for j := 0 to length(profileArray.points)-1 do
      ProfileArray.points[j].p.x := -ProfileArray.points[j].p.x;
    l^.ProfileArrays[i] := ProfileArray;
  end;
  if doRotateFlip and (VertFlip in l^.profile.Modifier) then for i := 0 to l^.ProfileArrays.Count-1 do begin
    ProfileArray := l^.ProfileArrays[i];
    for j := 0 to length(profileArray.points)-1 do
      ProfileArray.points[j].p.y := -ProfileArray.points[j].p.y;
    l^.ProfileArrays[i] := ProfileArray;
  end;
  if doRotateFlip and (Rot180 in l^.profile.Modifier) then for i := 0 to l^.ProfileArrays.Count-1 do begin
    ProfileArray := l^.ProfileArrays[i];
    for j := 0 to length(profileArray.points)-1 do begin
      ProfileArray.points[j].p.x := -ProfileArray.points[j].p.x;
      ProfileArray.points[j].p.y := -ProfileArray.points[j].p.y;
    end;
    l^.ProfileArrays[i] := ProfileArray;
  end;
  if doRotateFlip and (Rot90 in l^.profile.Modifier) then for i := 0 to l^.ProfileArrays.Count-1 do begin
    ProfileArray := l^.ProfileArrays[i];
    for j := 0 to length(profileArray.points)-1 do begin
      d := ProfileArray.points[j].p.x;
      ProfileArray.points[j].p.x := -ProfileArray.points[j].p.y;
      ProfileArray.points[j].p.y := d;
    end;
    l^.ProfileArrays[i] := ProfileArray;
  end;

  if (l^.StartRotation <> zero) then begin
    setrotrel (mat, l^.StartRotation, z, l^.profile.refpnt);
    for i := 0 to l^.ProfileArrays.Count-1 do begin
      ProfileArray := l^.ProfileArrays[i];
      for j := 0 to length(profileArray.points)-1 do
        xformpt (ProfileArray.points[j].p, mat, ProfileArray.points[j].p);
      l^.ProfileArrays[i] := ProfileArray;
    end;
  end;
  {$endregion 'Apply starting rotation and flip modifiers'}
end;


procedure ProfileToArray;
begin
  ProfileToArray (true);
end;


procedure ReverseProfileArray (var ProfileArray : tProfileArray);
var
  ndx, ndx1 : integer;
  newpnts : TArray<tProfileArrayItem>;
begin
  setlength (newpnts, length(ProfileArray.points));
  for ndx := length(ProfileArray.points)-1  downto 0 do begin
    if ndx > 0 then
      ndx1 := ndx-1
    else
      ndx1 := length(ProfileArray.points)-1;
    newpnts[length(newpnts)-1-ndx].p := ProfileArray.points[ndx].p;
    newpnts[length(newpnts)-1-ndx].visible := ProfileArray.points[ndx1].visible;
    newpnts[length(newpnts)-1-ndx].thicknessLine := ProfileArray.points[ndx1].thicknessLine;
  end;
  ProfileArray.points := newpnts;
end;



end.
