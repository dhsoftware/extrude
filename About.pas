unit About;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, ShellApi, UInterfaces, UConstants,
  System.UITypes, Constants;

type
  TfmAbout = class(TForm)
    lblVersion: TLabel;
    Label1: TLabel;
    lblwww: TLabel;
    lblContribute: TLabel;
    Button1: TButton;
    btnHelp: TButton;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure lblContributeClick(Sender: TObject);
    procedure lblwwwClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure Memo1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure SetMemo;
  public
    { Public declarations }
  end;

var
  fmAbout: TfmAbout;

function CheckHelpFile : string;

implementation

{$R *.dfm}


procedure GetBuildInfo(var V1, V2, V3, V4: word);
var
  VerInfoSize, VerValueSize, Dummy: DWORD;
  VerInfo: Pointer;
  VerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  if VerInfoSize > 0 then
  begin
      GetMem(VerInfo, VerInfoSize);
      try
        if GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo) then
        begin
          VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
          with VerValue^ do
          begin
            V1 := dwFileVersionMS shr 16;
            V2 := dwFileVersionMS and $FFFF;
            V3 := dwFileVersionLS shr 16;
            V4 := dwFileVersionLS and $FFFF;
          end;
        end;
      finally
        FreeMem(VerInfo, VerInfoSize);
      end;
  end;
end;

function GetModuleName: string;
var
  szFileName: array[0..MAX_PATH] of Char;
begin
  FillChar(szFileName, SizeOf(szFileName), #0);
  GetModuleFileName(hInstance, szFileName, MAX_PATH);
  Result := szFileName;
end;

function GetBuildInfoAsString: string;
var
  V1, V2, V3, V4: word;
begin
  GetBuildInfo(V1, V2, V3, V4);
  Result := IntToStr(V1) + '.' + IntToStr(V2) + '.' +
    IntToStr(V3) + '.' + IntToStr(V4);
end;

function CheckHelpFile : string;
var
  msg, helpfilename : string;
begin
  helpfilename := ExtractFilePath(GetModuleName) + 'ExtrudeMacro.pdf';
  if FileExists (helpfilename) then begin
    result := helpfilename
  end
  else begin
    msg := 'REFERENCE MANUAL WAS NOT FOUND at the following location:' + sLineBreak + sLineBreak +
            helpfilename + sLineBreak + sLineBreak +'Please copy the manual (ExtrudeMacro.pdf) to this location.';
    MessageDlg (msg, mtError, [mbOK], 0);
    result := '';
  end;
end;

procedure TfmAbout.btnHelpClick(Sender: TObject);
var
  helpfilename : string;
begin
  helpfilename := CheckHelpFile;
  if helpfilename > '' then
    ShellExecute(0, 'open', PWideChar(helpfilename),nil,nil, SW_SHOWNORMAL);
end;

procedure TfmAbout.SetMemo;
begin
  Memo1.Lines.Clear;
  Memo1.Lines.Add(LicParagraph1);
  Memo1.Lines.Add('');
  Memo1.Lines.Add(LicParagraph2);
  Memo1.Lines.Add('');
  Memo1.Lines.Add(LicParagraph3);
  Memo1.Lines.Add('');
  Memo1.Lines.Add(LicParagraph4);
  Memo1.Lines.Add('');
  Memo1.Lines.Add(LicParagraph5);
end;

procedure TfmAbout.FormCreate(Sender: TObject);
begin
  lblVersion.Caption := 'Version ' + GetBuildInfoAsString;
  SetMemo;
end;

procedure TfmAbout.FormResize(Sender: TObject);
begin
  SetMemo;
end;

procedure TfmAbout.lblContributeClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','https://www.dhsoftware.com.au/contribute.htm',nil,nil, SW_SHOWNORMAL);
end;

procedure TfmAbout.lblwwwClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','https://www.dhsoftware.com.au',nil,nil, SW_SHOWNORMAL);
end;

procedure TfmAbout.Memo1Click(Sender: TObject);
begin
  btnHelp.SetFocus;
end;

end.
