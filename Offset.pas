unit Offset;

interface

  uses Commonstuff, URecords, UInterfaces, UConstants, System.Generics.Collections,
       Util, CommonTypes;

  PROCEDURE OffsetProfileArray (const pnts_in : TArray<tProfileArrayItem>;
                                len_in        : integer;
                                var pnts_out  : TArray<tProfileArrayItem>;
                                closed        : boolean;
                                offsdist      : double);

  PROCEDURE OffsetTracePnts;

implementation

TYPE
  LineTyp = record
    pt1, pt2 : point;
    visible : boolean;
  end;

  PROCEDURE DrawDebugLine (p1, p2 : point; clr : integer);
  var
    lin : entity;
  begin
    ent_init (lin, entlin);
    lin.linpt1 := p1;
    lin.linpt2 := p2;
    lin.color := clr;
    ent_draw (lin, drmode_white);
    ent_init (lin, entmrk);
    lin.mrkpnt := p1;
    lin.mrktyp := 2;
    lin.mrksiz := 25;
    lin.color := clr;
    ent_draw (lin, drmode_white);
    lin.mrkpnt := p2;
    lin.mrktyp := 2;
    lin.mrksiz := 25;
    lin.color := clr;
    ent_draw (lin, drmode_white);
  end;





PROCEDURE IntersectNewLines (var NewLines : TList<LineTyp>; closed : boolean);
VAR
  FirstLine : LineTyp;
  i, j   : integer;
  Line1, Line2 : LineTyp;
  int1 : point;
  Ang1, Ang2 : double;
BEGIN
    // adjust newlines to intersect with each other
    i := 0;
    FirstLine := NewLines[0];
    while i < NewLines.Count - ord (not closed) do begin
      Line1 := NewLines[i];
      j := (i+1) mod NewLines.Count;
      if j=0 then
        Line2 := FirstLine
      else
        Line2 := NewLines[j];

      ang1 := angle (Line1.pt1, Line1.pt2);
      ang2 := angle (line2.pt1, Line2.pt2);
      if RealEqual (ang1, ang2) then
         meanpnt (Line1.pt2, Line2.pt1, int1)
      else if RealEqual (abs(ang1-ang2), Pi) then begin
        Line1.pt2 := Line2.pt2;
        Line1.visible := Line1.visible or Line2.visible;
        NewLines.Delete(j);
        continue;
      end
      else if not intr_linlin (Line1.pt1, Line1.pt2, Line2.pt1, Line2.pt2, int1, false) then
        exit;

      if closed or (i < (NewLines.Count-1)) then begin
        Line1.pt2 := int1;
        NewLines[i] := Line1;
        Line2.pt1 := int1;
        NewLines[j] := Line2;
      end
      else begin
        Line2.pt1 := int1;
        NewLines[j] := Line2;
      end;
      i := i+1;
    end;
END;


PROCEDURE OffsetProfileArray (const pnts_in : TArray<tProfileArrayItem>;
                              len_in        : integer;
                              var pnts_out  : TArray<tProfileArrayItem>;
                              closed        : boolean;
                              offsdist      : double);

VAR
  Line, NewLine : LineTyp;
  Lines, NewLines : TList<LineTyp>;
  direction : double;
  mov : point;
  i   : integer;
BEGIN
  Lines := TList<LineTyp>.Create;
  NewLines := TList<LineTyp>.Create;
  try
    // populate lines array
    if len_in > length(pnts_in) then
      len_in := length(pnts_in);
    for i := 0 to len_in-1 do begin
      if (not closed) and (i = len_in-1) then
        break;
      if (not pointsequal (pnts_in[i].p, pnts_in[(i+1) mod len_in].p)) then begin
        Line.pt1 := pnts_in[i].p;
        Line.pt2 := pnts_in[(i+1) mod len_in].p;
        Line.visible := pnts_in[i].visible;
        Lines.add(Line);
      end;
    end;

    // create newlines array offset to the the original
    for i := 0 to Lines.Count-1 do begin
      Line := Lines[i];
      NewLine.visible := Line.visible;
        direction := angle (Line.pt1, Line.pt2);
        direction := direction + halfpi;

        cylind_cart (offsdist, direction, 0.0, mov.x, mov.y, mov.z);

        addpnt (Line.pt1, mov, NewLine.pt1);
        addpnt (Line.pt2, mov, NewLine.pt2);

        NewLines.Add(NewLine);
    end;

    if (newlines.Count = 1) then begin
      setlength (pnts_out, 2);
      pnts_out[0].p := newlines[0].pt1;
      pnts_out[0].visible := true;
      pnts_out[1].p := newlines[0].pt2;
      pnts_out[1].visible := true;
      exit;
    end;

    IntersectNewLines (NewLines, closed);

    if NewLines.Count = 0 then
      exit;    // don't think this can ever happen, but just in case ...

    if not closed then begin
      setlength (pnts_out, NewLines.Count + 1);
      for i := 0 to NewLines.Count-1 do begin
        pnts_out[i].p := NewLines[i].pt1;
        pnts_out[i].visible := NewLines[i].visible;
      end;
      pnts_out[NewLines.Count].p := NewLines[NewLines.Count-1].pt2;
      pnts_out[NewLines.Count].visible := true;
    end
    else begin
      setlength (pnts_out, NewLines.Count);
      for i := 0 to NewLines.Count-1 do begin
        pnts_out[i].p := NewLines[i].pt1;
        pnts_out[i].visible := NewLines[i].visible;
      end;
    end;
  finally
    Lines.Free;
    NewLines.Free;
  end;

END;

PROCEDURE FilletLines (CONST NewLines : TList<LineTyp>);
VAR
  i, i2, j : integer;
  radius, ang, ang2, d1, d2, cz : double;
  ln1, ln2 : LineTyp;
  mov1, mov2, cntr : point;
  done, clockwise : boolean;
  Line, NewLine,
  extseg1, extseg2 : LineTyp; // these are used to define the maximum extent of the fillet:
                              // generally it is half the length of the line being filleted, except that
                              // if the path being offset is not close then it can be the full lenth
                              // for the first and last segments.
  TempPnts : TList<point>;
BEGIN
  i := 0;
  while (i < NewLines.count-1 + ord(l^.traceclosed in [ClosedByOption, ClosedByPoints])) and NewLines[i].visible  do begin
    i2 := (i+1) mod NewLines.Count;
    if not NewLines[i2].visible then begin
      i := i+1;
      continue
    end;
    cz := crossz (NewLines[i].pt1, NewLines[i].pt2, NewLines[i2].pt2);
    if RealEqual (cz, zero) then begin  // lines are at the same angle - no need to fillet
      i := i+1;
    end
    else begin
      radius := l^.FilletRad;  // initial value of radius - logic to reduced it if it doesn't work has been removed for now

      // find acceptable segments where fillet can start/end
      extseg1 := NewLines[i];
      extseg2 := NewLines[i2];

      ang := angle (NewLines[i].pt1, NewLines[i].pt2);
      ang2 := angle (NewLines[i2].pt1, NewLines[i2].pt2);
      if cz > zero then begin
        d1 := ang + halfpi;
        d2 := ang2 + halfpi;
      end
      else begin
        d1 := ang - halfpi;
        d2 := ang2 - halfpi;
      end;
      done := false;     // note: done was used when I was trying to adjust fillet size if it didn't fit. I no longer do this,
                         //       so done is not really required, but have left it here as I may revisit the size logic ...
      repeat
        cylind_cart (radius, d1, 0.0, mov1.x, mov1.y, mov1.z);
        addpnt (NewLines[i].pt1, mov1, ln1.pt1);
        addpnt (NewLines[i].pt2, mov1, ln1.pt2);    // make ln1 parallel to NewLines[i] at a distance of radius
        cylind_cart (radius, d2, 0.0, mov2.x, mov2.y, mov2.z);
        addpnt (NewLines[i2].pt1, mov2, ln2.pt1);
        addpnt (NewLines[i2].pt2, mov2, ln2.pt2);  // make ln2 parallel to NewLines[i+1] at a distance of radius

        if intr_linlin (ln1.pt1, ln1.pt2, ln2.pt1, ln2.pt2, cntr, false) then begin   // fillet cntr is intersection of ln1 & ln2
          ln1.pt1 := cntr;
          subpnt (ln1.pt1, mov1, ln1.pt2);
          if between (extseg1.pt1, extseg1.pt2, ln1.pt2) < 0 then begin
            i := i+1;
            done := true;
            continue;
          end;
          ln2.pt1 := cntr;
          subpnt (ln2.pt1, mov2, ln2.pt2);
          if between (extseg2.pt1, extseg2.pt2, ln2.pt2) < 0 then begin
              i := i+1;
              done := true;
              continue;
          end;
          intr_linlin (ln1.pt1, ln1.pt2, extseg1.pt1, extseg1.pt2, NewLine.pt2, false);
          NewLine.pt1 := NewLines[i].pt1;
          NewLine.visible := true;
          NewLines[i] := NewLine;
          intr_linlin (ln2.pt1, ln2.pt2, extseg2.pt1, extseg2.pt2, NewLine.pt1, false);
          NewLine.pt2 := NewLines[i2].pt2;
          NewLines[i2] := NewLine;

          TempPnts := TList<Point>.Create;
          try
            d1 := d1+pi;
            d2 := d2+pi;
            angnormalize(d1);
            angnormalize(d2);
            if d2 < d1 then
              d2 := d2+twopi;
            clockwise := d2 > d1+pi;
            if clockwise then
              swapafloat (d1, d2);
            ArcToPntList (cntr, radius, d1, d2, l^.Divs.Crc, l^.Divs.PthEqDivs, TempPnts);
            if clockwise then
              tempPnts.Reverse;
            for j := TempPnts.count-2 downto 0 do begin
              NewLine.pt1 := TempPnts[j];
              NewLine.pt2 := TempPnts[j+1];
              NewLine.visible := false;   // skip past this line when looking for next line to process
              NewLines.insert(i2, NewLine);
            end;
          finally
            TempPnts.Free;
          end;
          if i2=0 then
            i := NewLines.count
          else
            repeat
              i := i+1
            until (i >= NewLines.count-1) or
                  (NewLines[i].visible);   // skip past fillet lines that have been inserted
          done := true;

        end
        else begin // should never happen, but prevent endless loop if it does
          i := i+1;
          done := true;
        end;
      until done;
    end;
  end;
END;

PROCEDURE OffsetTracePnts;
VAR
  Line, NewLine : LineTyp;
  Lines, NewLines : TList<LineTyp>;
  direction : double;
  i   : integer;
  mov : point;
  noOffs, clockwise : boolean;



BEGIN
  if length(l^.tracepnts) = 0 then begin
    setlength(l^.OffsTracePnts, 0);
    exit;
  end;
  noOffs := not l^.Fillet;
  if noOffs then
    for i := 0 to length(l^.tracepnts)-1 do begin
      if not realequal(l^.tracepnts[i].ofs, zero) then begin
        noOffs := false;
        break;
      end;
    end;
  if (length(l^.tracepnts) < 2) or noOffs  then begin
    setlength (l^.OffsTracePnts, length(l^.TracePnts));
    if length(l^.TracePnts) > 0 then
      for i := 0 to length(l^.TracePnts)-1 do
        l^.OffsTracePnts[i] := l^.TracePnts[i].pnt;
    exit;
  end;

  Lines := TList<LineTyp>.Create;
  NewLines := TList<LineTyp>.Create;
  try
    // populate lines, newlines arrays
    for i := 0 to length(l^.tracepnts)-1 do begin
      if (l^.TraceClosed = NotClosed) and (i = length(l^.tracepnts)-1) and
         PointsEqual (l^.tracepnts[i].pnt, l^.tracepnts[0].pnt) then
        l^.TraceClosed := ClosedByPoints;
      if (l^.TraceClosed in [NotClosed, ForceNotClosed]) and (i = length(l^.tracepnts)-1) then
        break;

      if (not pointsequal (l^.tracepnts[i].pnt, l^.tracepnts[(i+1) mod length(l^.tracepnts)].pnt)) then begin
        Line.pt1 := NewPoint(l^.tracepnts[i].pnt);
        Line.pt2 := NewPoint(l^.tracepnts[(i+1) mod length(l^.tracepnts)].pnt);
        Line.visible := true;
        Lines.add(Line);

        direction := angle (Line.pt1, Line.pt2);
        direction := direction + halfpi;
        cylind_cart (l^.tracepnts[i].ofs , direction, 0.0, mov.x, mov.y, mov.z);

        addpnt (Line.pt1, mov, NewLine.pt1);
        addpnt (Line.pt2, mov, NewLine.pt2);
        NewLine.visible := true;
        NewLines.Add(NewLine);
      end;
    end;

    if (newlines.Count = 1) then begin
      setlength (l^.OffsTracePnts, 2);
      l^.OffsTracePnts[0] := NewPnt2D(newlines[0].pt1);
      l^.OffsTracePnts[1] := NewPnt2D(newlines[0].pt2);
      exit;
    end;

    IntersectNewLines (NewLines, l^.TraceClosed in [ClosedByOption, ClosedByPoints]);
      if NewLines.Count = 0 then
      exit;    // don't think this can ever happen, but just in case ...

    if l^.fillet then
      FilletLines (NewLines);


    if l^.TraceClosed in [NotClosed, ForceNotClosed] then begin
      setlength (l^.OffsTracePnts, NewLines.Count + 1);
      for i := 0 to NewLines.Count-1 do begin
        l^.OffsTracePnts[i] := NewPnt2D(NewLines[i].pt1);
      end;
      l^.OffsTracePnts[NewLines.Count] := NewPnt2D(NewLines[NewLines.Count-1].pt2);
    end
    else begin
      setlength (l^.OffsTracePnts, NewLines.Count);
      for i := 0 to NewLines.Count-1 do begin
        l^.OffsTracePnts[i] := NewPnt2D(NewLines[i].pt1);
      end;
    end;
  finally
    Lines.Free;
    NewLines.Free;
  end;
END;


end.
