unit ConfirmParameters;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, DcadEdit, Vcl.Samples.Spin, CommonStuff, CommonTypes,
  UConstants, URecords, UInterfaces, Settings, Util, Enlargement, Z_Profile, Save, System.UITypes,
  Constants, AdvancedOptions, ProcessProfile, Profile, Vcl.Buttons, Vcl.ExtCtrls;

type
  TfmConfirmParam = class(TForm)
    gbDivisions: TGroupBox;
    lblPflDivs: TLabel;
    lblPthDivs: TLabel;
    gbTwist: TGroupBox;
    Label5: TLabel;
    dceRotationStart: TDcadEdit;
    rbNumTwist: TRadioButton;
    dceNumTwists: TDcadEdit;
    rbDistTwist: TRadioButton;
    dceTwistDist: TDcadEdit;
    gbZ: TGroupBox;
    cbZHite: TCheckBox;
    cbZBase: TCheckBox;
    cbZHiteBase: TCheckBox;
    cbZBaseHite: TCheckBox;
    cbZProfile: TCheckBox;
    cbZVertical: TCheckBox;
    gbEnd: TGroupBox;
    chbCap: TCheckBox;
    rbPerpEnd: TRadioButton;
    rbVertEnd: TRadioButton;
    gbEnl: TGroupBox;
    lblEnlStart: TLabel;
    lblEnlEnd: TLabel;
    dceEnlStart: TDcadEdit;
    dceEnlEnd: TDcadEdit;
    chEnlProfile: TCheckBox;
    gbThick: TGroupBox;
    rbNoThickness: TRadioButton;
    rbThickness: TRadioButton;
    dceThickness: TDcadEdit;
    Button1: TButton;
    btnOK: TButton;
    gbTraceZ: TGroupBox;
    Label6: TLabel;
    dceTraceZ: TDcadEdit;
    lblPth3Divs: TLabel;
    btnAdvancedOpt: TButton;
    lblEnlWarn: TLabel;
    cbMidZ: TCheckBox;
    cbZBasePlus: TCheckBox;
    dceZBasePlus: TDcadEdit;
    rbEnlStretch: TRadioButton;
    rbEnlRepeat: TRadioButton;
    dceRepeatDis: TDcadEdit;
    sbEnlReset: TSpeedButton;
    cbEnlX: TCheckBox;
    cbEnlY: TCheckBox;
    pnlTwistVert: TPanel;
    Label1: TLabel;
    rbNoTwist: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure cbZHiteClick(Sender: TObject);
    procedure cbZHiteBaseClick(Sender: TObject);
    procedure cbZBaseHiteClick(Sender: TObject);
    procedure cbZProfileClick(Sender: TObject);
    procedure btnAdvancedOptClick(Sender: TObject);
    procedure dceThicknessChange(Sender: TObject);
    procedure dceThicknessExit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure rbNoThicknessClick(Sender: TObject);
    procedure cbMidZClick(Sender: TObject);
    procedure cbZBasePlusClick(Sender: TObject);
    procedure EnableEnlProfileStuff (b : boolean);
    procedure sbEnlResetClick(Sender: TObject);
    procedure chEnlProfileClick(Sender: TObject);
    procedure dceTwistDistChange(Sender: TObject);
    procedure dceNumTwistsChange(Sender: TObject);
  private
    { Private declarations }
    svThick : double;
    initialised : boolean;
    procedure ShowDivs;
    procedure EnableEnlargement;
  public
    { Public declarations }
  end;

var
  fmConfirmParam: TfmConfirmParam;

implementation

{$R *.dfm}

procedure TFmConfirmParam.EnableEnlProfileStuff (b : boolean);
begin
  rbEnlStretch.Enabled := b;
  rbEnlRepeat.Enabled := b;
  dceRepeatDis.Enabled := b;
  sbEnlReset.Enabled := b;
  dceEnlStart.Enabled := not b;
  dceEnlEnd.Enabled := not b;
  if b then begin
    dceEnlStart.Text := '';
    dceEnlEnd.Text := '';
  end
  else begin
    dceEnlStart.NumValue := l^.StartEnl;
    dceEnlEnd.NumValue := l^.EndEnl;
  end;
end;

procedure TfmConfirmParam.btnOKClick(Sender: TObject);
const
  SLOPE_ERROR = 'Z Settings: Slope selections can only be used individually (not in combination with other options)';
  MID_ERROR = 'Z Settings: Mid Z can only be used individually (not in combination with other options)';
  BASEPLUS_ERROR = 'Z Settings: Base+ can only be used individually (not in combination with other options)';
  PROFILE_ERROR = 'Z Settings: Z Profile cannot be used in combination with any other options';
  ENLARGE_ERROR = 'Enlargement Factor cannot be less than zero';
  ENLARGE0_ERROR = 'Starting and Ending Enlargements cannot both be zero';
  DIVISIONS_ERROR = 'Circle Divisions must be between 6 and 126 (inclusive)';
  TWIST_ERROR = 'Invalid twist option specified';
var
  atr : attrib;
  ent : entity;
  svEnlXY : tEnlXY;
begin
  if (not (cbEnlX.Checked or cbEnlY.Checked)) and
      gbEnl.Enabled and
      ((dceEnlStart.NumValue <> one) or
       (dceEnlEnd.NumValue <> one) or
       chEnlProfile.checked) then begin
    if MessageDlg(ENL_WARN, TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0)= mrNo then begin
      cbEnlX.SetFocus;
      exit;
    end;
  end;



  if gbTraceZ.visible then begin
    case l^.traceZtype of
      trBase : if not RealEqual (dceTraceZ.NumValue, zbase) then begin
          l^.traceZtype := trSpecified;
          l^.traceZ := dceTraceZ.NumValue
        end;
      trHite : if not RealEqual (dceTraceZ.NumValue, zhite) then begin
          l^.traceZtype := trSpecified;
          l^.traceZ := dceTraceZ.NumValue
        end;
      trUser1 : if not RealEqual (dceTraceZ.NumValue, PGSv.userz1) then begin
          l^.traceZtype := trSpecified;
          l^.traceZ := dceTraceZ.NumValue
        end;
      trUser2 : if not RealEqual (dceTraceZ.NumValue, PGSv.userz2) then begin
          l^.traceZtype := trSpecified;
          l^.traceZ := dceTraceZ.NumValue
        end;
      trSpecified : l^.traceZ := dceTraceZ.NumValue;
    end;
    if l^.traceZtype = trSpecified then
      SaveRl(dhExtTraceZ, l^.traceZ, false);
  end
  else begin
    if cbZHite.checked or cbZBase.Checked or cbZVertical.Checked then begin
      if cbZHiteBase.Checked or cbZBaseHite.Checked then begin
        MessageDlg(SLOPE_ERROR, mtError, [mbOK], 0);
        if cbZHiteBase.Checked  then
          cbZHiteBase.SetFocus
        else
          cbZBaseHite.SetFocus;
        Exit;
      end;
      if cbZProfile.checked then begin
        MessageDlg(PROFILE_ERROR, mtError, [mbOK], 0);
        cbZProfile.SetFocus;
        Exit;
      end;
    end;
    if cbZProfile.checked and (cbZHiteBase.Checked or cbZBaseHite.Checked) then begin
        MessageDlg(PROFILE_ERROR, mtError, [mbOK], 0);
        cbZProfile.SetFocus;
        Exit;
    end;
    if cbZHiteBase.Checked and cbZBaseHite.Checked then begin
      MessageDlg(SLOPE_ERROR, mtError, [mbOK], 0);
      cbZHiteBase.SetFocus;
      Exit;
    end;
  end;

  if (rbDistTwist.Checked and (dceTwistDist.NumValue < 0.1)) then begin
    MessageDlg (TWIST_ERROR, mtError, [mbOK], 0);
    dceTwistDist.SetFocus;
    exit;
  end;
  if (rbNumTwist.Checked and (dceNumTwists.NumValue < 0.00001)) then begin
    MessageDlg (TWIST_ERROR, mtError, [mbOK], 0);
    dceNumTwists.SetFocus;
    exit;
  end;


  if not chEnlProfile.Checked then begin
    if (dceEnlStart.NumValue < 0) or (dceEnlEnd.NumValue < 0) then begin
      MessageDlg(ENLARGE_ERROR, mtError, [mbOK], 0);
      if (dceEnlStart.NumValue < 0) then
        dceEnlStart.SetFocus
      else
        dceEnlEnd.SetFocus;
      Exit;
    end;
    if (dceEnlStart.NumValue <= abszero) and (dceEnlEnd.NumValue <= abszero) then begin
      MessageDlg(ENLARGE0_ERROR, mtError, [mbOK], 0);
      dceEnlStart.SetFocus;
      Exit;
    end;
  end;

  l^.zTreatment := [];
  if cbZHite.Checked then l^.zTreatment := l^.zTreatment + [hite];
  if cbZBase.Checked then l^.zTreatment := l^.zTreatment + [base];
  if cbZHiteBase.Checked then l^.zTreatment := l^.zTreatment + [revslope];
  if cbZBaseHite.Checked then l^.zTreatment := l^.zTreatment + [slope];
  if cbZProfile.Checked then
    l^.zTreatment := l^.zTreatment + [zprofile]
  else if l^.ZProfFormCreated then begin
      SaveFormPos (TForm(l^.fmZProfile));
      l^.fmZProfile.Free;
      l^.ZProfFormCreated := false;
    end;

  if cbMidZ.Checked then l^.zTreatment := l^.zTreatment + [zMid];
  if cbZBasePlus.Checked then begin
    l^.zTreatment := l^.zTreatment + [BasePlus];
    l^.ZBasePlus := dceZBasePlus.NumValue;
  end;
  if cbZVertical.Checked then l^.zTreatment := l^.zTreatment + [verticals];

  SaveZTreatment;

  l^.StartRotation := dceRotationStart.NumValue;
  SaveRl (dhExtStrtRot, l^.StartRotation, false);
  if rbDistTwist.Checked and (dceTwistDist.NumValue > abszero) then begin
    l^.TwistType := DistTwist;
    l^.TwistDistance := dceTwistDist.NumValue;
    SaveRl (dhExtTwistDi, l^.TwistDistance, false);
  end
  else if rbNumTwist.Checked and (dceNumTwists.NumValue > abszero) then begin
    l^.TwistType := NumTwist;
    l^.TwistNumTurns := dceNumTwists.NumValue;
    SaveRl (dhExtTwistNu, l^.TwistNumTurns, false);
  end
  else
    l^.TwistType := NoTwist;
  SaveInt (dhExtTwistTy, ord(l^.TwistType));

  svEnlXY := l^.EnlXY;
  if cbEnlX.Checked and cbEnlY.Checked then
    l^.EnlXY := EnlBoth
  else if cbEnlX.Checked then
    l^.EnlXY := EnlX
  else if cbEnlY.Checked then
    l^.EnlXY := EnlY
  else
    l^.EnlXY := EnlNone;
  if chEnlProfile.checked then begin
    if (dceRepeatDis.NumValue > abszero) then begin
      if not RealEqual (l^.EnlRptDis, dceRepeatDis.NumValue, 0.01) then
        l^.EnlRptDis := dceRepeatDis.NumValue;
      SaveRl(dhEnlPRptDis, l^.EnlRptDis, false);
    end;
    l^.EnlRepeat := rbEnlRepeat.Checked;
    if l^.EnlProfFormCreated then begin
      l^.fmEnlargeProfile.SetRepeat;
      if svEnlXY <> l^.EnlXY then
        l^.fmEnlargeProfile.DrawProfile;
    end;
  end
  else begin
    if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then
      atr_delent (ent, atr);
    setnil (l^.EnlProfile);
    if l^.EnlProfFormCreated then begin
      SaveFormPos (TForm(l^.fmEnlargeProfile));
      l^.fmEnlargeProfile.Free;
      l^.EnlProfFormCreated := false;
    end;
    l^.StartEnl := dceEnlStart.NumValue;
    l^.EndEnl := dceEnlEnd.NumValue;
    SaveInt (dhExtEnlXY, ord(l^.EnlXY), false);
    SaveRl(dhExtEnlStrt, l^.StartEnl, false);
    SaveRl(dhExtEnlEnd, l^.EndEnl, false);
  end;

  l^.Cap := chbCap.Checked;
  SaveBool (dhExtCapEnd, l^.Cap, true);

  l^.VerticalEnds := rbVertEnd.Checked;
  SaveBool (dhExtVertEnd, l^.VerticalEnds, true);
  if rbNoThickness.Checked or (abs(dceThickness.NumValue) <= abszero)  then
    l^.ExtrThick := 0
  else
    l^.ExtrThick := dceThickness.NumValue;
  SaveRl (dhExtShelTk, l^.ExtrThick, false);

  ModalResult := mrOK;
end;

procedure TfmConfirmParam.EnableEnlargement;
begin
  gbEnl.Enabled := not (cbZVertical.Checked and (cbZHite.Checked or cbZBase.Checked));
  lblEnlStart.Visible := gbEnl.Enabled;
  lblEnlEnd.Visible := gbEnl.Enabled;
  dceEnlStart.Visible := gbEnl.Enabled;
  dceEnlEnd.Visible := gbEnl.Enabled;
  chEnlProfile.Visible := gbEnl.Enabled;
  lblEnlWarn.Visible := not gbEnl.Enabled;
  rbEnlStretch.Visible := gbEnl.Enabled;
  rbEnlRepeat.Visible := gbEnl.Enabled;
  dceRepeatDis.Visible := gbEnl.Enabled;
  sbEnlReset.Visible := gbEnl.Enabled;
end;

procedure TfmConfirmParam.Button1Click(Sender: TObject);
begin
  if l^.ExtrThick <> svThick then begin
    l^.ExtrThick := svThick;
    if initialised and l^.ProfileFormCreated and l^.FmProfile.Visible then begin
      ProfileToArray(false);
      l^.FmProfile.DrawProfile;
    end;
  end;
  ModalResult := mrCancel;
end;

procedure TfmConfirmParam.ShowDivs;
begin
  lblPflDivs.Caption := 'Profile: ' + inttostr(l^.Divs.PflCrc);
  if l^.Divs.PflEqDivs then
    lblPflDivs.Caption := lblPflDivs.Caption + ' (Eq)';

  lblPthDivs.Caption := '2D Path: ' + inttostr(l^.Divs.Crc);
  if l^.Divs.PthEqDivs then
    lblPthDivs.Caption := lblPthDivs.Caption + ' (Eq)';
  if (l^.Divs.Ent3D < 3) or (l^.Divs.Ent3D > 128) then
    lblPth3Divs.Caption := '3D Path: DCAD Setting'
  else begin
    lblPth3Divs.Caption := '3D Path: ' + inttostr(l^.Divs.Ent3D);
    if l^.Divs.PthEqDivs then
      lblPth3Divs.Caption := lblPth3Divs.Caption + ' (Eq)';
  end;
end;

procedure TfmConfirmParam.btnAdvancedOptClick(Sender: TObject);
var
  pflChanged, EnlChanged : boolean;
begin
  fmAdvanced := TFmAdvanced.Create(nil);
  try
    if fmAdvanced.ShowModal = mrOK then with fmAdvanced do begin
      l^.WindowFlags[0] := byte (rgPflWdw.ItemIndex);
      l^.WindowFlags[1] := byte (rgEnlWdw.ItemIndex);

      pflChanged := (l^.Divs.PflEqDivs <> chbEqPflDivs.Checked) or
                    (l^.Divs.PflCrc <> sePflCrcArcDivs.Value) or
                    (l^.Divs.PflBez <> cbPflBezSpline.ItemIndex);
      l^.Divs.PflEqDivs := chbEqPflDivs.Checked;
      l^.Divs.PflCrc := sePflCrcArcDivs.Value;
      l^.Divs.PflBez := cbPflBezSpline.ItemIndex;
      if pflChanged then begin
        if l^.ProfileFormCreated then begin
          ProfileToArray (false);
          l^.FmProfile.DrawProfile;
        end;
      end;

      l^.Divs.PthEqDivs := chbEqual.Checked;
      l^.Divs.Crc := seCrcArcDivs.Value;
      l^.Divs.Bez := cbBezSpline.ItemIndex;
      if rbDcadDivs.Checked then
        l^.Divs.Ent3D := 0
      else
        l^.Divs.Ent3D := se3Ddivs.Value;

      EnlChanged := (l^.Divs.EnlEqDivs <> chbEqEnlDivs.Checked) or
                    (l^.Divs.EnlDivs <> seEnlDivs.Value);
      l^.Divs.EnlEqDivs := chbEqEnlDivs.Checked;
      l^.Divs.EnlDivs := seEnlDivs.Value;
      if EnlChanged and l^.EnlProfFormCreated then begin
        l^.fmEnlargeProfile.DrawProfile;
      end;

      ShowDivs;

      SaveInt (dhExtSPflWdw, l^.WindowFlags[_WindowProfile], true);
      SaveInt (dhExtEPflWdw, l^.WindowFlags[_WindowEnlarge], true);
      SaveBool (dhExtPflEqDv, l^.Divs.PflEqDivs);
      SaveInt (dhExtPflDivs, l^.Divs.PflCrc);
      SaveInt (dhExtPflBDiv, l^.Divs.PflBez);
      SaveBool(dhExtEqDivs, l^.Divs.PthEqDivs);
      SaveInt(dhExtCrcDivs,l^.Divs.Crc);
      SaveInt(dhExtBezDivs, l^.Divs.Bez);
      SaveInt(dhExt3DDivs, l^.Divs.Ent3D);
      SaveBool (dhExtEnlEqDv, l^.Divs.EnlEqDivs);
      SaveInt (dhExtEnlDivs, l^.Divs.EnlDivs);
    end;
  finally
    fmAdvanced.Free;
  end;
end;

procedure TfmConfirmParam.cbMidZClick(Sender: TObject);
begin
  if cbMidZ.checked then begin
    cbZHiteBase.Checked := false;
    cbZBaseHite.Checked := false;
    cbZProfile.Checked := false;
    cbZHite.Checked := false;
    cbZBase.Checked := false;
    cbZVertical.Checked := false;
    cbZHiteBase.checked := false;
    cbZBasePlus.Checked := false;
    cbMidZ.Checked := true;
  end;
  EnableEnlargement;
end;

procedure TfmConfirmParam.cbZBaseHiteClick(Sender: TObject);
begin
  if cbZBaseHite.checked then begin
    cbZHiteBase.Checked := false;
    cbZProfile.Checked := false;
    cbZHite.Checked := false;
    cbZBase.Checked := false;
    cbZVertical.Checked := false;
    cbMidZ.checked := false;
    cbZBasePlus.Checked := false;
    cbZBaseHite.checked := true;
  end;
  EnableEnlargement;
end;

procedure TfmConfirmParam.cbZBasePlusClick(Sender: TObject);
begin
  if cbZBasePlus.checked then begin
    cbZHiteBase.Checked := false;
    cbZBaseHite.Checked := false;
    cbZProfile.Checked := false;
    cbZHite.Checked := false;
    cbZBase.Checked := false;
    cbZVertical.Checked := false;
    cbZHiteBase.checked := false;
    cbMidZ.Checked := false;
    cbZBasePlus.Checked := true;
  end;
  EnableEnlargement;
end;

procedure TfmConfirmParam.cbZHiteBaseClick(Sender: TObject);
begin
  if cbZHiteBase.checked then begin
    cbZBaseHite.Checked := false;
    cbZProfile.Checked := false;
    cbZHite.Checked := false;
    cbZBase.Checked := false;
    cbZVertical.Checked := false;
    cbMidZ.Checked := false;
    cbZBasePlus.Checked := false;
    cbZHiteBase.checked := true;
  end;
  EnableEnlargement;
end;

procedure TfmConfirmParam.cbZHiteClick(Sender: TObject);
var
  hc, bc, vc : boolean;
begin
  if cbZHite.Checked or cbZBase.Checked or cbZVertical.checked then begin
    hc := cbZHite.Checked;
    bc := cbZBase.Checked;
    vc := cbZVertical.checked;
    cbZHiteBase.Checked := false;
    cbZBaseHite.Checked := false;
    cbZProfile.Checked := false;
    cbMidZ.Checked := false;
    cbZBasePlus.Checked := false;
    cbZHite.Checked := hc;
    cbZBase.Checked := bc;
    cbZVertical.checked := vc;
  end;
  //gbTwist.visible := not cbZVertical.checked;
  pnlTwistVert.Visible := cbZVertical.checked;
  rbNoTwist.visible := not cbZVertical.checked;
  EnableEnlargement;
end;

procedure TfmConfirmParam.cbZProfileClick(Sender: TObject);
begin
  if cbZProfile.checked then begin
    cbZBaseHite.Checked := false;
    cbZHiteBase.Checked := false;
    cbZHite.Checked := false;
    cbZBase.Checked := false;
    cbZVertical.Checked := false;
    cbMidZ.Checked := false;
    cbZBasePlus.Checked := false;
    cbZProfile.checked := true;
  end;
  EnableEnlargement;
end;

procedure TfmConfirmParam.chEnlProfileClick(Sender: TObject);
begin
  EnableEnlProfileStuff(chEnlProfile.Checked);
end;

procedure TfmConfirmParam.dceNumTwistsChange(Sender: TObject);
begin
  if initialised then
    if dceNumTwists.NumValue <> zero then
      rbNumTwist.Checked := true;
end;

procedure TfmConfirmParam.dceThicknessChange(Sender: TObject);
begin
  rbThickness.Checked := dceThickness.NumValue <> 0;
  if initialised and l^.ProfileFormCreated and l^.FmProfile.Visible then begin
    l^.ExtrThick := dceThickness.NumValue;
    ProfileToArray(false);
    l^.FmProfile.DrawProfile;
  end;
end;

procedure TfmConfirmParam.dceThicknessExit(Sender: TObject);
begin
  if RealEqual (dceThickness.NumValue, abszero) then
    rbNoThickness.Checked := true;
end;

procedure TfmConfirmParam.dceTwistDistChange(Sender: TObject);
begin
  if initialised then
    if dceTwistDist.NumValue > zero then
      rbDistTwist.Checked := true;
end;

procedure TfmConfirmParam.FormCreate(Sender: TObject);
var
  ent : entity;
begin
  initialised := false;
  dceThickness.AllowNegative := true;  // don't know why setting this in the object inspector does not seem to save?
  dceZBasePlus.AllowNegative := true;  // don't know why setting this in the object inspector does not seem to save?
  gbTraceZ.visible := length (l^.OffsTracepnts) > 1;
  if gbTraceZ.visible then begin
    gbZ.Visible := false;
    case l^.traceZtype of
      trBase : dceTraceZ.NumValue := zbase;
      trHite : dceTraceZ.NumValue := zhite;
      trUser1 : dceTraceZ.NumValue := PGSv.userz1;
      trUser2 : dceTraceZ.NumValue := PGSv.userz2;
      trSpecified : dceTraceZ.NumValue := l^.traceZ;
    end;
  end;


  cbZHite.Checked := Hite in l^.zTreatment;
  cbZBase.Checked := Base in l^.zTreatment;
  cbZBaseHite.Checked := Slope in l^.zTreatment;
  cbzHiteBase.Checked := RevSlope in l^.zTreatment;
  cbZProfile.Checked := zProfile in l^.zTreatment;
  cbZProfile.Enabled := cbZProfile.Checked;
  cbMidZ.Checked := zmid in l^.zTreatment;
  cbZBasePlus.Checked := BasePlus in l^.zTreatment;
  dceZBasePlus.NumValue := l^.ZBasePlus;
  cbZVertical.Checked := verticals in l^.zTreatment;
  pnlTwistVert.visible := verticals in l^.zTreatment;
  rbNoTwist.visible := not (verticals in l^.zTreatment);
  EnableEnlargement;

  dceRotationStart.NumValue := l^.StartRotation;

  case l^.TwistType of
    NoTwist : begin
        dceNumTwists.NumValue := 0;
        dceTwistDist.NumValue := 0;
        rbNoTwist.Checked := true;
      end;
    DistTwist : begin
        dceTwistDist.NumValue := l^.TwistDistance;
        dceNumTwists.NumValue := 0;
        rbDistTwist.Checked := true;
      end;
    NumTwist : begin
        dceTwistDist.NumValue := 0;
        dceNumTwists.NumValue := l^.TwistNumTurns;
        rbNumTwist.Checked := true;
      end;
  end;

  ShowDivs;

  dceEnlStart.NumValue := l^.StartEnl;
  dceEnlEnd.NumValue := l^.EndEnl;
  chEnlProfile.checked := (not isnil(l^.EnlProfile)) and ent_get(ent, l^.EnlProfile) and (ent.enttype = entpln);
  chEnlProfile.Enabled := chEnlProfile.Checked;
  EnableEnlProfileStuff (chEnlProfile.Enabled);
  if chEnlProfile.Checked then
    dceRepeatDis.NumValue := l^.EnlRptDis
  else
    dceRepeatDis.Text := '';
  rbEnlStretch.Checked := not l^.EnlRepeat;
  rbEnlRepeat.Checked := l^.EnlRepeat;
  cbEnlX.Checked := l^.EnlXY in [EnlBoth, EnlX];
  cbEnlY.Checked := l^.EnlXY in [EnlBoth, EnlY];

  chbCap.Checked := l^.Cap;
  if l^.VerticalEnds then
    rbVertEnd.Checked := true
  else
    rbPerpEnd.Checked := true;

  svThick := l^.ExtrThick;
  if abs(l^.ExtrThick) > abszero then begin
    rbThickness.Checked := true;
    dceThickness.NumValue := l^.ExtrThick;
  end
  else begin
    rbNoThickness.Checked := true;
    dceThickness.NumValue := 0;
  end;
  initialised := true;
end;

procedure TfmConfirmParam.rbNoThicknessClick(Sender: TObject);
begin
  if initialised and l^.ProfileFormCreated and l^.FmProfile.Visible then begin
    dceThickness.NumValue := zero;
    l^.ExtrThick := zero;
    ProfileToArray(false);
    l^.FmProfile.DrawProfile;
  end;
end;

procedure TfmConfirmParam.sbEnlResetClick(Sender: TObject);
var
  ent : entity;
  atr : attrib;
  minpt, maxpt : point;
begin
  if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then begin
    ent_extent (ent, minpt, maxpt);
    l^.EnlRptDis := abs(maxpt.x - minpt.x);
    dceRepeatDis.NumValue := l^.EnlRptDis;
    SaveRl(dhEnlPRptDis, l^.EnlRptDis, false);
  end;

end;

end.
