unit CommonTypes;

interface

uses URecords, UInterfacesRecords, UConstants, UInterfaces, UDCLibrary,
     Winapi.Windows, System.Generics.Collections, SysUtils;

type
  tDivs = record
    PflEqDivs : boolean;
    PflCrc    : integer;
    PflBez    : integer;
    PthEqDivs : boolean;
    Crc       : integer;
    Bez       : integer;
    Ent3D     : integer;
    Twist     : integer;
    EnlEqDivs : boolean;
    EnlDivs   : integer;
  end;
  PointRec = record
    case byte of
      0 : (p : point);
      1 : (x,y,z,twist : double);
  end;

  EntPntArr = record
    enttyp : byte;
    closed : boolean;   // set true if first and last points of lines/arcs are equal
    zb, zh : double;  // store zbase and zheight for 2d lines/arcs/polylines
    Matrix : modmat;  // store modelling matrix for some 3D types & polylines
    Pnts   : TArray<PointRec>;
    Pnts2  : TArray<point>;        // only used for entity types where height path may not be vertically above base path (e.g. blocks, cones trncones which may be skewed)
    Cntr2  : point;                // only used for cones and truncated cones
    visible: TArray<boolean>;
    Thick  : point;
  end;

  tEnlXY = (EnlBoth, EnlX, EnlY, EnlNone);

  tProfileType = (p_entity, p_symbol, p_circle, p_polygon, p_rectangle);
  tPortion = (complete, tophalf, bottomhalf, lefthalf, righthalf);
  tModifier = (HorizFlip, VertFlip, Rot90, Rot180);
  tModifierSet = set of tModifier;
  tSelectnType = (s_group, s_entity, s_area);
  tProfile = record
    ProfileType : tProfileType;
    case tProfileType of
      p_entity: (adr : entaddr);
      p_symbol: (name : symstr);
      p_circle: (radius : double;
                 cportion : tPortion);
      p_polygon: (numsides : integer;
                  plyradius : double;
                  pportion : tPortion);
      p_rectangle: (width, height : double;
                    rportion : tPortion);
  end;

  tProfileItems = TList<tProfile>;
  tPaths = TList<EntPntArr>;

  tProfiles = record
    Items : tProfileItems;
    refpnt : point;
    Modifier : tModifierSet;
  end;

  tEntities = TList<Entity>;

  tZTreatment = (Hite, Base, Slope, RevSlope, zProfile, verticals, zMid, BasePlus);
  tZTreatmentSet = set of tZTreatment;
  tTwistType = (NoTwist, NumTwist, DistTwist);
  TTraceZtype = (trBase, trHite, trUser1, trUser2, trSpecified);

  EInvalidEntType = class(Exception);

  tProfileArrayItem = packed record
    p : point;
    visible : boolean;
    thicknessLine : boolean;    // used to display thickness lines in grey in the profile window
  end;

  tProfileArray = packed record
    points : TArray<tProfileArrayItem>;
    closed : boolean;
    capped : boolean;
    enttyp : byte;
    colour : byte;
  end;


implementation

end.
