EXTRUDE MACRO FOR DATACAD

To install the Extrude macro please copy both the following files to your normal DataCAD Macros directory:
  Extrude.dmx
  ExtrudeMacro.pdf
The zip file also contains the NonPlanar macro which is used to triangulate twisted sides of extrusions that can be created in some circumstances (see section 4 of the ExtrudeMacro.pdf file). To install the NonPlanar macro copy the following files to your normal DataCAD Macros directory:
  NonPlanar.dmx
  NonPlanar.pdf

Note that there is a bug in DataCAD that potentially allows you to select another macro whilst the Extrude macro is still running, and doing so can cause DataCAD to crash.  This has been fixed in versions of DataCAD from 21.01.04.01 onwards.
Whilst I expect that it would not be common for you to invoke another macro whilst using Extrude, you should consciously avoid doing so if you are using a version of DataCAD prior to 21.01.04.01.

You will be prompted to register your use of this macro the first time you run it.  If you don't want to see this prompt again then click the 'Do not show this message again' checkbox.  Registration will allow me to send you information about future updates to the Extrude macro (it will not add you to my general mailing list, so you won't get general information about other macros).  You can register at any time at the following URL: https://www.dhsoftware.com.au/contactreg.htm

Thank you,
David Henderson



COPYRIGHT NOTICE & LICENCE AGREEMENT:

This software is copyright 2022 by David Henderson.

This software is distributed free of charge and on an "AS IS" basis. To the extent permitted by law, I disclaim all warranties of any kind, either express or implied, including but not limited to, warranties of merchantability, fitness for a particular purpose, and non-infringement of third-party rights.

I will not be liable for any direct, indirect, actual, exemplary or any other damages arising from the use or inability to use this software, even if I have been advised of the possibility of such damages.

Whilst I do solicit contributions toward the cost of developing and distributing my software, such contributions are entirely at your discretion and in no way constitute a payment for the software.

You may distribute this software to others provided that you distribute the complete unaltered zip file provided by me at the dhsoftware.com.au web site, and that you do so free of charge. This includes not charging for any other software, service or product that you associate with this software in such a way as to imply that a purchase is required in order to obtain this software (without limitation, examples of unacceptable charges would be charging for distribution media or for any accompanying software that is on the same media or contained in the same download or distribution file).  If you wish to make any charge at all, you need to obtain specific permission from me.

Whilst it is free (or because of this), I would like and expect that if you can think of any improvements or spot any bugs (or even spelling or formatting errors in the documentation) that you would let me know. Your feedback will help with future development of the macro. 