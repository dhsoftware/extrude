unit EntToPntArr;

interface

uses URecords, UConstants, UInterfaces, CommonStuff, CommonTypes;

function GetBspBezPnts (const ent : entity; var pnts : TArray<point>) : boolean;   overload;
function GetBspBezPnts (const ent : entity; var pntsrec : TArray<PointRec>) : boolean;   overload;

implementation

procedure AddPntToArray (const pnt : point;
                         var pntarr : TArray<point>;
                         var len : integer);
begin
  if length(pntarr) <= len then
    setlength (pntarr, len+128);
  pntarr[len] := pnt;
  len := len+1;
end;

function GetBspBezPnts (const ent : entity; var pnts : TArray<point>) : boolean;
var
  svlyr, templyr : lgl_addr;
  adr : entaddr;
  tempent : entity;
  len : integer;
  mode : mode_type;
  firstsegment : boolean;
begin
  result := ent.enttype in [entbez, entbsp];
  if not result then
    exit;

  setlength (pnts, 128);
  len := 0;

  svlyr := getlyrcurr;
  lyr_init (templyr);
  lyr_set (templyr);
  try
    if ent_get (tempent, ent.addr) then begin
      // explode entity to temp layer
      if ent_explode (tempent, templyr, 1) then begin
        // process created entities
        firstsegment := true;
        mode_init (mode);
        mode_lyr (mode, lyr_curr);
        adr := ent_first (mode);
        while ent_get (tempent, adr) do begin
          { note: observation of exploded ell/bez/bxp entities shows:
                   - points 1 & 2 of created polygons at z-hite going the same direction
                     as the exploded polygons for ellipses
                   - points 3 & 4 at z-hite going in the same direction for bezier & b-spline curves.
                  I have relied on these observations as I expect they will always be the case
                  (although it is not guaranteed and could cause problems) }
          if firstsegment then begin
            if ent.enttype = entell then
              AddPntToArray (tempent.plypnt[1], pnts, len)
            else
              AddPntToArray (tempent.plypnt[3], pnts, len);
            firstsegment := false;
          end;
          if ent.enttype = entell then
            AddPntToArray (tempent.plypnt[2], pnts, len)
          else
            AddPntToArray (tempent.plypnt[4], pnts, len);

          adr := ent_next (tempent, mode);
        end;
      end;
    end;
    setlength (pnts, len);
    result := len > 0;
  finally
    lyr_set (svlyr);
    lyr_clear (templyr);
    lyr_term (templyr);
  end;
end;

function GetBspBezPnts (const ent : entity; var pntsrec : TArray<PointRec>) : boolean;
var
  pnts : TArray<point>;
  i : integer;
begin
  result := GetBspBezPnts (ent, pnts);
  setlength (pntsrec, length(pnts));
  for i := 0 to length(pnts)-1 do begin
    pntsrec[i].p := pnts[i];
    pntsrec[i].twist := zero;
  end;
  setlength(pnts, 0);
end;


end.
