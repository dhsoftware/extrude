object RegistationPrompt: TRegistationPrompt
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Please register this macro'
  ClientHeight = 211
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 449
    Height = 146
    BorderStyle = bsNone
    Color = clBtnFace
    Lines.Strings = (
      
        'By using this macro you agree to the licence conditions laid out' +
        ' in the Help/About screen '
      
        '(and also in the ReadMe.txt file included in the install zip fil' +
        'e).'
      ''
      
        'This macro is free - there is no obligation to pay for it (altho' +
        'ugh if you find it useful a '
      
        'contribution towards the cost of its development and distributio' +
        'n is expected and would '
      'be appreciated!).'
      ''
      
        'Registration is also free, and is recommended as it will allow m' +
        'e to advise you of any '
      'updated versions of the macro.'
      'Please click on the button below to go to the registration page.')
    ReadOnly = True
    TabOrder = 0
    OnEnter = Memo1Enter
  end
  object btnOK: TButton
    Left = 8
    Top = 178
    Width = 360
    Height = 25
    Caption = 'Go to Registration Page'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 374
    Top = 178
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Close'
    TabOrder = 2
    OnClick = btnCancelClick
  end
  object chbNotAgain: TCheckBox
    Left = 8
    Top = 160
    Width = 249
    Height = 17
    Caption = 'Do not show this message again'
    TabOrder = 3
  end
end
