unit Licence;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Constants;

type
  TfmLicence = class(TForm)
    Label1: TLabel;
    memLic: TMemo;
    btnAccept: TButton;
    btnExit: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLicence: TfmLicence;

implementation

{$R *.dfm}

procedure TfmLicence.FormCreate(Sender: TObject);
begin
  MemLic.Lines.Clear;
  MemLic.Lines.Add(LicParagraph1);
  MemLic.Lines.Add('');
  MemLic.Lines.Add(LicParagraph2);
  MemLic.Lines.Add('');
  MemLic.Lines.Add(LicParagraph3);
  MemLic.Lines.Add('');
  MemLic.Lines.Add(LicParagraph4);
  MemLic.Lines.Add('');
  MemLic.Lines.Add(LicParagraph5);
  Caption := 'First Use of ' + ver;
end;

end.
