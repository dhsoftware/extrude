unit AdvancedOptions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Samples.Spin, CommonStuff,
  Vcl.ExtCtrls, UInterfaces, Constants, Enlargement, Profile, Settings;

type
  TfmAdvanced = class(TForm)
    rgPflWdw: TRadioGroup;
    rgEnlWdw: TRadioGroup;
    gbPthDivisions: TGroupBox;
    Label1: TLabel;
    seCrcArcDivs: TSpinEdit;
    rbDcadDivs: TRadioButton;
    rbCustomDivs: TRadioButton;
    se3Ddivs: TSpinEdit;
    Label3: TLabel;
    Label4: TLabel;
    cbBezSpline: TComboBox;
    chbEqual: TCheckBox;
    btnOK: TButton;
    btnCancel: TButton;
    gbPflDivisions: TGroupBox;
    Label2: TLabel;
    Label6: TLabel;
    sePflCrcArcDivs: TSpinEdit;
    cbPflBezSpline: TComboBox;
    chbEqPflDivs: TCheckBox;
    GroupBox1: TGroupBox;
    seEnlDivs: TSpinEdit;
    chbEqEnlDivs: TCheckBox;
    Label5: TLabel;
    btnResetSection: TButton;
    btnResetEnlargement: TButton;
    procedure FormCreate(Sender: TObject);
    procedure cbBezSplineKeyPress(Sender: TObject; var Key: Char);
    procedure btnResetSectionClick(Sender: TObject);
    procedure btnResetEnlargementClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAdvanced: TfmAdvanced;

implementation

{$R *.dfm}

procedure TfmAdvanced.btnResetEnlargementClick(Sender: TObject);
begin
  if not l^.EnlProfFormCreated then
    l^.fmEnlargeProfile := Enlargement.TfmEnlargeProfile.Create(nil);
  l^.fmEnlargeProfile.Top := 0;
  l^.fmEnlargeProfile.Left := 0;
  SaveFormPos (TForm(l^.fmEnlargeProfile));
  if not l^.EnlProfFormCreated then
    l^.fmEnlargeProfile.Free;
end;

procedure TfmAdvanced.btnResetSectionClick(Sender: TObject);
begin
  if not l^.ProfileFormCreated then
    l^.FmProfile := Profile.TFmProfile.Create(nil);
  l^.FmProfile.Top := 0;
  l^.FmProfile.Left := 0;
  SaveFormPos (TForm(l^.fmProfile));
  if not l^.ProfileFormCreated then
    l^.FmProfile.Free;
end;

procedure TfmAdvanced.cbBezSplineKeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key) in [VK_UP, VK_DOWN, VK_TAB] then begin
    // do nothing - allow these keys to be processed
  end
  else begin
    case key of
      'D', 'd' : cbBezSpline.ItemIndex := 0;
      '2' : cbBezSpline.ItemIndex := 1;
      '3' : cbBezSpline.ItemIndex := 2;
      '4' : cbBezSpline.ItemIndex := 3;
      '6' : cbBezSpline.ItemIndex := 4;
      '8' : cbBezSpline.ItemIndex := 5;
    end;
    Key := #0;
  end;
end;


procedure TfmAdvanced.FormCreate(Sender: TObject);
begin
  rgPflWdw.ItemIndex := l^.WindowFlags[_WindowProfile];
  rgEnlWdw.ItemIndex := l^.WindowFlags[_WindowEnlarge];
  chbEqPflDivs.Checked := l^.Divs.PflEqDivs;
  sePflCrcArcDivs.Value := l^.Divs.PflCrc;
  cbPflBezSpline.ItemIndex := l^.Divs.PflBez;
  chbEqual.Checked := l^.Divs.PflEqDivs ;
  seCrcArcDivs.Value := l^.Divs.Crc;
  cbBezSpline.ItemIndex := l^.Divs.Bez;
  if (l^.Divs.Ent3D < 3) or (l^.Divs.Ent3D > 128) then begin
    rbDcadDivs.Checked := true;
    se3Ddivs.Value := PGSv^.circldiv1;
  end
  else begin
    rbCustomDivs.Checked := true;
    se3Ddivs.Value := l^.Divs.Ent3D;
  end;
  chbEqEnlDivs.Checked := l^.Divs.EnlEqDivs;
  seEnlDivs.Value := l^.Divs.EnlDivs;
(*  btnResetSection.Enabled := l^.ProfileFormCreated;
  btnResetEnlargement.Enabled := l^.EnlProfFormCreated; *)
end;

end.
