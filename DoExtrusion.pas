unit DoExtrusion;

interface

uses URecords, UInterfaces, uConstants, CommonStuff, System.Generics.Collections,
     System.Math, Util, Offset, Dialogs, EntToPntArr, CommonTypes, System.UItypes,
     PlnUtil, Constants;


procedure ExtrudePath (pth : EntPntArr);

implementation

type
  tProfileLine3D = Array[1..2] of point;



procedure ExtrudePath (pth : EntPntArr);
type
  tMat = (mat_b, mat_h, mat_v1, mat_v2, mat_s);
  tMatArray = array[mat_b .. mat_s] of modmat;
var
  PflArrNdx, ProfNdx, ProfNxt, PathNdx, h, i, j, k, n : integer;
  rotation, v_rotation, PrevRotation, NextRotation,JointRotation, SlopeAng,
    zEnlargement, yEnlargement, xEnlargement, enlstart, enlend, enlStartX,
    enlStartY, enlEndX, enlEndY, SlopeHite1, SlopeHite2, z1, z2, twist : double;
  modmats : TArray<tMatArray>;
  PflLine3D, ModPflLine1, ModPflLine2, SlopeJointPflLine : tProfileLine3D;
  SlopeJointMat : modmat;
  ent_s, ent_v1, ent_v2, ent_h, ent_b, ent_csb, ent_ceb, ent_csh, ent_ceh, enl_ent : entity;
  pathlen, runninglen, prevrunninglen : double;
  intr1, intr2, p2 : point;
  pnts : pntarr;
  npnts : integer;
  mat : modmat;
  d, e : double;
  ProxNdx: Integer;
  ProfileArray : tProfileArray;
  Ang1, Ang2, enllen : double;
  PflPnts, EnlArray, EnlPnts, TwistPnts : TArray<point>;
  EnlVis : TArray<boolean>;
  PthClosed, DoStartCap, DoEndCap : boolean;
  zProcessing : tZTreatmentSet;
  atr : attrib;
  len, origlen, PthNdx, EnlNdx : integer;
  doProfileEnlargement : boolean;
  twistang, twistdisinc : double;

function VisibleSide : boolean;
begin
  result := true;
  if (pth.enttyp in [entply, entslb, entpln]) and (length(pth.visible) > PathNdx) then
    result := pth.visible[PathNdx];
end;
function PrevVisibleSide : boolean;
var
  ndx : integer;
begin
  result := true;
  if (PathNdx=0) then begin
    if VisibleSide and ((not Pth.Closed) or
                        (((Slope in zProcessing) or (RevSlope in zProcessing)) and
                         not RealEqual(SlopeHite1, SlopeHite2)))
    then begin
      result := false;
      exit;
    end;
  end;

  if (pth.enttyp in [entply, entslb, entpln]) then begin
    ndx := PathNdx-1;
    if ndx < 0 then
      ndx := length(pth.visible)-1;
    if (length(pth.visible) > ndx) then
      result := pth.visible[ndx];
  end
end;

function NextVisibleSide : boolean;
var
  ndx : integer;
begin
  result := true;
  if (PathNdx=length(Pth.Pnts)-2) then begin
    if VisibleSide and ((not Pth.Closed) or
                        (((Slope in zProcessing) or (RevSlope in zProcessing)) and
                         not RealEqual(SlopeHite1, SlopeHite2)))
    then begin
      result := false;
      exit;
    end;
  end;

  if (pth.enttyp in [entply, entslb, entpln]) then begin
    ndx := PathNdx+1;
    if ndx >= length(Pth.Pnts) then
      ndx := 0;
    if (length(pth.visible) > ndx) then
      result := pth.visible[ndx];
  end
end;

function DoStartVertical : boolean;   // returns true if current path side is visible and previous side is not visible
begin
  if (not (verticals in zProcessing)) or RealEqual (SlopeHite1, SlopeHite2) then
    result := false
  else if PathNdx=0 then
    result := pth.visible[0] and not (pth.closed and pth.visible[length(pth.Pnts)-1])
  else
    result := pth.visible[PathNdx] and not pth.visible[PathNdx-1];
end;

function DoEndVertical : boolean;   // returns true if current path side is visible and next side is not visible
begin
  if (not (verticals in zProcessing)) or RealEqual (SlopeHite1, SlopeHite2) then
    result := false
  else if PathNdx=length(pth.Pnts)-2 then
    result := pth.visible[PathNdx] and not (pth.closed and pth.visible[0])
  else
    result := pth.visible[PathNdx] and not pth.visible[PathNdx+1];
end;

begin
  zProcessing := l^.zTreatment;
  if pth.enttyp = 0 then  // trace points
    zProcessing := [hite]
  else if pth.enttyp in [byte(entln3), byte(entply), byte(entar3)] then begin
    if zProcessing = [verticals] then
      exit;       // do not proces verticals for these entities which have no height
    zProcessing := [base]  // base,height,slope all have same result.  Set just base to avoid duplicates
  end
  else if pth.enttyp in [byte(entcon), byte(enttrn)] then
      zProcessing := zProcessing - [slope, revslope];   // have not yet coded slopes for these entities, so just ignore this processing for these entities for the time being

  if [slope, revslope] * zProcessing = [] then
    PthClosed := pth.closed
  else
    PthClosed := false;


 ent_csb.plynpnt := 0;    // initialise length of all cap entities to zero (length will be set if they need to be created)
 ent_ceb.plynpnt := 0;
 ent_csh.plynpnt := 0;
 ent_ceh.plynpnt := 0;


  case pth.enttyp of
    {$region 'entlin, entply, entslb'}
    0, entlin, entply, entslb, entpln, enttrn, entblk, entcon, entln3, entar3, entcyl, entcrc, entarc, entell, entbsp, entbez :
      begin
        case pth.enttyp of
          0, entlin, entpln, entblk, entcon, enttrn, entln3, entcyl, entcrc, entarc, entslb, entell, entbez, entbsp : begin
              SlopeHite1 := pth.zb;
              SlopeHite2 := pth.zh;
            end
          else begin
              SlopeHite1 := 0;
              SlopeHite2 := 0;
            end;
        end;

        SlopeAng := zero;
        pathlen := zero;
        for i := 0 to length(pth.Pnts)-2 do
          pathlen := pathlen + distance (pth.pnts[i].p, pth.Pnts[i+1].p);
        if (Slope in zProcessing) or (RevSlope in zProcessing) then begin
          SlopeAng := -ArcTan2 (SlopeHite2 - SlopeHite1, pathlen);
          if RevSlope in zProcessing then begin
            SlopeAng := -SlopeAng;
            swapaFloat (SlopeHite1, SlopeHite2);
          end;
        end;

        {$region 'Enlargement Profile'}
        doProfileEnlargement := (not isnil (l^.EnlProfile)) and (l^.EnlXY <> EnlNone) and
                                (pth.enttyp in [0, entlin, entcrc, entbez, entarc, entell, entln3, entbsp, entar3, entply, entpln]) and
                                not ((verticals in l^.zTreatment) and ((base in l^.zTreatment) or (hite in l^.zTreatment)));
        if doProfileEnlargement  and not (l^.zTreatment = [verticals]) then  // verticals only are handled separately
        if ent_get (enl_ent, l^.EnlProfile) and (enl_ent.enttype = entpln) and atr_entfind (enl_ent, dhExtEnlProf, atr) then begin
          EnlArray := PlnToPntArray (enl_ent, l^.Divs.Crc, l^.Divs.PthEqDivs);
          if l^.EnlRevers then
            for i := 0 to length(enlArray)-1 do begin
              enlArray[i].x := -enlArray[i].x;
            end;
          if EnlArray[0].x > EnlArray[length(EnlArray)-1].x then
            ReversePnts(EnlArray);
          // move EnlArray to start at X=0, and set Z for each point to be the actual enlargement factor
          for i := length(enlArray)-1 downto 0 do begin
            EnlArray[i].x := EnlArray[i].x - EnlArray[0].x;
            EnlArray[i].z := EnlArray[i].y / atr.rl;
          end;
          // scale or repeatArray to match length of path
          enllen := EnlArray[length(EnlArray)-1].x;
          if l^.EnlRepeat then begin
            if not realequal (l^.EnlRptDis, enllen) then begin
              for i := 0 to length(enlArray)-1 do begin
                EnlArray[i].x := EnlArray[i].x * l^.EnlRptDis/enllen;
              end;
              enllen := EnlArray[length(EnlArray)-1].x;
            end;
            len := length(EnlArray);
            origlen := len;
            setlength (EnlArray, ceil (pathlen/enllen)*len);
            while EnlArray[len-1].x < pathlen do begin
              EnlArray[len].x := EnlArray[len-origlen+1].x + enllen;
              EnlArray[len].z := EnlArray[len-origlen+1].z;            // note : y not used in subsequent processing, so don't bother setting it
              len := len+1;
            end;
            setlength (EnlArray, len);
            if RealEqual (EnlArray[length(EnlArray)-1].x, pathlen) then
              EnlArray[length(EnlArray)-1].x := pathlen
            else if EnlArray[length(EnlArray)-1].x > pathlen then begin
              EnlArray[length(EnlArray)-1].z :=
                  EnlArray[length(EnlArray)-2].z +
                  (pathlen - EnlArray[length(EnlArray)-2].x)/(EnlArray[length(EnlArray)-1].x - EnlArray[length(EnlArray)-2].x)
                  * (EnlArray[length(EnlArray)-1].z - EnlArray[length(EnlArray)-2].z);
              EnlArray[length(EnlArray)-1].x := pathlen;
            end;
          end
          else for i := 1 to length(enlArray)-1 do begin
            EnlArray[i].x := EnlArray[i].x * pathlen/enllen;
          end;

          // insert enlargement points into path
          setlength (EnlPnts, length(pth.Pnts) + length(EnlArray));
          setlength (EnlVis, length(EnlPnts));
          EnlPnts[0].x := pth.Pnts[0].x;
          EnlPnts[0].y := pth.Pnts[0].y;
          EnlPnts[0].z := EnlArray[0].z;
          if length (pth.visible) > 0 then
            EnlVis[0] := pth.visible[0]
          else
            EnlVis[0] := true;
          len := 1;
          PthNdx := 1;
          EnlNdx := 1;
          runninglen := zero;
          while PthNdx < length(Pth.Pnts) do begin
            prevrunninglen := runninglen;
            runninglen := runninglen + distance (Pth.Pnts[PthNdx-1].p, Pth.Pnts[PthNdx].p);
            while EnlArray[EnlNdx].x < runninglen do begin
              d := (EnlArray[EnlNdx].x-prevrunninglen)/(runninglen - prevrunninglen);
              EnlPnts[len].x := Pth.Pnts[PthNdx-1].x + (Pth.Pnts[PthNdx].x - Pth.Pnts[PthNdx-1].x)*d;
              EnlPnts[len].y := Pth.Pnts[PthNdx-1].y + (Pth.Pnts[PthNdx].y - Pth.Pnts[PthNdx-1].y)*d;
              EnlPnts[len].z := EnlArray[EnlNdx].z;
              if length (Pth.visible) > (PthNdx-1) then
                EnlVis[len] := Pth.visible[PthNdx-1]
              else
                EnlVis[len] := true;
              EnlNdx := EnlNdx+1;
              len := len+1;
            end;
            if RealEqual(runninglen, EnlArray[EnlNdx].x) then begin
              EnlPnts[len].x := Pth.Pnts[PthNdx].x;
              EnlPnts[len].y := Pth.Pnts[PthNdx].y;
              EnlPnts[len].z := EnlArray[EnlNdx].z;
              if length (Pth.visible) > PthNdx then
                EnlVis[len] := Pth.visible[PthNdx]
              else
                EnlVis[len] := true;
              EnlNdx := EnlNdx+1;
              len := len + 1;
            end
            else begin
              EnlPnts[len].x := Pth.Pnts[PthNdx].x;
              EnlPnts[len].y := Pth.Pnts[PthNdx].y;
              d := (runninglen - EnlArray[EnlNdx-1].x)/(EnlArray[EnlNdx].x - EnlArray[EnlNdx-1].x);
              EnlPnts[len].z := EnlArray[EnlNdx-1].z + (EnlArray[EnlNdx].z - EnlArray[EnlNdx-1].z)*d;
              if length (Pth.visible) > PthNdx   then
                EnlVis[len] := Pth.visible[PthNdx]
              else
                EnlVis[len] := true;
              len := len+1;
            end;
            PthNdx := PthNdx+1;
          end;
          setlength (Pth.Pnts, len);
          setlength (Pth.visible, len);
          for i := 0 to len-1 do begin
            Pth.Pnts[i].p := EnlPnts[i];
            Pth.visible[i] := EnlVis[i];
          end;
          setlength (EnlPnts, 0);
          setlength (EnlVis, 0);
        end
        else begin
          doProfileEnlargement := false;
        end;
        {$endregion 'Enlargement Profile'}

        {$region 'Add Twist Points'}
        if (l^.TwistType in [DistTwist, NumTwist]) and not (verticals in zProcessing) then begin
          if l^.TwistType = DistTwist then
            twistdisinc := l^.TwistDistance / l^.Divs.Twist
          else if l^.TwistType = NumTwist then
            twistdisinc := pathlen / (l^.TwistNumTurns * l^.Divs.Twist);

          for i := length(pth.Pnts)-2 downto 0 do begin  // insert points into segments as required
            p2 := pth.Pnts[i+1].p;
            d := distance (pth.Pnts[i].p, p2);
            d := d/twistdisinc;
            if realequal (d, round(d), 0.009) then
              n := round(d)
            else
              n := ceil(d);
            if n > 1 then begin
              setlength (TwistPnts, n-1);
              for j := 0 to n-2 do begin
                TwistPnts[j].x := pth.Pnts[i].x + (p2.x - pth.Pnts[i].x) * (j+1)/n;
                TwistPnts[j].y := pth.Pnts[i].y + (p2.y - pth.Pnts[i].y) * (j+1)/n;
                TwistPnts[j].z := pth.Pnts[i].z + (p2.z - pth.Pnts[i].z) * (j+1)/n;
              end;
              setlength (pth.Pnts, length(pth.Pnts) + n-1);
              for j := length(pth.pnts)-1 downto i+n do
                pth.pnts[j] := pth.pnts[j-n+1];
              for j := 1 to n-1 do
                pth.pnts[i+j].p := TwistPnts[j-1];
            end;
          end;

          runninglen := zero;
          for i := 0 to length(pth.Pnts)-1 do begin //calculate twist rotation for each point
            if i > 0 then
              runninglen := runninglen + distance (pth.Pnts[i-1].p, pth.Pnts[i].p);
            if l^.TwistType = NumTwist then
              pth.Pnts[i].twist := runninglen/pathlen * l^.TwistNumTurns * TwoPi
            else
              pth.Pnts[i].twist := (runninglen - l^.TwistDistance*floor(runninglen/l^.TwistDistance))/l^.TwistDistance * TwoPi;
            if not l^.ClkwiseRot then
              pth.Pnts[i].twist := -pth.Pnts[i].twist;
          end;
        end;   //(l^.TwistType in [DistTwist, NumTwist]) and not (verticals in zProcessing)
        {$endregion 'Add Twist Points'}


        setlength (modmats, length(pth.Pnts));

        {$region 'set up modelling matrix for various z treatments'}
        for i := 0 to length (pth.Pnts)-1 do begin
          if (i=0) and not PthClosed then begin
            rotation := ArcTan2 (pth.Pnts[i+1].y - pth.Pnts[i].y, pth.Pnts[i+1].x - pth.Pnts[i].x);
            yEnlargement := one;
            zEnlargement := one;
          end
          else if (i=length(pth.Pnts)-1) and not PthClosed then begin
            rotation := ArcTan2 (pth.Pnts[i].y - pth.Pnts[i-1].y, pth.Pnts[i].x - pth.Pnts[i-1].x);
            yEnlargement := one;
            zEnlargement := one;
          end
          else begin
            if i=0 then
              prevRotation := ArcTan2 (pth.Pnts[i].y - pth.Pnts[length (pth.Pnts)-2].y,     // note that for closed paths the first and last points in the path are equal,
                                       pth.Pnts[i].x - pth.Pnts[length (pth.Pnts)-2].x)     // hence we look to the penultimate point as the previous point to the first point.
            else
              prevRotation := ArcTan2 (pth.Pnts[i].y - pth.Pnts[i-1].y, pth.Pnts[i].x - pth.Pnts[i-1].x);
            if i = length (pth.Pnts)-1 then
              nextRotation := ArcTan2 (pth.Pnts[1].y - pth.Pnts[i].y, pth.Pnts[1].x - pth.Pnts[i].x)   // note that for closed paths the first and last points in the path are equal,
            else                                                                                       // hende we look to the second point as the next point from the last one.
              nextRotation := ArcTan2 (pth.Pnts[i+1].y - pth.Pnts[i].y, pth.Pnts[i+1].x - pth.Pnts[i].x);
            angnormalize (prevrotation);
            angnormalize(nextrotation);
            rotation := (prevrotation + nextrotation)/2;
            if RealEqual (cos (rotation-prevRotation), zero) then begin
              if not ErrorShown then
                MessageDlg ('Paths that double back on themselves will not be processed', mtWarning, [mbOK], 0);
              ErrorShown := true;
              Exit;
            end;
            yEnlargement := 1 / cos (rotation-prevRotation);
          end;
          if doProfileEnlargement then begin
            if l^.EnlXY in [EnlBoth, EnlX] then
              yEnlargement := yEnlargement * pth.Pnts[i].z;    // z used for enlargement
            if l^.EnlXY in [EnlBoth, EnlY] then begin
              xEnlargement := pth.Pnts[i].z;
              zEnlargement := pth.Pnts[i].z;
            end
            else begin
              xEnlargement := one;
              zEnlargement := one;
            end;
          end
          else begin
            xEnlargement := one;
            zEnlargement := one;
          end;

          setscale (modmats[i][mat_b], xEnlargement, yEnlargement, zEnlargement);
          if (l^.TwistType in [DistTwist, NumTwist]) and not (verticals in zProcessing) then begin
            catrotate (modmats[i][mat_b], pth.Pnts[i].twist, x);
          end;
          catrotate (modmats[i][mat_b], rotation, z);
          if verticals in zProcessing then begin
            if zProcessing * [Base, Hite] = [Hite] then begin    // If Verticals, Base, AND Hite are all set then
              setrotate (modmats[i][mat_v1], -halfpi, y);          // the base extrusion is processed the right way up
              setrotate (modmats[i][mat_v2], halfpi, y);           // but the hite is processed upside down so that the
            end                                                    // extrusion wraps around the perimieter of what is
            else if base in zProcessing then begin               // effectively a vertical polygon.
              setrotate (modmats[i][mat_v1], halfpi, y);           // If Base is not set then hite is processed the right
              setrotate (modmats[i][mat_v2], -halfpi, y);          // way up, so verticals are inverted to what they would
            end                                                    // be if base was not set
            else begin
              setident (modmats[i][mat_v1]);
            end;
            catrotate (modmats[i][mat_v1], rotation, z);
            catrotate (modmats[i][mat_v2], rotation, z);
            if Hite in zProcessing then begin
              if base in zProcessing then begin
                setscale (modmats[i][mat_h], xEnlargement, yEnlargement, zEnlargement);
                catrotate (modmats[i][mat_h], pi, y);
                catrotate (modmats[i][mat_h], rotation, z);
              end
              else begin
                setscale (modmats[i][mat_h], xEnlargement, yEnlargement, zEnlargement);
                catrotate (modmats[i][mat_h], rotation, z);
              end;
            end;
          end
          else if hite in zProcessing then begin
            setscale (modmats[i][mat_h], xEnlargement, yEnlargement, zEnlargement);
            if (l^.TwistType in [DistTwist, NumTwist]) and not (verticals in zProcessing) then begin
              catrotate (modmats[i][mat_h], pth.Pnts[i].twist, x);
            end;
            catrotate (modmats[i][mat_h], rotation, z);
          end;
          if (Slope in zProcessing) or (RevSlope in zProcessing) then begin
            if (l^.TwistType in [DistTwist, NumTwist]) and not (verticals in zProcessing) then begin
              setrotate (modmats[i][mat_s], pth.Pnts[i].twist, x);
            end
            else
              setident (modmats[i][mat_s]);
            if ((i = 0) or (i = length (pth.Pnts)-1)) and not l^.VerticalEnds then begin
              catrotate (modmats[i][mat_s], SlopeAng, y);
              catrotate (modmats[i][mat_s], rotation, z);
            end
            else begin
              zEnlargement := 1 / cos(SlopeAng);
              catscale (modmats[i][mat_s], xEnlargement, yEnlargement , zEnlargement);
              catrotate (modmats[i][mat_s], rotation, z);
            end;
          end;
        end;
        {$endregion 'set up modelling matrix for various z treatments'}

        for PflArrNdx := 0 to l^.ProfileArrays.Count-1 do begin
          ProfileArray.closed := l^.ProfileArrays[PflArrNdx].closed;
          ProfileArray.capped := l^.ProfileArrays[PflArrNdx].capped;
          ProfileArray.enttyp := l^.ProfileArrays[PflArrNdx].enttyp;
          ProfileArray.points := TArray<tProfileArrayItem>.create();
          setlength (ProfileArray.points, length(l^.ProfileArrays[PflArrNdx].points));
          for i := 0 to length(ProfileArray.points)-1 do begin
            ProfileArray.points[i] := l^.ProfileArrays[PflArrNdx].points[i];
          end;
          {$region 'Set up profile to desired orientation'}
          if PointsEqual (ProfileArray.points[0].p, ProfileArray.points[length(ProfileArray.points)-1].p) then begin
            setlength(ProfileArray.points, length(ProfileArray.points)-1);
            ProfileArray.closed := true;
          end;
          if zProcessing = [verticals] then
            { No need to change - it is already set up as horizontal }
          else for ProfNdx := 0 to length (ProfileArray.points)-1 do begin
            { Set up in vertical plane parallel to y axis }
            ProfileArray.points[ProfNdx].p.z := ProfileArray.points[ProfNdx].p.y;
            ProfileArray.points[ProfNdx].p.y := -ProfileArray.points[ProfNdx].p.x;
            ProfileArray.points[ProfNdx].p.x := zero;
          end;
          {$endregion 'Set up profile to desired orientation'}

          runninglen := zero;
          for PathNdx := 0 to length (pth.Pnts) - 1 do begin
            if (zProcessing = [verticals]) then begin
              {$region 'verticals ONLY.  Just stamp the profile at each corner'}
              if ((PathNdx < length (pth.Pnts) - 1) or (not PthClosed)) and
               (not RealEqual (SlopeHite1, SlopeHite2)) and
               (VisibleSide or PrevVisibleSide)
              then begin
                if pth.enttyp in [entcon, entcyl, enttrn, entcrc, entarc] then begin
                  {$B-}
                  if (length(pth.pnts2) > PathNdx) and not PointsEqual(pth.pnts[PathNdx].p, pth.pnts2[PathNdx], false) then
                    Rotation := arctan2 (pth.pnts[PathNdx].y - pth.pnts2[PathNdx].y , pth.pnts[Pathndx].x - pth.pnts2[PathNdx].x) + halfpi
                  else
                    Rotation := arctan2 (pth.pnts[PathNdx].y - pth.Cntr2.y , pth.pnts[Pathndx].x - pth.Cntr2.x) + halfpi
                end
                else if (pth.enttyp = entblk) and (length(pth.pnts2) > PathNdx) and not PointsEqual(pth.pnts[PathNdx].p, pth.pnts2[PathNdx], false) then
                  Rotation := arctan2 (pth.pnts[PathNdx].y - pth.pnts2[PathNdx].y , pth.pnts[Pathndx].x - pth.pnts2[PathNdx].x) + halfpi
                else if (not PthClosed) and ((PathNdx = 0) or (PathNdx = length(pth.Pnts)-1)) then begin
                  if PathNdx = 0 then
                    Rotation := ArcTan2 (pth.Pnts[0].y - pth.Pnts[1].y, pth.Pnts[0].x - pth.Pnts[1].x)
                  else
                    Rotation := ArcTan2 (pth.Pnts[PathNdx-1].y - pth.Pnts[PathNdx].y, pth.Pnts[PathNdx-1].x - pth.Pnts[PathNdx].x);
                end
                else begin
                  ang1 := ArcTan2 (pth.Pnts[(pathNdx-1+length(pth.Pnts)) mod length(pth.Pnts)].y - pth.Pnts[PathNdx].y,
                                  pth.Pnts[(pathNdx-1+length(pth.Pnts)) mod length(pth.Pnts)].x - pth.Pnts[PathNdx].x);
                  ang2 := ArcTan2 (pth.Pnts[PathNdx].y - pth.Pnts[(PathNdx+1) mod length(pth.Pnts)].y,
                                  pth.Pnts[PathNdx].x - pth.Pnts[(PathNdx+1) mod length(pth.Pnts)].x);
                  Rotation := ang1 + (ang2-ang1)/2;
                  angnormalize (Rotation);
                  cylind_cart (1, rotation, 0, intr1.x, intr1.y, intr1.z);
                  addpnt (intr1, pth.Pnts[PathNdx].p, intr1);
                  if crossz (pth.Pnts[(pathNdx-1+length(pth.Pnts)) mod length(pth.Pnts)].p,  pth.Pnts[PathNdx].p, intr1) < 0 then
                    Rotation := Rotation + Pi;

                end;
                if (pth.enttyp in [entcon, enttrn]) and not PointsEqual (pth.Pnts[pathNdx].p, pth.pnts2[pathNdx], false) then begin
                  // set enlargement of profile to cater for slope of the extrusion
                  ang1 := arctan2 (pth.zh - pth.zb, distance (pth.Pnts[pathNdx].p, pth.pnts2[pathNdx]));
                  setscale (mat,1, 1/sin(ang1), 1);
                end
                else
                  setident (mat);

                catrotate (mat, Rotation, z);
                setlength (PflPnts, length(ProfileArray.points));
                for ProfNdx := 0 to length(ProfileArray.points)-1 do
                  xformpt (ProfileArray.points[ProfNdx].p, mat, PflPnts[ProfNdx]);



                if doProfileEnlargement then
                if ent_get (enl_ent, l^.EnlProfile) and (enl_ent.enttype = entpln) and atr_entfind (enl_ent, dhExtEnlProf, atr) then begin
                  EnlArray := PlnToPntArray (enl_ent, l^.Divs.Crc, l^.Divs.PthEqDivs);
                  if l^.EnlRevers then
                    for i := 0 to length(enlArray)-1 do begin
                      enlArray[i].x := -enlArray[i].x;
                    end;
                  if EnlArray[0].x > EnlArray[length(EnlArray)-1].x then
                    ReversePnts(EnlArray);
                  // move EnlArray to start at X=0, and set Z for each point to be the actual enlargement factor
                  for i := length(enlArray)-1 downto 0 do begin
                    EnlArray[i].x := EnlArray[i].x - EnlArray[0].x;
                    EnlArray[i].z := EnlArray[i].y / atr.rl;
                  end;
                  // scale or repeatArray to match length of path
                  pathlen := pth.zh-pth.zb;
                  enllen := EnlArray[length(EnlArray)-1].x;
                  if l^.EnlRepeat then begin
                    len := length(EnlArray);
                    origlen := len;
                    setlength (EnlArray, ceil (pathlen/enllen)*len);
                    while EnlArray[len-1].x < pathlen do begin
                      EnlArray[len].x := EnlArray[len-origlen+1].x + enllen;
                      EnlArray[len].z := EnlArray[len-origlen+1].z;            // note : y not used in subsequent processing, so don't bother setting it
                      len := len+1;
                    end;
                    setlength (EnlArray, len);
                    if RealEqual (EnlArray[length(EnlArray)-1].x, pathlen) then
                      EnlArray[length(EnlArray)-1].x := pathlen
                    else if EnlArray[length(EnlArray)-1].x > pathlen then begin
                      EnlArray[length(EnlArray)-1].z :=
                          EnlArray[length(EnlArray)-2].z +
                          (pathlen - EnlArray[length(EnlArray)-2].x)/(EnlArray[length(EnlArray)-1].x - EnlArray[length(EnlArray)-2].x)
                          * (EnlArray[length(EnlArray)-1].z - EnlArray[length(EnlArray)-2].z);
                      EnlArray[length(EnlArray)-1].x := pathlen;
                    end;
                  end
                  else for i := 1 to length(enlArray)-1 do begin
                    EnlArray[i].x := EnlArray[i].x * pathlen/enllen;
                  end;
                end
                else begin
                  doProfileEnlargement := false;
                end;


                if doProfileEnlargement then for PthNdx := 0 to length(EnlArray)-2 do begin
                  for ProfNdx := 0 to length(ProfileArray.points)-1 do
                    if ((ProfNdx < length(ProfileArray.points)-1) or (ProfileArray.closed)) and
                        ProfileArray.points[ProfNdx].visible  then begin
                      if l^.enlXY in [EnlBoth, EnlX] then begin
                        enlStartX := EnlArray[PthNdx].z;
                        enlEndX := EnlArray[PthNdx+1].z;
                      end
                      else begin
                        enlStartX := one;
                        enlEndX := one;
                      end;
                      if l^.enlXY in [EnlBoth, EnlY] then begin
                        enlstartY := EnlArray[PthNdx].z;
                        enlEndY := EnlArray[PthNdx+1].z;
                      end
                      else begin
                        enlStartY := one;
                        enlEndY := one;
                      end;
                      ent_init(ent_v1, entply);
                      ent_v1.plynpnt := 4;
                      ent_v1.plypnt[1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb+EnlArray[PthNdx].x),
                                                    NewPoint (PflPnts[ProfNdx].x * enlStartY,
                                                              PflPnts[ProfNdx].y * enlStartX, zero));
                      ent_v1.plypnt[2] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb+EnlArray[PthNdx+1].x),
                                                    NewPoint (PflPnts[ProfNdx].x * enlEndY,
                                                              PflPnts[ProfNdx].y * enlEndX, zero));
                      ProfNxt := ProfNdx+1;
                      if ProfNxt > length(ProfileArray.points)-1 then ProfNxt := 0;
                      ent_v1.plypnt[3] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb+EnlArray[PthNdx+1].x),
                                                    NewPoint (PflPnts[ProfNxt].x * enlEndY,
                                                              PflPnts[ProfNxt].y * enlEndX, zero));
                      ent_v1.plypnt[4] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb+EnlArray[PthNdx].x),
                                                    NewPoint (PflPnts[ProfNxt].x * enlStartY,
                                                              PflPnts[ProfNxt].y * enlStartX, zero));
                      if pth.enttyp in PathEntsWithMatrix then for i := 1 to 4 do begin
                        xformpt (ent_v1.plypnt[i], pth.Matrix, ent_v1.plypnt[i]);
                      end;
                      ent_add(ent_v1);
                      ent_draw (ent_v1, drmode_white);
                    end;
                end //profile enlargement
                else for ProfNdx := 0 to length(ProfileArray.points)-1 do begin
                  if ((ProfNdx < length(ProfileArray.points)-1) or (ProfileArray.closed)) and
                      ProfileArray.points[ProfNdx].visible
                  then begin
                    if l^.EnlXY in [enlBoth, enlX] then begin
                      enlStartX := l^.StartEnl;
                      enlEndX := l^.EndEnl;
                    end
                    else begin
                      enlStartX := one;
                      enlEndX := one;
                    end;
                    if l^.EnlXY in [enlBoth, enlY] then begin
                      enlStartY := l^.StartEnl;
                      enlEndY := l^.EndEnl;
                    end
                    else begin
                      enlStartY := one;
                      enlEndY := one;
                    end;
                    ent_init(ent_v1, entply);
                    ent_v1.plynpnt := 4;
                    ent_v1.plypnt[1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb),
                                                  NewPoint (PflPnts[ProfNdx].x * enlStartX,
                                                            PflPnts[ProfNdx].y * enlStartY,
                                                            PflPnts[ProfNdx].z * enlStartY));
                    if length (pth.Pnts2) > 0 then
                      ent_v1.plypnt[2] := AddPoints(NewPoint (pth.Pnts2[PathNdx], pth.zh),
                                                    NewPoint (PflPnts[ProfNdx].x * enlEndX,
                                                              PflPnts[ProfNdx].y * enlEndY,
                                                              PflPnts[ProfNdx].z * enlEndY))
                    else if pth.enttyp = entcon then
                      ent_v1.plypnt[2] := AddPoints(NewPoint (pth.Cntr2, pth.zh),
                                                    NewPoint (PflPnts[ProfNdx].x * enlEndX,
                                                              PflPnts[ProfNdx].y * enlEndY,
                                                              PflPnts[ProfNdx].z * enlEndY))
                    else
                      ent_v1.plypnt[2] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zh),
                                                    NewPoint (PflPnts[ProfNdx].x * enlEndX,
                                                              PflPnts[ProfNdx].y * enlEndY,
                                                              PflPnts[ProfNdx].z * enlEndY));
                    ProfNxt := ProfNdx+1;
                    if ProfNxt > length(ProfileArray.points)-1 then ProfNxt := 0;
                    if length (pth.Pnts2) > 0 then
                      ent_v1.plypnt[3] := AddPoints(NewPoint (pth.Pnts2[PathNdx], pth.zh),
                                                    NewPoint (PflPnts[ProfNdx].x * enlEndX,
                                                              PflPnts[ProfNdx].y * enlEndY,
                                                              PflPnts[ProfNdx].z * enlEndY))
                    else if pth.enttyp = entcon then
                      ent_v1.plypnt[3] := AddPoints(NewPoint (pth.Cntr2, pth.zh),
                                                    NewPoint (PflPnts[ProfNdx+1].x * enlEndX,
                                                              PflPnts[ProfNdx+1].y * enlEndY,
                                                              PflPnts[ProfNdx+1].z * enlEndY))
                    else
                      ent_v1.plypnt[3] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zh),
                                                    NewPoint (PflPnts[ProfNxt].x * enlEndX,
                                                              PflPnts[ProfNxt].y * enlEndY,
                                                              PflPnts[ProfNxt].z * enlEndY));
                    ent_v1.plypnt[4] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb),
                                                  NewPoint (PflPnts[ProfNxt].x * enlStartX,
                                                            PflPnts[ProfNxt].y * enlStartY,
                                                            PflPnts[ProfNxt].z * enlEndY));
                    if pth.enttyp in PathEntsWithMatrix then for i := 1 to 4 do begin
                      xformpt (ent_v1.plypnt[i], pth.Matrix, ent_v1.plypnt[i]);
                    end;
                    ent_add(ent_v1);
                    ent_draw (ent_v1, drmode_white);
                  end;
                end;
                if l^.Cap and (length (PflPnts) <= 256) then begin
                  if doProfileEnlargement then begin
                    if l^.EnlXY in [enlBoth, enlX] then begin
                      enlStartX := EnlArray[0].z;
                      enlEndX := EnlArray[length(EnlArray)-1].z;
                    end
                    else begin
                      enlStartX := one;
                      enlEndX := one;
                    end;
                    if l^.EnlXY in [enlBoth, enlY] then begin
                      enlStartY := EnlArray[0].z;
                      enlEndY := EnlArray[length(EnlArray)-1].z;
                    end
                    else begin
                      enlStartY := one;
                      enlEndY := one;
                    end
                  end
                  else begin
                    if l^.EnlXY in [enlBoth, enlX] then begin
                      enlStartX := l^.StartEnl;
                      enlEndX := l^.EndEnl;
                    end
                    else begin
                      enlStartX := one;
                      enlEndX := one;
                    end;
                    if l^.EnlXY in [enlBoth, enlY] then begin
                      enlStartY := l^.StartEnl;
                      enlEndY := l^.EndEnl;
                    end
                    else begin
                      enlStartY := one;
                      enlEndY := one;
                    end
                  end;

                  ent_init (ent_csb, entply);
                  ent_init (ent_csh, entply);
                  ent_csb.plynpnt := length (PflPnts);
                  ent_csh.plynpnt := length (PflPnts);
                  for ProfNdx := 0 to length(PflPnts)-1 do begin
                    ent_csb.plypnt[ProfNdx+1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb),
                                                           NewPoint (PflPnts[ProfNdx].x * EnlStartX,
                                                                     PflPnts[ProfNdx].y * EnlStartY,
                                                                     PflPnts[ProfNdx].z * EnlStartY));
                    if pth.enttyp in PathEntsWithMatrix then
                      xformpt (ent_csb.plypnt[ProfNdx+1], pth.Matrix, ent_csb.plypnt[ProfNdx+1]);
                    if length (pth.Pnts2) > 0 then
                      ent_csh.plypnt[ProfNdx+1] := AddPoints(NewPoint (pth.Pnts2[PathNdx], pth.zh),
                                                             NewPoint (PflPnts[ProfNdx].x * EnlEndX,
                                                                       PflPnts[ProfNdx].y * EnlEndY,
                                                                       PflPnts[ProfNdx].z * EnlEndY))
                    else if pth.enttyp = entcon then
                      ent_csh.plypnt[ProfNdx+1] := AddPoints(NewPoint (pth.Cntr2, pth.zh),
                                                             NewPoint (PflPnts[ProfNdx].x * EnlEndX,
                                                                       PflPnts[ProfNdx].y * EnlEndy,
                                                                       PflPnts[ProfNdx].z * EnlEndy))
                    else
                      ent_csh.plypnt[ProfNdx+1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zh),
                                                             NewPoint (PflPnts[ProfNdx].x * EnlEndX,
                                                                       PflPnts[ProfNdx].y * EnlEndY,
                                                                       PflPnts[ProfNdx].z * EnlEndY));
                    if pth.enttyp in PathEntsWithMatrix then
                      xformpt (ent_csh.plypnt[ProfNdx+1], pth.Matrix, ent_csh.plypnt[ProfNdx+1]);
                  end;

                  ent_add (ent_csb);
                  ent_draw (ent_csb, drmode_white);
                  ent_add (ent_csh);
                  ent_draw (ent_csh, drmode_white);
                end;

              end;
              {$endregion 'verticals ONLY.  Just stamp the profile at each corner'}
              continue;
            end;
            if PathNdx = length (pth.Pnts) - 1 then
              break;    // no need to process last point of path as following logic processes side from current point to next point

            {$region 'Set Enlargement'}
            prevrunninglen := runninglen;
            runninglen := runninglen + distance (pth.Pnts[PathNdx].p, pth.pnts[PathNdx+1].p);
            if (verticals in zProcessing) and (not RealEqual(SlopeHite1, SlopeHite2)) or
               ((l^.StartEnl=1) and (l^.EndEnl=1)) then begin
              // do not process enlargement when both verticals and horizontal extrusions are involved
              enlstartX := one;
              enlstartY := one;
              enlendX := one;
              enlendY := one;
            end
            else begin
              enlstart := l^.StartEnl + (l^.EndEnl - l^.StartEnl)*prevrunninglen/pathlen;
              enlend := l^.StartEnl + (l^.EndEnl - l^.StartEnl)*runninglen/pathlen;
              if l^.enlXY in [EnlBoth, EnlX] then begin
                enlstartX := enlstart;
                enlendX := enlend;
              end
              else begin
                enlstartX := one;
                enlendX := one;
              end;
              if l^.enlXY in [EnlBoth, EnlY] then begin
                enlstartY := enlstart;
                enlendY := enlend;
              end
              else begin
                enlstartY := one;
                enlendY := one;
              end;
            end;
            {$endregion 'Set Enlargement'}

            DoStartCap := l^.Cap and VisibleSide and (not PrevVisibleSide) and (length (ProfileArray.points) <= 256);  // polygon entity used for caps has max 256 sides
            DoEndCap := l^.Cap and VisibleSide and (not NextVisibleSide) and (length (ProfileArray.points) <= 265);
            if DoStartCap then begin
              if ((base in zProcessing) and not (verticals in zProcessing)) or
                 ((verticals in zProcessing) and not (base in zProcessing)) or
                 (Slope in zProcessing) or (RevSlope in zProcessing) then begin
                ent_init (ent_csb, entply);
                ent_csb.plynpnt := length (ProfileArray.points);
              end;
              if ((hite in zProcessing) and not (verticals in zProcessing)) or
                 ((verticals in zProcessing) and not (hite in zProcessing)) or
                 (Slope in zProcessing) or (RevSlope in zProcessing) then begin
                ent_init (ent_csh, entply);
                ent_csh.plynpnt := length (ProfileArray.points);
              end;
            end;
            if DoEndCap then begin
              if ((base in zProcessing) and not (verticals in zProcessing)) or
                 ((verticals in zProcessing) and not (base in zProcessing)) or
                 (Slope in zProcessing) or (RevSlope in zProcessing) then begin
                ent_init (ent_ceb, entply);
                ent_ceb.plynpnt := length (ProfileArray.points);
              end;
              if ((hite in zProcessing) and not (verticals in zProcessing)) or
                 ((verticals in zProcessing) and not (hite in zProcessing)) or
                 (Slope in zProcessing) or (RevSlope in zProcessing)  then begin
                ent_init (ent_ceh, entply);
                ent_ceh.plynpnt := length (ProfileArray.points);
              end;
            end;

            if VisibleSide then for ProfNdx := 0 to length(ProfileArray.points)-1 do begin
              if ProfNdx = length(ProfileArray.points)-1 then begin
                if not ProfileArray.closed then begin
                  break;
                end
                else ProfNxt := 0
              end
              else ProfNxt := ProfNdx+1;

              ent_v1.plynpnt := 0;
              ent_v2.plynpnt := 0;
              if DoStartVertical then begin
                {$region 'verticals + horizontal - start of path'}
                xformpt (ProfileArray.points[ProfNdx].p, modmats[PathNdx][mat_v1], ModPflLine1[1]);
                xformpt (ProfileArray.points[ProfNxt].p, modmats[PathNdx][mat_v1], ModPflLine1[2]);
                ent_init (ent_v1, entply);
                ent_v1.plynpnt := 4;
                ent_v1.plypnt[1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb), ModPflLine1[1]);
                ent_v1.plypnt[2] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zh), ModPflLine1[1]);
                ent_v1.plypnt[3] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zh), ModPflLine1[2]);
                ent_v1.plypnt[4] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb), ModPflLine1[2]);
                if DoStartCap then begin
                  if base in zProcessing then
                    ent_csb.plynpnt := 0
                  else begin
                    ent_csb.plypnt[ProfNdx+1] := ent_v1.plypnt[1];
                    if (ProfNxt > 0) then
                      ent_csb.plypnt[ProfNxt+1] := ent_v1.plypnt[4];
                  end;
                  if hite in zProcessing then
                    ent_csh.plynpnt := 0
                  else begin
                    ent_csh.plypnt[ProfNdx+1] := ent_v1.plypnt[2];
                    if (ProfNxt > 0) then
                      ent_csh.plypnt[ProfNxt+1] := ent_v1.plypnt[3];
                  end;
                end;
                if (not ProfileArray.points[ProfNdx].visible) then
                  ent_v1.plynpnt := 0;

                {$endregion 'verticals + horizontal - start of path'}
              end;
              if DoEndVertical then begin
                {$region 'verticals + horizontal - end of path'}
                xformpt (ProfileArray.points[ProfNdx].p, modmats[PathNdx+1][mat_v2], ModPflLine1[1]);
                xformpt (ProfileArray.points[ProfNxt].p, modmats[PathNdx+1][mat_v2], ModPflLine1[2]);
                ent_init (ent_v2, entply);
                ent_v2.plynpnt := 4;
                ent_v2.plypnt[1] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zb), ModPflLine1[1]);
                ent_v2.plypnt[2] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zh), ModPflLine1[1]);
                ent_v2.plypnt[3] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zh), ModPflLine1[2]);
                ent_v2.plypnt[4] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zb), ModPflLine1[2]);
                if DoEndCap then begin
                  if base in zProcessing then
                    ent_ceb.plynpnt := 0
                  else begin
                    ent_ceb.plypnt[ProfNdx+1] := ent_v2.plypnt[1];
                    if (ProfNxt > 0) then
                      ent_ceb.plypnt[ProfNxt+1] := ent_v2.plypnt[4];
                  end;
                  if hite in zProcessing then
                    ent_ceh.plynpnt := 0
                  else begin
                    ent_ceh.plypnt[ProfNdx+1] := ent_v2.plypnt[2];
                    if (ProfNxt > 0) then
                      ent_ceh.plypnt[ProfNxt+1] := ent_v2.plypnt[3];
                  end;
                end;
                if (not ProfileArray.points[ProfNdx].visible) then
                  ent_v2.plynpnt := 0;

                {$endregion 'verticals + horizontal - end of path'}
              end;
              if (base in zProcessing) or (zMid in zProcessing) or (BasePlus in zProcessing) then begin
                {$region 'base'}
                xformpt (NewPoint (ProfileArray.points[ProfNdx].p.x,
                                   ProfileArray.points[ProfNdx].p.y * enlstartX,
                                   ProfileArray.points[ProfNdx].p.z * enlstartY),
                         modmats[PathNdx][mat_b], ModPflLine1[1]);
                xformpt (NewPoint (ProfileArray.points[ProfNxt].p.x,
                                   ProfileArray.points[ProfNxt].p.y * enlStartX,
                                   ProfileArray.points[ProfNxt].p.z * enlStartY),
                         modmats[PathNdx][mat_b], ModPflLine1[2]);
                xformpt (NewPoint (ProfileArray.points[ProfNdx].p.x,
                                   ProfileArray.points[ProfNdx].p.y * enlEndX,
                                   ProfileArray.points[ProfNdx].p.z * enlEndY),
                         modmats[PathNdx+1][mat_b], ModPflLine2[1]);
                xformpt (NewPoint (ProfileArray.points[ProfNxt].p.x,
                                   ProfileArray.points[ProfNxt].p.y * enlEndX,
                                   ProfileArray.points[ProfNxt].p.z * enlEndY),
                         modmats[PathNdx+1][mat_b], ModPflLine2[2]);
                ent_init (ent_b, entply);
                ent_b.plynpnt := 4;
                ent_b.plypnt[1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb), ModPflLine1[1]);
                ent_b.plypnt[2] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zb), ModPflLine2[1]);
                ent_b.plypnt[3] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zb), ModPflLine2[2]);
                ent_b.plypnt[4] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zb), ModPflLine1[2]);

                if DoStartVertical then begin
                  ent_b.plypnt[1].x := ent_v1.plypnt[1].x;
                  ent_b.plypnt[1].y := ent_v1.plypnt[1].y;
                  ent_b.plypnt[4].x := ent_v1.plypnt[4].x;
                  ent_b.plypnt[4].y := ent_v1.plypnt[4].y;
                  ent_v1.plypnt[1].z := ent_b.plypnt[1].z;
                  ent_v1.plypnt[4].z := ent_b.plypnt[4].z;
                end;

                if DoEndVertical then begin
                  ent_b.plypnt[2].x := ent_v2.plypnt[2].x;
                  ent_b.plypnt[2].y := ent_v2.plypnt[2].y;
                  ent_b.plypnt[3].x := ent_v2.plypnt[3].x;
                  ent_b.plypnt[3].y := ent_v2.plypnt[3].y;
                  ent_v2.plypnt[1].z := ent_b.plypnt[2].z;
                  ent_v2.plypnt[4].z := ent_b.plypnt[3].z;
                end;

                if pth.enttyp in PathEntsWithMatrix then for j := 1 to 4 do
                  xformpt (ent_b.plypnt[j], pth.Matrix, ent_b.plypnt[j]);

                if DoStartCap and not DoStartVertical then begin
                  ent_csb.plypnt[ProfNdx+1] := ent_b.plypnt[1];
                  ent_csb.plyisln[ProfNdx+1] := ProfileArray.points[ProfNdx].visible;
                  if (ProfNxt > 0) then begin
                    ent_csb.plypnt[ProfNxt+1] := ent_b.plypnt[4];
                    ent_csb.plyisln[ProfNxt+1] := ProfileArray.points[ProfNxt].visible;
                  end;
                end;
                if DoEndCap and not DoEndVertical then begin
                  ent_ceb.plypnt[ProfNdx+1] := ent_b.plypnt[2];
                  ent_csh.plyisln[ProfNdx+1] := ProfileArray.points[ProfNdx].visible;
                  if (ProfNxt > 0) then begin
                    ent_ceb.plypnt[ProfNxt+1] := ent_b.plypnt[3];
                    ent_ceb.plyisln[ProfNxt+1] := ProfileArray.points[ProfNxt].visible;
                  end;
                end;
                if ProfileArray.points[ProfNdx].visible then begin
                  ent_add (ent_b);
                  ent_draw (ent_b, drmode_white);
                end;
                {$endregion 'base'}
              end;
              if (hite in zProcessing) and not ((base in zProcessing) and RealEqual(SlopeHite1, SlopeHite2)) and (pth.enttyp <> entcon) then begin
                {$region 'Hite'}
                xformpt (NewPoint (ProfileArray.points[ProfNdx].p.x,
                                   ProfileArray.points[ProfNdx].p.y * enlStartX,
                                   ProfileArray.points[ProfNdx].p.z * enlStartY),
                         modmats[PathNdx][mat_h], ModPflLine1[1]);
                xformpt (NewPoint (ProfileArray.points[ProfNxt].p.x,
                                   ProfileArray.points[ProfNxt].p.y * enlStartX,
                                   ProfileArray.points[ProfNxt].p.z * enlStartY),
                         modmats[PathNdx][mat_h], ModPflLine1[2]);
                xformpt (newPoint (ProfileArray.points[ProfNdx].p.x,
                                   ProfileArray.points[ProfNdx].p.y * enlEndX,
                                   ProfileArray.points[ProfNdx].p.z * enlEndY),
                         modmats[PathNdx+1][mat_h], ModPflLine2[1]);
                xformpt (NewPoint (ProfileArray.points[ProfNxt].p.x,
                                   ProfileArray.points[ProfNxt].p.y * enlEndX,
                                   ProfileArray.points[ProfNxt].p.z * enlEndY),
                         modmats[PathNdx+1][mat_h], ModPflLine2[2]);
                ent_init (ent_h, entply);
                ent_h.plynpnt := 4;
                if length (pth.Pnts2) = length (pth.Pnts) then begin
                  ent_h.plypnt[1] := AddPoints(pth.Pnts2[PathNdx], ModPflLine1[1]);
                  ent_h.plypnt[2] := AddPoints(pth.Pnts2[PathNdx+1], ModPflLine2[1]);
                  ent_h.plypnt[3] := AddPoints(pth.Pnts2[PathNdx+1], ModPflLine2[2]);
                  ent_h.plypnt[4] := AddPoints(pth.Pnts2[PathNdx], ModPflLine1[2]);
                end
                else begin
                  ent_h.plypnt[1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zh), ModPflLine1[1]);
                  ent_h.plypnt[2] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zh), ModPflLine2[1]);
                  ent_h.plypnt[3] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, pth.zh), ModPflLine2[2]);
                  ent_h.plypnt[4] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, pth.zh), ModPflLine1[2]);
                end;

                if DoStartVertical then begin
                  ent_h.plypnt[1].x := ent_v1.plypnt[2].x;
                  ent_h.plypnt[1].y := ent_v1.plypnt[2].y;
                  ent_h.plypnt[4].x := ent_v1.plypnt[3].x;
                  ent_h.plypnt[4].y := ent_v1.plypnt[3].y;
                  ent_v1.plypnt[2].z := ent_h.plypnt[2].z;
                  ent_v1.plypnt[3].z := ent_h.plypnt[3].z;
                end;

                if DoEndVertical then begin
                  ent_h.plypnt[2].x := ent_v2.plypnt[2].x;
                  ent_h.plypnt[2].y := ent_v2.plypnt[2].y;
                  ent_h.plypnt[3].x := ent_v2.plypnt[3].x;
                  ent_h.plypnt[3].y := ent_v2.plypnt[3].y;
                  ent_v2.plypnt[2].z := ent_h.plypnt[2].z;
                  ent_v2.plypnt[3].z := ent_h.plypnt[3].z;
                end;

                if pth.enttyp in PathEntsWithMatrix then for j := 1 to 4 do
                  xformpt (ent_h.plypnt[j], pth.Matrix, ent_h.plypnt[j]);

                if DoStartCap and not DoStartVertical then begin
                  ent_csh.plypnt[ProfNdx+1] := ent_h.plypnt[1];
                  ent_csh.plyisln[ProfNdx+1] := ProfileArray.points[ProfNdx].visible;
                  if (ProfNxt > 0) then begin
                    ent_csh.plypnt[ProfNxt+1] := ent_h.plypnt[4];
                    ent_csh.plyisln[ProfNxt+1] := ProfileArray.points[ProfNxt].visible;
                  end
                end;
                if DoEndCap and not DoEndVertical then begin
                  ent_ceh.plypnt[ProfNdx+1] := ent_h.plypnt[2];
                  ent_ceh.plyisln[ProfNdx+1] := ProfileArray.points[ProfNdx].visible;
                  if (ProfNxt > 0) then begin
                    ent_ceh.plypnt[ProfNxt+1] := ent_h.plypnt[3];
                    ent_ceh.plyisln[ProfNxt+1] := ProfileArray.points[ProfNxt].visible;
                  end;
                end;

                if ProfileArray.points[ProfNdx].visible then begin
                  ent_add (ent_h);
                  ent_draw (ent_h, drmode_white);
                end;
                {$endregion 'Hite'}
              end;
              if (Slope in zProcessing) or (RevSlope in zProcessing) then begin
                {$region 'Slope'}
                z1 := SlopeHite1 + (SlopeHite2-SlopeHite1) * prevrunninglen/pathlen;
                z2 := SlopeHite1 + (SlopeHite2-SlopeHite1) * runninglen/pathlen;

                enlstart := l^.StartEnl + (l^.EndEnl - l^.StartEnl)*prevrunninglen/pathlen;
                enlend := l^.StartEnl + (l^.EndEnl - l^.StartEnl)*runninglen/pathlen;
                if l^.EnlXY in [enlboth, enlX] then begin
                  enlstartX := enlstart;
                  enlendX := enlend;
                end
                else begin
                  enlstartX := one;
                  enlendX := one;
                end;
                if l^.EnlXY in [enlboth, enlY] then begin
                  enlstartY := enlstart;
                  enlendY := enlend;
                end
                else begin
                  enlstartY := one;
                  enlendY := one;
                end;

                xformpt (NewPoint (ProfileArray.points[ProfNdx].p.x,
                                   ProfileArray.points[ProfNdx].p.y * enlStartX,
                                   ProfileArray.points[ProfNdx].p.z * enlStartY),
                         modmats[PathNdx][mat_s], ModPflLine1[1]);
                xformpt (NewPoint (ProfileArray.points[ProfNxt].p.x,
                                   ProfileArray.points[ProfNxt].p.y * enlStartX,
                                   ProfileArray.points[ProfNxt].p.z * enlStartY),
                         modmats[PathNdx][mat_s], ModPflLine1[2]);
                xformpt (NewPoint (ProfileArray.points[ProfNdx].p.x,
                                   ProfileArray.points[ProfNdx].p.y * enlEndX,
                                   ProfileArray.points[ProfNdx].p.z * enlEndY),
                         modmats[PathNdx+1][mat_s], ModPflLine2[1]);
                xformpt (NewPoint (ProfileArray.points[ProfNxt].p.x,
                                   ProfileArray.points[ProfNxt].p.y * enlEndX,
                                   ProfileArray.points[ProfNxt].p.z * enlEndY),
                         modmats[PathNdx+1][mat_s], ModPflLine2[2]);
                ent_init (ent_s, entply);
                ent_s.plynpnt := 4;
                ent_s.plypnt[1] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, z1), ModPflLine1[1]);
                ent_s.plypnt[2] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, z2), ModPflLine2[1]);
                ent_s.plypnt[3] := AddPoints(NewPoint (pth.Pnts[PathNdx+1].p, z2), ModPflLine2[2]);
                ent_s.plypnt[4] := AddPoints(NewPoint (pth.Pnts[PathNdx].p, z1), ModPflLine1[2]);

                if pth.enttyp in PathEntsWithMatrix then for j := 1 to 4 do
                  xformpt (ent_s.plypnt[j], pth.Matrix, ent_s.plypnt[j]);

                if DoStartCap then begin
                  ent_csb.plypnt[ProfNdx+1] := ent_s.plypnt[1];
                  if (ProfNxt > 0) then
                    ent_csb.plypnt[ProfNxt+1] := ent_s.plypnt[4];
                end;
                if DoEndCap then begin
                  ent_ceb.plypnt[ProfNdx+1] := ent_s.plypnt[2];
                  if (ProfNxt > 0) then
                    ent_ceb.plypnt[ProfNxt+1] := ent_s.plypnt[3];
                end;

                if ProfileArray.points[ProfNdx].visible then begin
                  ent_add (ent_s);
                  ent_draw (ent_s, drmode_white);
                end;
                {$endregion 'Slope'}
              end;

              {$region 'Update and draw verticals'}
              if ent_v1.plynpnt > 0 then begin
                if pth.enttyp in PathEntsWithMatrix then for j := 1 to 4 do
                  xformpt (ent_v1.plypnt[j], pth.Matrix, ent_v1.plypnt[j]);
                ent_add (ent_v1);
                ent_draw (ent_v1, drmode_white);
              end;
              if ent_v2.plynpnt > 0 then begin
                if pth.enttyp in PathEntsWithMatrix then for j := 1 to 4 do
                  xformpt (ent_v2.plypnt[j], pth.Matrix, ent_v2.plypnt[j]);
                ent_add (ent_v2);
                ent_draw (ent_v2, drmode_white);
              end;
              {$endregion 'Update and draw verticals'}

            end; // for ProfNdx
            {$region 'Add previously constructed caps to drawing'}
            if ent_csb.plynpnt > 0 then begin
              if (Verticals in zProcessing) and (pth.enttyp in PathEntsWithMatrix) then
                for j := 1 to ent_csb.plynpnt do
                  xformpt (ent_csb.plypnt[j], pth.Matrix, ent_csb.plypnt[j]);
              ent_add (ent_csb);
              ent_draw (ent_csb, drmode_white);
              ent_csb.plynpnt := 0;
            end;
            if ent_csh.plynpnt > 0 then begin
              if (Verticals in zProcessing) and (pth.enttyp in PathEntsWithMatrix) then
                for j := 1 to ent_csh.plynpnt do
                  xformpt (ent_csh.plypnt[j], pth.Matrix, ent_csh.plypnt[j]);
              ent_add (ent_csh);
              ent_draw (ent_csh, drmode_white);
              ent_csh.plynpnt := 0;
            end;
            if ent_ceb.plynpnt > 0 then begin
              if (Verticals in zProcessing) and (pth.enttyp in PathEntsWithMatrix) then
                for j := 1 to ent_ceb.plynpnt do
                  xformpt (ent_ceb.plypnt[j], pth.Matrix, ent_ceb.plypnt[j]);
              ReversePly (ent_ceb);
              ent_add (ent_ceb);
              ent_draw (ent_ceb, drmode_white);
              ent_ceb.plynpnt := 0;
            end;
            if ent_ceh.plynpnt > 0 then begin
              if (Verticals in zProcessing) and (pth.enttyp in PathEntsWithMatrix) then
                for j := 1 to ent_ceh.plynpnt do
                  xformpt (ent_ceh.plypnt[j], pth.Matrix, ent_ceh.plypnt[j]);
              ReversePly (ent_ceh);
              ent_add (ent_ceh);
              ent_draw (ent_ceh, drmode_white);
              ent_ceh.plynpnt := 0;
            end;
            {$endregion 'Add previously constructed caps to drawing'}
          end;  // for PathNdx
        end;
     end;
      {$endregion 'entlin, entply, entslb'}

  end;
end;


end.
