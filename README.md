# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Extrude macro for DataCAD

### How do I get set up? ###

This is a Delphi project.  

It requires Header Files that are supplied with DataCAD.  It should compile with most versions of these files, but 
I am compiling it with files that were provided with DataCAD version 14 which I have modified slightly from the released
versions to fix a bug or 2.
These header files are included in my d4d_headerfiles repository.

You will also need to have installed delphi components from my DataCAD Components repository

It also requires files from the Common repository to be compiled (should be placed in a 'Common'
folder at the same level as the folder that contains this project

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact