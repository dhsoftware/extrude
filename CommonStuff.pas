unit CommonStuff;

interface

  uses URecords, UInterfacesRecords, UConstants, UInterfaces, UDCLibrary,
       Winapi.Windows, System.Generics.Collections, Math, SysUtils, Dialogs,
       System.UITypes, util, Profile, CommonTypes, Z_Profile, Enlargement,
       Constants;

const
  ValidPathEnts: set of byte = [entlin, entcrc, entbez, entarc, entell, entln3, entbsp,
                                entar3, entply, entcyl, entcon, entblk, entslb,
                                enttrn, entpln, enttxt];
  PathEntsWithMatrix: set of byte = [entar3, entply, entcyl, entcon, entblk, entslb, enttrn, entpln, entln3];


type
  TracePntsTyp = record
    pnt : point2D;
    ofs : double;
  end;

  TraceClosedTyp = (NotClosed, ClosedByPoints, ClosedByOption, ForceNotClosed);

  ExtrudeL = packed record
    state:    integer;
    prevstate : integer;
    initialised : boolean;
    ShowOptions : boolean;
    dcHandle : HWND;
    FmProfile: TFmProfile;
    ProfileFormCreated : boolean;
    EnlProfFormCreated : boolean;
    ZProfFormCreated : boolean;
    OffsetByPoint : boolean;
    profile, lastprofile, tempprofiles  : tProfiles;
    ProfileArraysCreated, tempprofilesCreated : boolean;
    ProfileArrays : TList<tProfileArray>;
    pathEnts : tEntities;
    tracepnts : TArray<TracePntsTyp>;
    OffsTracePnts : TArray<Point2D>;
    OffsTraceAll : boolean;
    fillet : boolean;
    prevpnt : point;
    traceZtype : TTraceZtype;
    traceZ : double;  // only used for traceZtype or trSpecified
    traceClosed : TraceClosedTyp;
    offset, FilletRad : double;
    pnt : point;
    zPflArr : TArray<point>;
    Divs : tDivs;
    pflsel, pathsel : tSelectnType;
    sympath : symstr;
    Cap, VerticalEnds : boolean;
    zTreatment : tZTreatmentSet;
    zProfile : entaddr;
    ZBasePlus : double;
    fmZProfile: TfmZProfile;
    StartEnl, EndEnl : double;
    EnlProfile, PrevEnlProfile : entaddr;
    PrevEnlProfileRef : double;
    EnlRepeat : boolean;
    EnlRevers : boolean;
    EnlRptDis : double;
    EnlXY : tEnlXY;
    fmEnlargeProfile: TfmEnlargeProfile;
    TwistType : tTwistType;
    TwistNumTurns, TwistDistance : double;
    StartRotation : double;
    ClkwiseRot : boolean;
    PrevEnlRpt : boolean;
    ExtrThick : double;
    TempRl : double;
    WindowFlags : packed array [_WindowProfile.._WindowZProfile] of byte;
    case byte of
       0: (getp: getpointarg);
       1: (gete: getEscArg);
       2: (geti: getIntArg);
       3: (getr: DgetrealArg);
       4: (getd: getdisArg);
       5: (dgetd: dgetdisArg);
       6: (GlobEsc : globescArg);
       7: (geta: getangArg)
  end;
  pExtrudeL = ^ExtrudeL;


var
  l : pExtrudeL;
  logfile : TextFile;
  Paths : tPaths;
  ErrorShown : boolean;


function entPnt1 (ent : entity) : point;

function entPnt2 (ent : entity) : point;



procedure ArcToPnts (cntr : point;
                     rad, bang, eang : double;
                     divs : integer;
                     var Pnts : TArray<point>);
procedure ArcToPntList (cntr : point;
                        rad, bang, eang : double;
                        divs : integer; equaldivs : boolean;
                        var Pnts : TList<point>);

(* procedure ReversePnts (var Pnts : TArray<point>);  *)

function NewProfileArrayItem (const pnt : point; visible : boolean = true; thicknessline : boolean = false) : tProfileArrayItem;

function Cylind_Cart_Rel (cntr: point; radius, ang : double; refpnt : point) : point;

function PflMiddle (profile : tProfiles) : point;

function PathLength (Pnts : TArray<point>) : double;    overload;
function PathLength (PntsRec : TArray<PointRec>) : double;    overload;

///  <summary>Adds the first numrecords of Array2 to the end of Array1.
///  </summary>
///  <param name="Array1">The array of points to be appended to</param>
///  <param name="Array2">The array of points to add to the end of Array1</param>
///  <param name="numrecords">The number of records from Array2 to add to the end of Array1. If zero then all points
///       will be appended</param>
procedure ConcatPntArray (var Array1 : TArray<point>;
                          const Array2 : TArray<point>;
                          numrecords : integer = 0);


///  <summary>Checks that line is not closed and always goes from left to right without doubling back on itself.
///  For "Enlargement" profile it also checks that the Y value of the line is never below zero.
///  </summary>
///  <param name="ent">The polyline entity to check</param>
///  <param name="checkType">Z=Z-profile check, E=Enlargement profile check</param>
///  <param name="showerror">Indicates whether to display an error if the polyline is not suitable</param>
///  <returns>Returns true is ent is a suitable polyline, else false.</returns>
function CheckPolyline (ent : entity; checkType : char; showerror : boolean = true) : boolean;


procedure Log (const LogPos : string);

procedure ReversePly (var ent : entity);


implementation



function entPnt1 (ent : entity) : point;
begin
  setpoint (result, 0.0);
  case ent.enttype of
    entlin : result := ent.linpt1;
    entln3 : result := ent.ln3pt1;
    entarc : begin
              cylind_cart (ent.arcrad, ent.arcbang, ent.arcbase, result.x, result.y, result.z);
              result.x := result.x + ent.arccent.x;
              result.y := result.y + ent.arccent.y;
             end;
    entar3 : if ent.ar3close then
                raise EInvalidEntType.Create('Closed Arc')
             else begin
              cylind_cart (ent.ar3rad, ent.ar3bang, 0, result.x, result.y, result.z);
              xformpt (result, ent.Matrix, result);
             end;
    else raise EInvalidEntType.Create('Invalid Ent Type');
  end;
end;

function entPnt2 (ent : entity) : point;
begin
  setpoint (result, 0.0);
  case ent.enttype of
    entlin : result := ent.linpt2;
    entln3 : result := ent.ln3pt2;
    entarc : begin
              cylind_cart (ent.arcrad, ent.arceang, ent.archite, result.x, result.y, result.z);
              result.x := result.x + ent.arccent.x;
              result.y := result.y + ent.arccent.y;
             end;
    entar3 : if ent.ar3close then
                raise EInvalidEntType.Create('Closed Arc')
             else begin
              cylind_cart (ent.ar3rad, ent.ar3eang, 0, result.x, result.y, result.z);
              xformpt (result, ent.Matrix, result);
             end;
    else raise EInvalidEntType.Create('Invalid Ent Type');
  end;
end;

procedure ArcToPnts (cntr : point;
                     rad, bang, eang : double;
                     divs : integer;
                     var Pnts : TArray<point>);
var
  numpnts, i : integer;
  angincrement : double;
begin
  if eang < bang then
    eang := eang + twopi;
  numpnts := Round(abs(eang - bang) / (Pi2 / divs) + 0.4999) + 1;
  if (numpnts = 1) and not RealEqual(bang, eang) then
    numpnts := 2;
  if numpnts = 1 then
     angincrement := 0
  else
    angincrement := abs(eang - bang)/(numpnts-1);
  SetLength(Pnts, numpnts);
  for i := 0 to numpnts-1 do begin
    cylind_cart (rad, min(bang, eang)+(angincrement*i), cntr.z, Pnts[i].x, Pnts[i].y, Pnts[i].z);
    Pnts[i].x := Pnts[i].x + cntr.x;
    Pnts[i].y := Pnts[i].y + cntr.y;
  end;
end;

procedure ArcToPntList (cntr : point;
                        rad, bang, eang : double;
                        divs : integer;  equaldivs : boolean;
                        var Pnts : TList<point>);
var
  numpnts, i : integer;
  angincrement : double;
  pnt : point;
begin
  if eang < bang then
    eang := eang + twopi;
  numpnts := ceil(abs(eang - bang) / (Pi2 / divs)-abszero) + 1;
  if (numpnts = 1) and not RealEqual(bang, eang) then
    numpnts := 2;
  if numpnts = 1 then
     angincrement := 0
  else if EqualDivs then
    angincrement := abs(eang - bang)/(numpnts-1)
  else
    angincrement := TwoPi / divs;
  Pnts.Clear;
  for i := 1 to numpnts do begin
    if (i = numpnts) and not equaldivs  then
      cylind_cart (rad, max(bang, eang), cntr.z, pnt.x, pnt.y, pnt.z)
    else
      cylind_cart (rad, min(bang, eang)+(angincrement*(i-1)), cntr.z, pnt.x, pnt.y, pnt.z);
    pnt.x := pnt.x + cntr.x;
    pnt.y := pnt.y + cntr.y;
    Pnts.Add(pnt);
  end;
end;


function NewProfileArrayItem (const pnt : point; visible : boolean = true; thicknessline : boolean = false) : tProfileArrayItem;
begin
  Result.p.x := pnt.x;
  Result.p.y := pnt.y;
  Result.p.z := zero;
  Result.visible := visible;
  Result.thicknessLine := thicknessline;
end;



function Cylind_Cart_Rel (cntr: point; radius, ang : double; refpnt : point) : point;
begin
  cylind_cart (radius, ang, 0, result.x, result.y, result.z);
  result.x := result.x + cntr.x - refpnt.x;
  result.x := result.y + cntr.y - refpnt.y;
end;

function PathLength (Pnts : TArray<point>) : double;
var
  i : integer;
begin
  result := 0;
  for i := 0 to length (Pnts)-2 do
    result := result + distance (Pnts[i], Pnts[i+1]);
end;

function PathLength (PntsRec : TArray<PointRec>) : double;
var
  i : integer;
begin
  result := 0;
  for i := 0 to length (PntsRec)-2 do
    result := result + distance (PntsRec[i].p, PntsRec[i+1].p);
end;

function PflMiddle (profile : tProfiles) : point;
/// returns a point in the middle of the bounding rectangle
var
  minpt, maxpt, minpt1, maxpt1 : point;
  ent : entity;
  i : integer;
begin
  if (profile.Items.Count < 1) or (profile.Items[0].ProfileType <> p_entity) or not ent_get (ent, profile.Items[0].adr) then begin
    setpoint (result, 0);   // should never happen
    exit;
  end;
  ent_extent (ent, minpt, maxpt);
  if profile.Items.Count > 1 then for i := 1 to profile.Items.Count-1 do begin
    if ent_get (ent, profile.Items[i].adr) then begin
      ent_extent (ent, minpt1, maxpt1);
      minpt.x := min (minpt.x, minpt1.x);
      minpt.y := min (minpt.y, minpt1.y);
      maxpt.x := max (maxpt.x, maxpt1.x);
      maxpt.y := max (maxpt.y, maxpt1.y);
    end;
  end;
  meanpnt (minpt, maxpt, result);
end;

function CheckPolyline (ent : entity; {polyline entity to check}
                        checkType : char; {Z=Z-profile check, E=Enlargement profile check}
                        showerror : boolean = true) : boolean;
{ Checks that line is not closed and always goes from left to right without doubling back on itself.
  For "Enlargement" profile it also checks that the Y value of the line is never below zero.  }
var
  errormsg : string;
  adr : lgl_addr;
  pv : polyvert;
  minpnt, maxpnt, cent : point;
  rad, bang, eang : double;
  ccw : boolean;
  left2right : integer;  {set to 1 for left to right, -1 for right to left}
  ArcPnts : TArray<point>;
  i : integer;
begin
  result := true;
  errormsg := '';
  {$region 'Basic checking of ent type, extents etc.'}
  if ent.enttype <> entpln then
    errormsg := 'Selected entity is not a polyline'
  else if ent.plnclose then
    errormsg := 'Closed polylines are not suitable'
  else begin
    ent_extent (ent, minpnt, maxpnt);
    if (checkType = 'E') and (minpnt.y < 0) then
      errormsg := 'Lines that go below Y-zero are not suitable'
  end;
  {$endregion 'Basic checking of ent type, extents etc.'}
  if length (errormsg) = 0 then begin
    adr := ent.plnfrst;
    if not polyvert_get (pv, adr, ent.plnfrst) then begin
      result := false;
      errormsg := 'Selected Line is not suitable';
    end
    else begin
      if pv.pnt.x < pv.nextpnt.x then
        left2right := 1
      else
        left2right := -1;
      while polyvert_get (pv, adr, ent.plnfrst) and result do begin
        adr := pv.Next;
        if not pv.last then begin
          if (pv.nextpnt.x - pv.pnt.x) * left2right < 0  then
            result := false
          else if pv.shape = pv_bulge then begin
            bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, cent, rad, bang, eang, ccw);
            if not ccw then
               swapaFloat (bang, eang);
            ArcToPnts(cent, rad, bang, eang, 128, ArcPnts);
            if not ccw then
              ReversePnts(ArcPnts);
            for i := 0 to length(ArcPnts)-2 do begin
            if (ArcPnts[i+1].x - arcPnts[i].x) * left2right < 0  then
              result := false;
            end;
          end;
        end;
      end;
    end;

    if not result then
      errormsg := 'Line NOT suitable - It must always travel in one x-direction (either left to right or right to left)';
  end;
  if errormsg > '' then begin
    result := false;
    if showerror then begin
      wrterr (str255(errormsg));
      sysUtils.Beep;
      if length (errormsg) > 40 then begin
        errormsg := StringReplace (errormsg, ' - ', ':' + sLineBreak + sLineBreak, []);
        MessageDlg (errormsg, mtError, [mbOK], 0);
      end;

    end;
  end;
end;

procedure ConcatPntArray (var Array1 : TArray<point>;
                          const Array2 : TArray<point>;
                          numrecords : integer = 0);
var
  i, pos : integer;
begin
  pos := length(Array1);
  if (numrecords = 0) or (numrecords > length(Array2))  then
    numrecords := length(Array2);
  setlength (Array1, length(Array1) + numrecords);
  for i := 0 to numrecords-1 do begin
    Array1[pos] := Array2[i];
    pos := pos+1;
  end;
end;


CONST
  logfilename = 'Extrude.log';

procedure Log (const LogPos : string);
begin
  exit;
  AssignFile(logfile, logfilename);
  if FileExists(logfilename) then
    Append (logfile)
  else
    Rewrite(logfile);
  Writeln(logfile, LogPos);
  CloseFile(logfile);
end;

procedure ReversePly (var ent : entity);
var
  i : integer;
begin
  if (not ent.enttype in [entply, entslb]) or (ent.plynpnt < 1) then
    exit;
  for i := 1 to ent.plynpnt div 2 do
    SwapPnt (ent.plypnt[i], ent.plypnt[ent.plynpnt-i+1]);
end;


end.
