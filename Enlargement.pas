unit Enlargement;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, DcadEdit, Vcl.ExtCtrls,
  UInterfaces, URecords, UInterfacesRecords, UConstants, Extents, Settings, Constants,
  CommonTypes, Util, System.UITypes, PlnUtil, System.Math, EnlRptDis;

type
  TfmEnlargeProfile = class(TForm)
    imgEnlProf: TImage;
    btnCancel: TButton;
    btnReverse: TButton;
    lblXYEnlargeMsg: TLabel;
    gbRepeat: TGroupBox;
    rbStretcb: TRadioButton;
    rbRepeat: TRadioButton;
    btnRptDisReset: TButton;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnReverseClick(Sender: TObject);
    procedure rbStretchRepeatClick(Sender: TObject);
    procedure btnRptDisResetClick(Sender: TObject);
  private
    { Private declarations }
    pEnlPfl : ^entaddr;
    pRepeat : ^boolean;
    pRptDis : ^double;
    pDivs   : ^integer;
    pEqDivs : ^boolean;
    pRevers : ^boolean;
    pXY     : ^tEnlXY;
    pState : ^asint;
    initialised : boolean;
    firstpnt, lastpnt : point;
  public
    { Public declarations }
    procedure SetEnlargePfl (EnlPflAddr : pointer; EnlRptAddr : pointer; EnlRptDisAddr : pointer;
                             StateAddr : pointer; DivsAddr : pointer; EqDivsAddr : pointer;
                             RevAddr : pointer; XYAddr : pointer);
    procedure DrawProfile;
    procedure SetRepeat;
  end;

implementation

{$R *.dfm}

procedure TfmEnlargeProfile.SetEnlargePfl (EnlPflAddr : pointer; EnlRptAddr : pointer; EnlRptDisAddr : pointer;
                                           StateAddr : pointer; DivsAddr : pointer; EqDivsAddr : pointer;
                                           RevAddr : pointer; XYAddr : pointer);
begin
  pEnlPfl := EnlPflAddr;
  pRepeat := EnlRptAddr;
  pRptDis := EnlRptDisAddr;
  pstate := StateAddr;
  initialised := true;
  pDivs := DivsAddr;
  pEqDivs := EqDivsAddr;
  pRevers := RevAddr;
  pXY := XYAddr;
  btnRptDisReset.Enabled := pRepeat^;
end;


procedure TfmEnlargeProfile.btnCancelClick(Sender: TObject);
var
  ent : entity;
  atr : attrib;
begin
  if ent_get (ent, pEnlPfl^) and atr_entfind (ent, dhExtEnlProf, atr) then begin
    atr_delent (ent, atr);
  end;
  setnil (pEnlPfl^);
  pXY^ := enlBoth;
  hide;
  if pState^ = _Path then begin
    lblsett (13, 'Enlarge', false, 'Specify enlargement to be applied to the section profile along the sweep path');
    lblson;
  end
  else if pState^ = _Enl then begin
    lblsett (5, EnlPflLbl, false, EnlPflHint);
    lblsett (7, EnlXLbl, true, EnlXHint);
    lblsett (8, EnlYLbl, true, EnlYHint);
    lblson;
  end;
  hide;
end;

procedure TfmEnlargeProfile.btnReverseClick(Sender: TObject);
begin
  pRevers^ := not pRevers^;
  DrawProfile;
end;

procedure TfmEnlargeProfile.btnRptDisResetClick(Sender: TObject);
begin
  fmEnlRptDis := TfmEnlRptDis.Create(nil);
  fmEnlRptDis.Top := Top+gbRepeat.Top;
  fmEnlRptDis.Left := Left;
  fmEnlRptDis.Width := Width;
  fmEnlRptDis.SetWidth (pRptDis^);
  fmEnlRptDis.PflWidth := abs(lastpnt.x - firstpnt.x);
  fmEnlRptDis.ShowModal;
  if fmEnlRptDis.ModalResult = mrOK then begin
    pRptDis^ := fmEnlRptDis.GetWidth;
    SetRepeat;
    SaveRl(dhEnlPRptDis, pRptDis^, false);
  end;
  fmEnlRptDis.Free;
end;

procedure TfmEnlargeProfile.SetRepeat;
var
  s : shortstring;
begin
  if not realequal (firstpnt.y, lastpnt.y) then begin
    pRepeat^ := false;
    rbRepeat.Caption := 'Repeat (N/A)'
  end
  else begin
    tostr (pRptDis^, s, false);
    rbRepeat.Caption := 'Repeat ' + string(s);
  end;

  btnRptDisReset.enabled := pRepeat^;
  if pRepeat^ then
    rbRepeat.checked := true
  else
    rbStretcb.Checked := true;
end;

procedure TfmEnlargeProfile.DrawProfile;
var
  ent : entity;
  atr : attrib;
  MinPnt, MaxPnt : point;
  MaxEnl : double;
  XMultiplier, YMultiplier : double;
  Margin, TxtCntr, inc, i : integer;
  s : string;
  pnts : TArray<point>;


begin
  if not initialised then
    exit;
  if not ent_get (ent, pEnlPfl^) then
    exit;
  if not atr_entfind (ent, dhExtEnlProf, atr) then
    exit;


  pnts := PlnToPntArray (ent, pDivs^, pEqDivs^);
  if length(pnts) < 2 then
    exit;

  if pRevers^ then for i := 0 to length(pnts)-1 do
    pnts[i].x := -pnts[i].x;

  if pXY^ = enlBoth then begin
    lblXYEnlargeMsg.Caption := '';
    imgEnlProf.Height := lblXYEnlargeMsg.Top + lblXYEnlargeMsg.Height;
  end
  else begin
    case pXY^ of
      enlNone : lblXYEnlargeMsg.Caption := 'NOT APPLIED to either axis';
      enlX    : lblXYEnlargeMsg.Caption := 'X-axis ONLY';
      enlY    : lblXYEnlargeMsg.Caption := 'Y-axis ONLY';
    end;
    imgEnlProf.Height := lblXYEnlargeMsg.Top - 1;
  end;


  MinPnt.x := pnts[0].x;
  MaxPnt.x := pnts[0].x;
  MinPnt.y := pnts[0].y;
  MaxPnt.y := pnts[0].y;
  for i := 1 to length(pnts)-1 do begin
    MinPnt.x := min (MinPnt.x, pnts[i].x);
    MinPnt.y := min (MinPnt.y, pnts[i].y);
    MaxPnt.x := max (MaxPnt.x, pnts[i].x);
    MaxPnt.y := max (MaxPnt.y, pnts[i].y);
  end;

  MaxEnl := MaxPnt.y / atr.rl;

  imgEnlProf.Canvas.Font := rbRepeat.Font;
  margin := imgEnlProf.Canvas.TextWidth('8.8')+2;
  TxtCntr := imgEnlProf.Canvas.TextHeight('8.8') div 2;
  YMultiplier := (imgEnlProf.Height-TxtCntr)/round(MaxPnt.y + 0.5);
  XMultiplier := (imgEnlProf.Width-Margin)/round(MaxPnt.x - MinPnt.x + 0.5);

  imgEnlProf.Picture.Bitmap.Width := imgEnlProf.Width;
  imgEnlProf.Picture.Bitmap.Height := imgEnlProf.Height;
  imgEnlProf.Canvas.Pen.Color := clWhite;
  imgEnlProf.Canvas.Brush.Color := clWhite;
  imgEnlProf.Canvas.Rectangle(0,0, imgEnlProf.Width, imgEnlProf.height);

  imgEnlProf.Canvas.Pen.Color := clSilver;
  imgEnlProf.Canvas.Font.Color := clSilver;

  if MaxEnl < 0.1 then begin

  end
  else if MaxEnl < 0.2 then begin
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(0.1*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(0.1*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(0.1*atr.rl*YMultiplier)-TxtCntr, '0.1');
  end
  else if MaxEnl < 0.6 then begin
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(0.2*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(0.2*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(0.2*atr.rl*YMultiplier)-TxtCntr, '0.2');
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(0.4*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(0.4*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(0.4*atr.rl*YMultiplier)-TxtCntr, '0.4');
  end
  else if MaxEnl < 1.25 then begin
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(0.25*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(0.25*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(0.25*atr.rl*YMultiplier)-TxtCntr, '0.25');
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(0.5*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(0.5*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(0.5*atr.rl*YMultiplier)-TxtCntr, '0.5');
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(0.75*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(0.75*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(0.75*atr.rl*YMultiplier)-TxtCntr, '0.75');
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(atr.rl*YMultiplier)-TxtCntr, '1.0');
  end
  else if MaxEnl < 2.5 then begin
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(0.5*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(0.5*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(0.5*atr.rl*YMultiplier)-TxtCntr, '0.5');
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(atr.rl*YMultiplier)-TxtCntr, '1.0');
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(1.5*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(1.5*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(1.5*atr.rl*YMultiplier)-TxtCntr, '1.5');
    imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(2*atr.rl*YMultiplier));
    imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(2*atr.rl*YMultiplier));
    imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(2*atr.rl*YMultiplier)-TxtCntr, '2.0');
  end
  else begin
    inc := round(MaxEnl/4 + 0.5);
    for i := 1 to 4 do begin
      imgEnlProf.Canvas.MoveTo (Margin, imgEnlProf.Height-round(i*inc*atr.rl*YMultiplier));
      imgEnlProf.Canvas.LineTo (imgEnlProf.Width, imgEnlProf.Height-round(i*inc*atr.rl*YMultiplier));
      s := IntToStr(i*inc);
      if i < 10 then
        s := ' ' + s;
      imgEnlProf.Canvas.TextOut(1, imgEnlProf.Height-round(i*inc*atr.rl*YMultiplier)-TxtCntr, s);
    end;
  end;

  imgEnlProf.Canvas.Pen.Color := clBlack;
  imgEnlProf.Canvas.Brush.Style := bsClear;

  for i := 0 to length(pnts)-2 do begin
    imgEnlProf.Canvas.MoveTo(Margin + round((pnts[i].x-MinPnt.x)*XMultiplier), imgEnlProf.Height-round(pnts[i].y*YMultiplier));
    imgEnlProf.Canvas.LineTo(Margin + round((pnts[i+1].x-MinPnt.x)*XMultiplier), imgEnlProf.Height-round(pnts[i+1].y*YMultiplier));
  end;

  firstpnt := pnts[0];
  lastpnt := pnts[length(pnts)-1];


  if not RealEqual (pnts[0].y, pnts[length(pnts)-1].y) then begin
    pRepeat^ := false;
  end;

  SetRepeat;
end;

procedure TfmEnlargeProfile.FormCreate(Sender: TObject);
begin
  initialised := false;
  SetFormPos (TForm(self));
end;

procedure TfmEnlargeProfile.FormResize(Sender: TObject);
begin
  DrawProfile;
end;

procedure TfmEnlargeProfile.rbStretchRepeatClick(Sender: TObject);
begin
  pRepeat^ := rbRepeat.Checked;
  if (not RealEqual (firstpnt.y, lastpnt.y)) and pRepeat^ then begin
    {I have chosen to leave the repeat button enabled so that I can give an explanation why it is not available if they try to select it}
    MessageDlg('Repeat is only valid when the starting and ending enlargements are the same', mtError, [mbOK], 0);
    pRepeat^ := false;
    rbStretcb.checked := true;
  end;
  btnRptDisReset.enabled := pRepeat^;
end;


end.
