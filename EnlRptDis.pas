unit EnlRptDis;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, DcadEdit;

type
  TfmEnlRptDis = class(TForm)
    dceRptDis: TDcadEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    PflWidth : double;
    procedure SetWidth (width : double);
    function GetWidth : double;
  end;

var
  fmEnlRptDis: TfmEnlRptDis;

implementation

{$R *.dfm}

  procedure TfmEnlRptDis.SetWidth (width : double);
  begin
    dceRptDis.NumValue := width;
  end;

  function TfmEnlRptDis.GetWidth : double;
  begin
    result := dceRptDis.NumValue;
  end;

procedure TfmEnlRptDis.Button1Click(Sender: TObject);
begin
  dceRptDis.NumValue := PflWidth;
end;

end.
