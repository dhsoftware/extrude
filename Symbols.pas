unit Symbols;

interface

uses    Windows, Dialogs, URecords, sysutils, strutils, Vcl.Forms ;

function GetSymbolFile (var FileName : symstr; Initpath : string;
                        dchandle : HWND;
                        existingFile : boolean = true) : boolean;

implementation

function GetSymbolFile (var FileName : symstr; Initpath : string;
                        dchandle : HWND;
                        existingFile : boolean = true) : boolean;
var
  OpenDialog : TOpenDialog;
  s : string;
begin
  result := false;
//  dchandle := GetForegroundWindow;    // get DataCADs handle so it can be moved back to the top when file dialog exits

  OpenDialog := TOpenDialog.Create(Application);
  try
    OpenDialog.DefaultExt := 'dsf';
    OpenDialog.Title := 'Select Symbol File';
    OpenDialog.InitialDir := initpath;
    OpenDialog.FileName := string(FileName);
    if ExistingFile then
      OpenDialog.Options := [ofFileMustExist]
    else
      OpenDialog.Options := [];
    OpenDialog.Filter := 'DataCAD Symbol Files|*.dsf';
    if OpenDialog.Execute then begin
      result := true;
      s := OpenDialog.FileName;
      if s.EndsWith ('.dsf') then
        setlength (s, length(s)-4);
      FileName := shortstring (s);
    end;
  finally
    SetWindowPos(dchandle, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOSIZE);  // return to DataCAD ... for some reason it doesn't do this automatically
    OpenDialog.Free;
  end;
end;



end.
