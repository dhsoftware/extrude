unit Profile;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, UInterfaces,
  URecords, UInterfacesRecords, UConstants, math, System.UITypes, System.Generics.Collections,
  Extents, PlnUtil, Util, CommonTypes, Symbols, Constants, Settings;

type
  TFmProfile = class(TForm)
    Image1: TImage;
    cbHorizFlip: TCheckBox;
    cbVertFlip: TCheckBox;
    cbRotate90: TCheckBox;
    cbRotate180: TCheckBox;
    btnSaveSymbol: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    function DrawProfile : boolean;
    procedure chChange(Sender: TObject);
    procedure btnSaveSymbolClick(Sender: TObject);
  private
    { Private declarations }
    NoResize : boolean;
    initialised : boolean;
    pProfile : ^tProfiles;
    pThickness : ^double;
    pSymPath : ^SymStr;
    pProfileArrays : ^TList<tProfileArray>;
    dchandle : HWND;
  public
    { Public declarations }
    plnWarningGiven : boolean;
    procedure SetProfileData (pPfl : pointer; pThick : pointer; pSymbolPath : pointer; pPflArrays : pointer; dchndl : HWND);
  end;

implementation

{$R *.dfm}

procedure TFmProfile.SetProfileData (pPfl : pointer; pThick : pointer; pSymbolPath : pointer; pPflArrays : pointer; dchndl : HWND);
begin
  pProfile := pPfl;
  pThickness := pThick;
  pSymPath := pSymbolPath;
  pProfileArrays := pPflArrays;
  dcHandle := dchndl;
  initialised := true;
end;


procedure TFmProfile.btnSaveSymbolClick(Sender: TObject);

var
  ent : entity;
  mode : mode_type;
  sym : sym_type;
  symname, fname : symstr;
  s : string;
  increment, ang, w, h : double;
  pnt, prevpnt, newpnt : point;
  pntlist : TList<point>;
  i, j, res : integer;
  atr : attrib;

begin
  if pProfile^.Items.Count < 1 then begin
    Beep;
    exit;
  end;

  symname := '';

  with pProfile^.Items[0] do case ProfileType of
    p_circle, p_polygon, p_rectangle : begin
        {$region 'process parameter profiles'}
        case ProfileType of
          p_circle: begin
              cvdisst (radius, symname);
              s := 'Circle' + string(symname);
              ValidFileName(s);
              symname := symstr(s);
              if cportion = complete then begin
                ent_init (ent, entcrc);
                setpoint (ent.crccent, 0);
                ent.crcrad := radius;
                ent.crcbase := 0;
                ent.crchite := 0;
              end
              else begin
                ent_init (ent, entarc);
                setpoint (ent.arccent, 0);
                ent.arcrad := radius;
                case cportion of
                  tophalf : begin
                      ent.arcbang := 0;
                      symname := symname + 'top';
                    end;
                  lefthalf : begin
                      ent.arcbang := halfpi;
                      symname := symname + 'left';
                    end;
                  bottomhalf : begin
                      ent.arcbang := pi;
                      symname := symname + 'bottom';
                    end;
                  righthalf : begin
                      ent.arcbang := pi + halfpi;
                      symname := symname + 'right';
                    end;
                end;
                ent.arceang := ent.arcbang + pi;
                ent.arcbase := 0;
                ent.archite := 0;
              end;
            end;
          p_polygon: begin
              cvdisst (plyradius, symname);
              s := 'Polygon' + IntToStr(numsides) + '_' + string(symname);
              ValidFileName(s);
              symname := symstr(s);
              ent_init (ent, entply);
              pntlist := TList<point>.Create;
              try
                increment := TwoPi / numsides;
                if VertFlip in pProfile^.Modifier then
                  increment := -increment;
                if HorizFlip in pProfile^.Modifier then
                  increment := -increment;
                ang := -halfpi - increment/2;
                if Rot90 in pProfile^.Modifier then
                  ang := ang + halfpi;
                if Rot180 in pProfile^.Modifier then
                  ang := ang + pi;

                cylind_cart (plyradius, ang-increment, 0, prevpnt.x, prevpnt.y, prevpnt.z);
                for i := 1 to numsides do begin
                  cylind_cart (plyradius, ang, 0, pnt.x, pnt.y, pnt.z);
                  case pportion of
                    complete :
                      begin
                        pntlist.Add(pnt);
                      end;
                    lefthalf :
                      begin
                        if RealEqual(pnt.x, 0) then begin
                          pnt.x := 0;
                          pntlist.Add(pnt);
                        end
                        else if (pnt.x < 0) and (prevpnt.x <= 0) then
                          pntlist.Add(pnt)
                        else if (pnt.x < 0) and (prevpnt.x > 0) then begin
                          newpnt.y := pnt.y  + (pnt.y - prevpnt.y)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.x := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                          pntlist.Add(pnt);
                        end
                        else if (pnt.x > 0) and (prevpnt.x < 0) then begin
                          newpnt.y := pnt.y  + (pnt.y - prevpnt.y)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.x := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                        end;
                      end;
                    righthalf :
                      begin
                        if RealEqual(pnt.x, 0) then begin
                          pnt.x := 0;
                          pntlist.Add(pnt);
                        end
                        else if (pnt.x > 0) and (prevpnt.x >= 0) then
                          pntlist.Add(pnt)
                        else if (pnt.x > 0) and (prevpnt.x < 0) then begin
                          newpnt.y := pnt.y  + (pnt.y - prevpnt.y)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.x := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                          pntlist.Add(pnt);
                        end
                        else if (pnt.x < 0) and (prevpnt.x > 0) then begin
                          newpnt.y := pnt.y  + (pnt.y - prevpnt.y)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.x := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                        end;
                      end;
                    tophalf :
                      begin
                        if RealEqual(pnt.y, 0) then begin
                          pnt.y := 0;
                          pntlist.Add(pnt);
                        end
                        else if (pnt.y > 0) and (prevpnt.y >= 0) then
                          pntlist.Add(pnt)
                        else if (pnt.y > 0) and (prevpnt.y < 0) then begin
                          newpnt.x := pnt.x  + (pnt.x - prevpnt.x)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.y := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                          pntlist.Add(pnt);
                        end
                        else if (pnt.x < 0) and (prevpnt.x > 0) then begin
                          newpnt.x := pnt.x  + (pnt.x - prevpnt.x)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.y := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                        end;
                      end;
                    bottomhalf :
                      begin
                        if RealEqual(pnt.y, 0) then begin
                          pnt.y := 0;
                          pntlist.Add(pnt);
                        end
                        else if (pnt.y < 0) and (prevpnt.y <= 0) then
                          pntlist.Add(pnt)
                        else if (pnt.y < 0) and (prevpnt.y > 0) then begin
                          newpnt.x := pnt.x  + (pnt.x - prevpnt.x)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.y := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                          pntlist.Add(pnt);
                        end
                        else if (pnt.x > 0) and (prevpnt.x < 0) then begin
                          newpnt.x := pnt.x  + (pnt.x - prevpnt.x)*pnt.x/(pnt.x-prevpnt.x);
                          newpnt.y := 0;
                          newpnt.z := 0;
                          pntlist.Add(newpnt);
                        end;
                      end;
                  end;
                  ang := ang + increment;
                  prevpnt := pnt;
                end;
                ent.plynpnt := pntlist.Count;
                for i := 1 to pntlist.Count do begin
                  ent.plypnt[i] := pntlist[i-1];
                  j := i mod pntlist.Count; // index of next point in pntlist
                  if ((pportion in [righthalf, lefthalf]) and (pntlist[i].x = 0) and (pntlist[j].x = 0)) or
                     ((pportion in [tophalf, bottomhalf]) and (pntlist[i].y = 0) and (pntlist[j].y = 0)) then
                    ent.plyisln[i] := false;
                end;
              finally
                pntlist.Free;
              end;
            end;
          p_rectangle : begin
              cvdisst (width, symname);
              s := 'Rect' + '_' + string(symname);
              cvdisst (height, symname);
              s := s + 'x' + string(symname);
              ValidFileName(s);
              symname := symstr(s);

              ent_init (ent, entply);
              ent.plynpnt := 4;
              if Rot90 in pProfile^.Modifier then begin
                h := width/2;
                w := height/2;
              end
              else begin
                h := height/2;
                w := width/2;
              end;
              ent.plypnt[1].x := -w;
              ent.plypnt[1].y := -h;
              ent.plypnt[1].z := 0;
              ent.plypnt[2].x := w;
              ent.plypnt[2].y := -h;
              ent.plypnt[2].z := 0;
              ent.plypnt[3].x := w;
              ent.plypnt[3].y := h;
              ent.plypnt[3].z := 0;
              ent.plypnt[4].x := -w;
              ent.plypnt[4].y := h;
              ent.plypnt[4].z := 0;
              if rportion = bottomhalf then begin
                ent.plypnt[3].y := 0;
                ent.plyisln[3] := false;
                ent.plypnt[4].y := 0;
                symname := symname + '_btm';
              end
              else if rportion = tophalf then begin
                ent.plypnt[1].y := 0;
                ent.plyisln[1] := false;
                ent.plypnt[2].y := 0;
                symname := symname + '_top';
              end
              else if rportion = lefthalf then begin
                ent.plypnt[2].x := 0;
                ent.plyisln[2] := false;
                ent.plypnt[3].x := 0;
                symname := symname + '_lft';
              end
              else if rportion = righthalf then begin
                ent.plypnt[4].x := 0;
                ent.plyisln[4] := false;
                ent.plypnt[1].x := 0;
                symname := symname + '_rht';
              end;
            end;
        end;

        fname := symname;
        if GetSymbolFile (fname, string(pSymPath^), dcHandle, false) then begin
          ent_add (ent);
          if ent_get (ent, ent.addr) then begin
            ent.color := clrwhite;
            ent_update (ent);
            atr_init (atr, atr_dis);
            atr.name := dhExtShelTk;
            atr.dis := pThickness^;
            atr_add2ent (ent, atr);
            setpoint (pProfile^.refpnt, 0);
            symname := symstr(ExtractFileName(string(fname)));

            ssClear (snap_ss);  // use sel set 11 for temporary as suggested by Dave G (https://www.datacad.com/bbs/viewtopic.php?f=14&t=15931&p=71360&hilit=selection+set#p71360)
            ssAdd (snap_ss, ent);
            mymodeinit(mode);
            mode_ss (mode, snap_ss);
            symcreate (sym, mode, pProfile^.refpnt, symname, true, false);
            ssClear (snap_ss);  // clear temporary ss after used - see comments by Dave G in above referenced DDN forum thread
            atr_init (atr, atr_dis);
            atr.name := dhExtShelTk;
            atr.dis := pThickness^;
            atr_add2sym (sym, atr);
            res := sym_write (sym, fname + '.dsf');
            if res = fl_ok then
              wrterr ('Symbol Saved')
            else begin
              wrterr (shortstring('Error ' + IntToStr(res) + ' saving Symbol'));
            end;

            // save path
            pSymPath^ := symstr(ExtractFilePath (string(fname)));
            SaveStr (dhExtSymPath, string(pSymPath^), true);
          end;
        end;
        {$endregion 'parameter profiles'}
      end;
    p_entity : begin
        fname := '';
        if GetSymbolFile (fname, string(pSymPath^), dcHandle, false) then begin
          ssClear (snap_ss);  // use sel set 11 for temporary as suggested by Dave G (https://www.datacad.com/bbs/viewtopic.php?f=14&t=15931&p=71360&hilit=selection+set#p71360)
          for i := 0 to pProfile^.Items.Count-1 do
            if ent_get (ent, pProfile^.Items[i].adr) then
              ssAdd (snap_ss, ent);
          mymodeinit(mode);
          mode_ss (mode, snap_ss);
          symcreate (sym, mode, pProfile^.refpnt, fname, false, false);
          atr_init (atr, atr_dis);
          atr.name := dhExtShelTk;
          atr.dis := pThickness^;
          atr_add2sym (sym, atr);
          res := sym_write (sym, fname + '.dsf');
          if res = fl_ok then
            wrterr ('Symbol Saved')
          else begin
            wrterr (shortstring('Error ' + IntToStr(res) + ' saving Symbol'));
          end;
          ssClear (snap_ss);  // clear temporary ss after used - see comments by Dave G in above referenced DDN forum thread

          // save path
          pSymPath^ := symstr(ExtractFilePath (string(fname)));
          SaveStr (dhExtSymPath, string(pSymPath^), true);
        end;
      end;
  end;
  self.BringToFront;
end;


procedure TFmProfile.chChange(Sender: TObject);
var
  modifier : set of tModifier;
begin
  modifier := pProfile^.modifier;
  modifier := [];
  if cbHorizFlip.Checked then
    include (modifier, HorizFlip);
  if cbVertFlip.Checked then
    include (modifier, VertFlip);
  if cbRotate90.Checked then
    include (modifier, Rot90);
  if cbRotate180.Checked then
    include (modifier, Rot180);
  pProfile^.Modifier := modifier;
  DrawProfile;
end;


function TfmProfile.DrawProfile : boolean;
// returns false if there are no profile entities to draw
var
  ProfileArray : tProfileArray;
  i, w, h, leftmargin, bottommargin : integer;
  ar, br, lr, rr, UsableWidth, UsableHeight : double;
  scaling : double;
  p, p1, p2 : point;
  lbl : string;

function AdjustForModifiers (const pnt : point) : point;
var
  r : double;
begin
  result := pnt;

  if HorizFlip in pProfile^.Modifier then
    result.x := -pnt.x;
  if VertFlip in pProfile^.Modifier then
    result.y := -pnt.y;
  if Rot90 in pProfile^.Modifier then begin
    r := result.y;
    result.y := result.x;
    result.x := -r;
  end;
  if Rot180 in pProfile^.Modifier then begin
    result.x := -result.x;
    result.y := -result.y;
  end;
end;

function LabelText : string;
var
  s : str255;
begin
  case pProfile^.Items[0].ProfileType of
    p_polygon : begin
      cvdisst (pProfile^.Items[0].plyradius, s);
      result := 'Radius:' + string(s);
    end;
    p_circle : begin
      cvdisst (pProfile^.Items[0].radius, s);
      result := 'Radius:'  + string(s);
    end;
    p_rectangle : begin
      cvdisst (pProfile^.Items[0].width, s);
      result := string(s) + ' x ';
      cvdisst (pProfile^.Items[0].height, s);
      result := result + string(s);
    end
    else Result := '';
  end
end;

begin
  result := false;
  if (not initialised) or (pProfileArrays^.Count < 1) then
    exit;


  cbHorizFlip.Enabled := true;
  cbVertFlip.Enabled := true;
  cbRotate90.Enabled := true;
  cbRotate180.Enabled := true;

  cbHorizFlip.Checked := HorizFlip in pProfile^.Modifier;
  cbVertFlip.Checked := VertFlip in pProfile^.Modifier;
  cbRotate90.Checked := Rot90 in pProfile^.Modifier;
  cbRotate180.Checked := Rot180 in pProfile^.Modifier;


  with Image1 do begin
    {$region 'Initialise the Canvas'}
    if (Image1.Width > 0) and (Image1.Height > 0) then begin
      Picture.Bitmap.Width := Image1.Width;
      Picture.Bitmap.Height := Image1.Height;
      Canvas.Pen.Color := clWhite;
      Canvas.Brush.Color := clWhite;
      Canvas.Rectangle(0,0, Image1.Width, Image1.height);
      Canvas.Pen.Color := clBlack;
      Canvas.Brush.Style := bsClear;
    end;
    {$endregion}

    {$region 'Calculate scaling required to fit image size'}
    {$region 'Find extents for the profile'}
    ar := 0;   //above ref
    br := 0;   //below ref
    lr := 0;   //left of ref
    rr := 0;   //right of reg

    for ProfileArray in pProfileArrays^ do begin
      for i := 0 to length(ProfileArray.points)-1 do begin
        p := AdjustForModifiers (ProfileArray.points[i].p);
        if p.x < lr then
          lr := p.x;
        if p.x > rr then
          rr := p.x;
        if p.y < br then
          br := p.y;
        if p.y > ar then
          ar := p.y
      end;
    end;
    {$endregion 'Find extents for the profile'}

    if (ar = 0) and (br = 0) and (lr = 0) and (rr = 0) then
      exit;

    UsableWidth := floor (width * 0.99 - 1.9);
    UsableHeight := floor (height * 0.99 - 2.9);
    scaling :=  max ((ar-br) / UsableHeight, (rr-lr) / UsableWidth);

    leftmargin := ceil(width - (rr-lr) / scaling) div 2;
    bottommargin := ceil(height - (ar-br) / scaling) div 2;

    {$endregion}

    Canvas.Pen.Width := min (1, round(cbHorizFlip.Height / 30));  // set pen width using height of a checkbox control as reference
    btnSaveSymbol.Enabled := pProfile^.Items[0].ProfileType <> p_symbol;

    {$region 'Draw Profile Entities on Canvas'}
    for ProfileArray in pProfileArrays^ do begin
      for i := length(ProfileArray.points)-1 downto 0 do begin    // loop through points backwards so grey thickness lines (if any) are drawn first and do not go over the top of black lines
        if ProfileArray.points[i].visible then begin
          if ProfileArray.points[i].thicknessLine then
            Canvas.Pen.Color := clLtGray
          else
            Canvas.Pen.Color := clBlack;

          p1 := AdjustForModifiers(ProfileArray.points[i].p);
          p2 := AdjustForModifiers(ProfileArray.points[(i+1) mod length(ProfileArray.points)].p);

          if (i < (length(ProfileArray.points)-1)) or ProfileArray.closed then begin
            Canvas.MoveTo(round((p1.x - lr)/scaling) + leftmargin,
                          height - round((p1.y - br)/scaling) - bottommargin);
            Canvas.LineTo(round((p2.x - lr)/scaling) + leftmargin,
                          height - round((p2.y - br)/scaling) - bottommargin);
          end;
          result := true;
        end; // visible
      end;
    end;
    {$endregion 'Draw Profile Entities on Canvas'}

    if pProfile^.Items[0].ProfileType in [p_circle, p_polygon, p_rectangle] then begin
      Canvas.Font := cbHorizFlip.Font;
      Canvas.Font.Size := Canvas.Font.Size+1;
      lbl := LabelText;
      repeat
        Canvas.Font.Size := Canvas.Font.Size-1;
        w := Canvas.TextWidth (string(lbl));
        h := Canvas.TextHeight(string(lbl));
      until (w < Image1.Width) or (Canvas.Font.Size = 4);
      w := (Image1.Width - w) div 2;
      h := Image1.Height div 2 - h;
      Canvas.Font.Color := clPurple;
      Canvas.TextOut(w, h, string(lbl));
    end;

    Canvas.Pen.Color := clRed;
    Canvas.Brush.Color := clRed;
    Canvas.Ellipse(round(-lr/scaling)+leftmargin-Canvas.Pen.Width*2, height + round(br/scaling)-Canvas.Pen.Width*2 - bottommargin,
                   round(-lr/scaling)+leftmargin+Canvas.Pen.Width*2, height + round(br/scaling)+Canvas.Pen.Width*2 - bottommargin);
    Canvas.Brush.Style := bsClear;
  end;

  if result then begin
    NoResize := true;
    Visible := true;
    NoResize := false;
  end

end;



procedure TFmProfile.FormCreate(Sender: TObject);
begin
  initialised := false;
  NoResize := false;
  SetFormPos (TForm(self));
end;

procedure TFmProfile.FormResize(Sender: TObject);
begin
  if not noResize then
    DrawProfile;
end;

end.
