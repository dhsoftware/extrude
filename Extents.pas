unit Extents;

interface

uses UInterfaces, URecords, UInterfacesRecords, UConstants, Math;

procedure EntExtent (var ent: entity; var min, max: point);

implementation

procedure EntExtent (var ent: entity; var min, max: point);
var
  adr : lgl_addr;
  pv : polyvert;
  cent : point;
  rad, bang, eang : double;
  ccw : boolean;
begin
  ent_extent (ent, min, max);
  if ent.enttype = entpln then begin  // ent_extent can return incorrect values for polylines with a bulge
    adr := ent.plnfrst;
    while polyvert_get (pv, adr, ent.plnfrst) do begin
      adr := pv.Next;
      if not pv.last then begin
        if (pv.shape = pv_bulge) then begin
          min.x := Math.Min(min.x, Math.Min(pv.pnt.x, pv.nextpnt.x));
          min.y := Math.Min(min.y, Math.Min(pv.pnt.y, pv.nextpnt.y));
          max.x := Math.Max(max.x, Math.Max(pv.pnt.x, pv.nextpnt.x));
          max.y := Math.Max(max.y, Math.Max(pv.pnt.y, pv.nextpnt.y));
          bulge_to_arc (pv.pnt, pv.nextpnt, pv.bulge, cent, rad, bang, eang, ccw);
          if not ccw then
            swapaFloat (bang, eang);
          angnormalize (bang);
          angnormalize (eang);
          if bang > eang then
            max.x := Math.Max (max.x, cent.x + rad);
          if ((bang < pi) and (eang > pi)) or ((bang > eang) and ((bang < pi) or (eang > pi))) then
            min.x := Math.Min (min.x, cent.x - rad);
          if ((bang < halfpi) and (eang > halfpi)) or ((bang > eang) and ((bang < halfpi) or (eang > halfpi))) then
            max.y := Math.Max (max.y, cent.y + rad);
          if ((bang  < pi32) and (eang > pi32)) or ((bang > eang) and ((bang < pi32) or (eang > pi32))) then
            min.y := Math.Min (min.y, cent.y - rad);
        end;
      end;
    end;
  end
  else if ent.enttype = entEll then begin
    //todo - this does not cater for partial ellipses or those that have been rotated (needs more work)
    min.x := Math.Min (min.x, ent.EllCent.x - ent.ellrad.x);
    max.x := Math.Max (max.x, ent.EllCent.x + ent.ellrad.x);
    min.y := Math.Min (min.y, ent.EllCent.y - ent.ellrad.y);
    max.y := Math.Max (max.y, ent.EllCent.y + ent.ellrad.y);
  end;
end;

end.
