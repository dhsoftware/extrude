unit Save;

interface

uses Settings, CommonStuff, CommonTypes, URecords, Constants;

procedure SaveZTreatment;


implementation


procedure SaveZTreatment;
var
  s : str80;
begin
  s := '        ';
  if Base in l^.zTreatment then
    s[1] := 'B';
  if Hite in l^.zTreatment then
    s[2] := 'H';
  if Slope in l^.zTreatment then
    s[3] := 'S';
  if RevSlope in l^.zTreatment then
    s[4] := 'R';
  if zProfile in l^.zTreatment then
    s[5] := 'P';
  if verticals in l^.zTreatment then
    s[6] := 'V';
  if zMid in l^.zTreatment then
    s[7] := 'M';
  if BasePlus in l^.zTreatment then
    s[8] := '+';
  SaveStr (dhExtZTrtmt, string(s), false);
  SaveRl(dhZBsPlusDis, l^.ZBasePlus, true);
end;

end.
