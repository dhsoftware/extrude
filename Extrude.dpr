﻿library extrude;

uses
  Windows,
  Dialogs,
  Forms,
  Vcl.Controls,
  Math,
  SysUtils,
  System.UITypes,
  System.Generics.Collections,
  Settings in 'Settings.pas',
  PolyParam in 'PolyParam.pas' {FmPolyParam},
  CommonStuff in 'CommonStuff.pas',
  Symbols in 'Symbols.pas',
  Profile in 'Profile.pas' {FmProfile},
  Enlargement in 'Enlargement.pas' {fmEnlargeProfile},
  Extents in 'Extents.pas',
  Z_Profile in 'Z_Profile.pas' {fmZProfile},
  PlnUtil in '..\Common\PlnUtil.pas',
  Util in '..\Common\Util.pas',
  UConstants in '..\..\d4d_headerfiles\14\UConstants.pas',
  UDCLibrary in '..\..\d4d_headerfiles\14\UDCLibrary.pas',
  UInterfaces in '..\..\d4d_headerfiles\14\UInterfaces.pas',
  UInterfacesRecords in '..\..\d4d_headerfiles\14\UInterfacesRecords.pas',
  URecords in '..\..\d4d_headerfiles\14\URecords.pas',
  UVariables in '..\..\d4d_headerfiles\14\UVariables.pas',
  Path in 'Path.pas',
  ConfirmParameters in 'ConfirmParameters.pas' {fmConfirmParam},
  DoExtrusion in 'DoExtrusion.pas',
  Save in 'Save.pas',
  Offset in 'Offset.pas',
  EntToPntArr in 'EntToPntArr.pas',
  CommonTypes in 'CommonTypes.pas',
  Constants in 'Constants.pas',
  ProcessProfile in 'ProcessProfile.pas',
  AdvancedOptions in 'AdvancedOptions.pas' {fmAdvanced},
  About in 'About.pas' {fmAbout},
  RegPrompt in 'RegPrompt.pas' {RegistationPrompt},
  DcadEdit in '..\datacad-components\DcadEdit.pas',
  EnlRptDis in 'EnlRptDis.pas' {fmEnlRptDis},
  Licence in 'Licence.pas' {fmLicence};

{$E dmx}
{$R *.res}

//=== NOTE : ver string defined in Constants module =====//

procedure PrepareToExit;
var
  mode : mode_type;
  ent : entity;
  atr : attrib;
  adr : lgl_addr;
  i : integer;

  procedure SaveModifier (modifier : tModifierSet);
  var
    s : string[4];
  begin
    s := '    ';
    if VertFlip in modifier then
      s[1] := 'v';
    if HorizFlip in modifier then
      s[2] := 'h';
    if Rot90 in modifier then
      s[3] := 'r';
    if Rot180 in modifier then
      s[4] := 'R';
    savestr (dhExtProfMod, string(s), false);
  end;

begin
  if l^.profile.Items.Count > 0 then begin
    SaveInt (dhExtProfTyp, ord(l^.profile.Items[0].ProfileType), false);
    case l^.profile.Items[0].ProfileType of
      p_circle : begin
          SaveRl(dhExtCircRad, l^.profile.Items[0].radius, false);
          SaveInt (dhExtPortion, ord(l^.profile.Items[0].cportion), false);
          SaveModifier(l^.profile.Modifier);;
        end;
      p_polygon : begin
          SaveRl(dhExtPlyRad, l^.profile.Items[0].plyradius, false);
          SaveInt (dhExtPlyNSid, l^.profile.Items[0].numsides, false);
          SaveInt (dhExtPortion, ord(l^.profile.Items[0].pportion), false);
          SaveModifier(l^.profile.Modifier);;
       end;
      p_rectangle : begin
          SaveRl (dhExtRWidth, l^.profile.Items[0].width, false);
          SaveRl (dhExtRHeight, l^.profile.Items[0].height, false);
          SaveInt (dhExtPortion, ord(l^.profile.Items[0].rportion), false);
          SaveModifier(l^.profile.Modifier);;
        end;
      p_entity : begin
          MyModeInit (mode);
          mode_lyr (mode, lyr_all);
          mode_atr (mode,  dhExtProfEnt);
          adr := ent_first (mode);
          while ent_get (ent, adr) do begin
            adr :=ent_next (ent, mode);
            if atr_entfind (ent, dhExtProfEnt, atr) then
              atr_delent (ent, atr);  // remove any existing save profile entities
          end;
          for i := 0 to l^.profile.Items.Count-1 do begin
            if ent_get (ent, l^.profile.Items[i].adr) then begin
              atr_init (atr, atr_int);
              atr.name := dhExtProfEnt;
              atr.int := l^.profile.Items.Count;  // value used to validate that all selected entities still exist if you re-enter the macro and use 'previous' profile
              atr.Visible := false;
              atr_add2ent (ent, atr);
            end;
          end;
          SaveModifier(l^.profile.Modifier);;
          SavePnt(dhExtRefPnt, l^.profile.refpnt);
        end;
      p_symbol : begin
          savestr (dhExtProfSym, string(l^.profile.Items[0].name), false);
          SaveModifier(l^.profile.Modifier);;
        end;
    end;
  end;

  SaveBool (dhExtShwOptn, l^.ShowOptions, false);
  SaveBool (dhExtEnlRpt, l^.EnlRepeat, true);


  if l^.initialised then begin
    l^.profile.Items.Free;
    l^.lastprofile.Items.Free;
    l^.pathEnts.Free;
    if l^.ProfileFormCreated then begin
      SaveFormPos(TForm(l^.FmProfile));
      try
        l^.FmProfile.Free;
      except
//        on E : Exception do
//          ShowMessage(E.Message);      // not sure why exception is sometimes happening here ....
      end;
      l^.ProfileFormCreated := false;
    end;
    if l^.EnlProfFormCreated then begin
      SaveFormPos (TForm(l^.fmEnlargeProfile));
      l^.fmEnlargeProfile.Free;
      l^.EnlProfFormCreated := false;
    end;
    if l^.ZProfFormCreated then begin
      SaveFormPos (TForm(l^.fmZProfile));
      l^.fmZProfile.Free;
      l^.ZProfFormCreated := false;
    end;
    if l^.ProfileArraysCreated then
      l^.ProfileArrays.Free;
    if l^.tempprofilesCreated then
      l^.tempprofiles.Items.Free;
  end;
  l^.initialised := false;
end;

procedure ProcessFormVisibility;
begin
  if l^.ProfileFormCreated and l^.FmProfile.Visible then with l^.FmProfile do begin
    if l^.WindowFlags[0] = 1 then
      SetWindowPos(Handle, HWND_TOPMOST, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE)
    else if l^.WindowFlags[0] = 2 then
      SetWindowPos(Handle, HWND_NOTOPMOST, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE)
    else
      SetWindowPos(Handle, HWND_TOP, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);
  end;
  if l^.EnlProfFormCreated and l^.FmEnlargeProfile.Visible then with l^.fmEnlargeProfile do begin
    if l^.WindowFlags[1] = 1 then
      SetWindowPos(Handle, HWND_TOPMOST, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE)
    else if l^.WindowFlags[1] = 2 then
      SetWindowPos(Handle, HWND_NOTOPMOST, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE)
    else
      SetWindowPos(Handle, HWND_TOP, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);
  end;
  if l^.ZProfFormCreated and l^.FmZProfile.Visible then with l^.fmZProfile do
    SetWindowPos(Handle, HWND_TOP, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);
end;

procedure LoadPreviousProfile;
var
{  filler : byte;  // without this filler a mode_init call using the mode variable
                  // corrupts adjacent variables -  do not include optimise as part
                  // of the build configuration     }
  mode : mode_type;
  i : integer;
  profile : tProfile;
  adr : lgl_addr;
  ent : entity;
  atr : attrib;

  function SavedModifier  : tModifierSet;
  var
    s : shortstring;
  begin
    s := shortstring (GetSvdStr(dhExtProfMod, '    ', false));
    result := [];
    if (s[1] = 'v') then
      result := result + [VertFlip];
    if s[2] = 'h' then
      result := result + [HorizFlip];
    if s[3] = 'r' then
      result := result + [Rot90];
    if s[4] = 'R' then
      result := result + [Rot180];
  end;

begin
  l^.lastprofile.Items.Clear;
  i := GetSvdInt (dhExtProfTyp, -1);
  if (i < ord(p_entity)) or (i > ord(p_rectangle)) then
    exit;

  profile.ProfileType := tProfileType(i);
  case profile.ProfileType of
    p_entity : begin
        if not GetSvdPoint (dhExtRefPnt, l^.lastprofile.refpnt) then
          exit;
        l^.lastprofile.Modifier := SavedModifier;
        MyModeInit (mode);
        mode_lyr (mode, lyr_all);
        mode_atr (mode, dhExtProfEnt);
        adr := ent_first (mode);
        i := 0;
        while ent_get (ent, adr) do begin
          adr := ent_next (ent, mode);
          if ent.enttype in [entlin, entcrc, entarc, entell, entpln, entply, entbez, entbsp] then begin
            i := i+1;
            if i = 1 then
              if (not atr_entfind (ent, dhExtProfEnt, atr)) or (atr.atrtype <> atr_int) then begin
                setnil (adr);
                atr.int := 0;
              end;
            profile.adr := ent.addr;
            l^.lastprofile.Items.Add(profile);
          end;
        end;
        if (i <> atr.int) then
          l^.lastprofile.Items.Clear;  // some entities used for the previous profile no longer exist, so do not use itv
      end;
    p_symbol : begin
        profile.name := symstr(GetSvdStr(dhExtProfSym, ''));
        if (profile.name > '') and fileexists (string(profile.name)+'.dsf') then
          l^.lastprofile.Items.Add(profile);
        l^.lastprofile.Modifier := SavedModifier;
      end;
    p_circle : begin
        profile.radius := GetSvdRl(dhExtCircRad, 0);
        profile.cportion := tPortion(GetSvdInt(dhExtPortion, ord(complete)));
        l^.lastprofile.Modifier := SavedModifier;
        if profile.radius > 0 then
          l^.lastprofile.Items.Add(profile);
      end;
    p_polygon : begin
        profile.plyradius := GetSvdRl (dhExtPlyRad, 0);
        profile.pportion := tPortion(GetSvdInt(dhExtPortion, ord(complete)));
        profile.numsides := GetSvdInt(dhExtPlyNSid, 0);
        l^.lastprofile.Modifier := SavedModifier;
        if (profile.plyradius > 0) and (profile.numsides >= 3) and (profile.numsides <= 20) then
          l^.lastprofile.Items.Add(profile);
      end;
    p_rectangle : begin
        profile.width := GetSvdRl (dhExtRWidth, 0);
        profile.height := GetSvdRl (dhExtRHeight, 0);
        profile.rportion := tPortion(GetSvdInt(dhExtPortion, ord(complete)));
        l^.lastprofile.Modifier := SavedModifier;
        if (profile.height > 0 ) and (profile.width > 0) then
          l^.lastprofile.Items.Add(profile);
      end;
  end;

end;



function extrude_Main(act : action; pl, pargs : Pointer) : wantType;
var
  retval : asint;
  ent, firstent, tempent : entity;
  i, len : integer;
  atr : attrib;
  refpnt,minpnt, tmppnt : point;
  temparr : pntarr;
  temppfl : tProfile;
  adr : lgl_addr;
  tempstr1, tempstr2 : str255;
  profile : tProfile;
  s : string;
  d : double;
  mode : mode_type;
  FmRegPrompt :TRegistationPrompt;
  enlLblState : integer;

  procedure MoveProfileToPrevious;
  var
    i : integer;
  begin
    if l^.profile.Items.Count > 0 then begin
      l^.lastprofile.Items.Clear;
      for i := 0 to l^.profile.Items.Count -1 do begin
        profile := l^.profile.Items[i];
        l^.lastprofile.Items.Add (profile);
      end;
    end;
    l^.lastprofile.refpnt := l^.profile.refpnt;
    l^.lastprofile.Modifier := l^.profile.Modifier;
    l^.profile.Items.Clear;
  end;

  procedure SetProfileAtrib;
  begin
    MyModeInit (mode);
    mode_lyr (mode, lyr_all);
    mode_atr (mode, dhExtProfile);
  end;


  procedure DrawTracePath (drmode, drmodeoffs : integer); overload;
  var
    i : integer;
    isOffset, svShowWgt : boolean;
  begin
    svShowWgt := PGSaveVar.showwgt;
    isOffset := false;
    for i := 0 to length(l^.Tracepnts)-2 do begin
      if not RealEqual (l^.Tracepnts[i].ofs, Zero) then
        isOffset := true;
    end;
    ent_init(ent, entlin);
    ent.spacing := pixsize * 12;
    if isOffset or l^.Fillet then
      ent.ltype := ltype_dashed
    else
      ent.ltype := ltype_solid;
    if length(l^.Tracepnts) > 1 then for i := 0 to length(l^.Tracepnts)-2 do begin
      ent.linpt1 := NewPoint(l^.Tracepnts[i].pnt);
      ent.linpt2 := NewPoint(l^.Tracepnts[i+1].pnt);
      ent_draw_dl (ent, drmode, true);
    end;
    if isOffset or (drmode <> drmodeOffs) or l^.Fillet then begin
      ent.ltype := ltype_solid;
      if length(l^.OffsTracepnts) > 1 then
        for i := 0 to length(l^.OffsTracepnts)-1-ord(l^.TraceClosed in [ForceNotClosed, NotClosed]) do begin
          ent.linpt1 := NewPoint(l^.OffsTracepnts[i]);
          ent.linpt2 := NewPoint(l^.OffsTracepnts[(i+1) mod length(l^.OffsTracePnts)]);
          ent_draw_dl (ent, drmodeOffs, true);
        end;
      PGSaveVar.showwgt := svShowWgt;
    end;
  end;

  procedure DrawTracePath (drmode : integer); overload;
  begin
    DrawTracePath (drmode, drmode);
  end;


  procedure ProcessPath;
  var
    pathEnt : entity;
    temparr : EntPntArr;
    mr : integer;
    thick : double;
    profilearray : tProfileArray;
    i : integer;
  begin
    ProcessFormVisibility;
    if l^.ShowOptions then  begin
      thick := l^.ExtrThick;
      fmConfirmParam := TfmConfirmParam.Create(nil);
      mr := fmConfirmParam.ShowModal;
      fmConfirmParam.Free;
      DrawTracePath(drmode_black);
      if mr <> mrOK then
        exit;
      if not realequal (l^.ExtrThick, thick) and l^.ProfileFormCreated then begin
        ProfileToArray (false);
        l^.FmProfile.DrawProfile;
      end;
    end
    else begin
      if ((not isnil(l^.EnlProfile)) or
          (not RealEqual(l^.EndEnl, one)) or (not RealEqual(l^.StartEnl, one))) and
         ((verticals in l^.zTreatment) and ((Hite in l^.zTreatment) or (Base in l^.zTreatment)))
      then begin
        if messagedlg ('Enlargement setting will be ignored:' + sLineBreak + sLineBreak +
                       'Enlargement is not used when Z Settings include Verticals as well as Base and/or Height',
                       mtWarning, [mbOK, mbCancel], 0) = mrCancel then
          exit;
      end;

      DrawTracePath (drmode_black);
      setlength (l^.tracepnts, 0);
      setlength (l^.OffsTracepnts, 0);
    end;


    paths := Tpaths.Create;
    try
      if length (l^.OffsTracePnts) > 0 then begin
        temparr.enttyp := 0;
        temparr.closed := not (l^.traceClosed in [NotClosed, ForceNotClosed]);
        case l^.traceZtype of
          trBase : temparr.zb := zbase;
          trHite : temparr.zb := zhite;
          trUser1 : temparr.zb := PGSv.userz1;
          trUser2 : temparr.zb := PGSv.userz2;
          trSpecified : temparr.zb := l^.traceZ;
        end;
        temparr.zh := temparr.zb;
        setident (temparr.Matrix);
        setlength (temparr.Pnts, length (l^.OffsTracePnts));
        for i := 0 to length(l^.OffsTracepnts)-1 do
          temparr.Pnts[i].p := NewPoint(l^.OffsTracePnts[i]);
        paths.Add(temparr);
      end
      else begin
        if zProfile in l^.zTreatment then
          zProfileToArray;

        for pathEnt in l^.pathEnts do begin
          PathEntToPntArr (pathEnt, paths);
        end;

        ChainPaths (paths);

      end;

      ErrorShown := false;

      ProfileToArray (true);

      if l^.ExtrThick < -abszero then begin
        for i := 0 to l^.ProfileArrays.Count -1 do begin
          ProfileArray := l^.ProfileArrays[i];
          ReverseProfileArray(ProfileArray);
          l^.ProfileArrays[i] := ProfileArray;
        end;
      end;

      stopgroup;

      for temparr in paths do begin
        ExtrudePath (temparr);
        stopgroup;
      end;


    finally
      paths.Free;
    end;

    if l^.profile.Modifier <> [] then
      ProfileToArray (false);   // reset to unmodified state as that is what profile window expects

    l^.pathEnts.Clear;
    l^.state := _path;
  end;


  procedure ShowEnlargementProfile;
  begin
    if (l^.WindowFlags[_WindowEnlarge] = _DoNotShow) or (l^.profile.Items = nil) then
      exit;
    if not l^.EnlProfFormCreated then begin
      l^.fmEnlargeProfile := TfmEnlargeProfile.Create(nil);
        l^.fmEnlargeProfile.SetEnlargePfl (@l^.EnlProfile, @l^.EnlRepeat, @l^.EnlRptDis, @l^.state,
                                         @l^.Divs.EnlDivs, @l^.Divs.EnlEqDivs, @l^.EnlRevers, @l^.EnlXY);
      l^.EnlProfFormCreated := true;
    end;
    l^.fmEnlargeProfile.DrawProfile;
    l^.fmEnlargeProfile.Show;
  end;

  procedure KeepTwistEnlargePrompt;
  var
    answer : integer;
    ent : entity;
    atr : attrib;
  begin
    if not (isnil (l^.EnlProfile)) or (l^.StartEnl <> one) or (l^.EndEnl <> one) then begin
      answer := MessageDlg('Do you wish to keep previously specified enlargement settings?', mtConfirmation, [mbYes, mbNo], 0);
      if answer = mrNo then begin
        l^.StartEnl := 1;
        l^.EndEnl := 1;
        SaveRl(dhExtEnlStrt, l^.StartEnl, false);
        SaveRl(dhExtEnlEnd, l^.EndEnl, false);
        if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then begin
          atr_delent (ent, atr);
          if l^.EnlProfFormCreated then
            l^.fmEnlargeProfile.Hide;
        end;
        setnil (l^.EnlProfile);
      end
      else if not isnil (l.EnlProfile) then
        ShowEnlargementProfile;
    end;
    if l^.twistType <> NoTwist then begin
      answer := MessageDlg('Do you wish to keep previously specified twist settings?', mtConfirmation, [mbYes, mbNo], 0);
      if answer = mrNo then begin
        l^.twistType := NoTwist;
        SaveInt (dhExtTwistTy, ord(noTwist));
      end;
    end;

  end;


  procedure GetAdvancedOptions;
  var
    pflChanged, EnlChanged : boolean;
  begin
    fmAdvanced := TfmAdvanced.Create(nil);
    ProcessFormVisibility;
    try
      if fmAdvanced.ShowModal = mrOK then with fmAdvanced do begin
        l^.WindowFlags[0] := byte (rgPflWdw.ItemIndex);
        l^.WindowFlags[1] := byte (rgEnlWdw.ItemIndex);

        pflChanged := (l^.Divs.PflEqDivs <> chbEqPflDivs.Checked) or
                      (l^.Divs.PflCrc <> sePflCrcArcDivs.Value) or
                      (l^.Divs.PflBez <> cbPflBezSpline.ItemIndex);
        l^.Divs.PflEqDivs := chbEqPflDivs.Checked;
        l^.Divs.PflCrc := sePflCrcArcDivs.Value;
        l^.Divs.PflBez := cbPflBezSpline.ItemIndex;
        if pflChanged then begin
          if l^.ProfileFormCreated then begin
            ProfileToArray (false);
            l^.FmProfile.DrawProfile;
          end;
        end;

        l^.Divs.PthEqDivs := chbEqual.Checked;
        l^.Divs.Crc := seCrcArcDivs.Value;
        l^.Divs.Bez := cbBezSpline.ItemIndex;
        if rbDcadDivs.Checked then
          l^.Divs.Ent3D := 0
        else
          l^.Divs.Ent3D := se3Ddivs.Value;

        EnlChanged := (l^.Divs.EnlEqDivs <> chbEqEnlDivs.Checked) or
                      (l^.Divs.EnlDivs <> seEnlDivs.Value);
        l^.Divs.EnlEqDivs := chbEqEnlDivs.Checked;
        l^.Divs.EnlDivs := seEnlDivs.Value;
        if EnlChanged then begin
          ShowEnlargementProfile;
        end;


        SaveInt (dhExtSPflWdw, l^.WindowFlags[_WindowProfile], true);
        SaveInt (dhExtEPflWdw, l^.WindowFlags[_WindowEnlarge], true);
        SaveBool (dhExtPflEqDv, l^.Divs.PflEqDivs);
        SaveInt (dhExtPflDivs, l^.Divs.PflCrc);
        SaveInt (dhExtPflBDiv, l^.Divs.PflBez);
        SaveBool(dhExtEqDivs, l^.Divs.PthEqDivs);
        SaveInt(dhExtCrcDivs,l^.Divs.Crc);
        SaveInt(dhExtBezDivs, l^.Divs.Bez);
        SaveInt(dhExt3DDivs, l^.Divs.Ent3D);
        SaveBool (dhExtEnlEqDv, l^.Divs.EnlEqDivs);
        SaveInt (dhExtEnlDivs, l^.Divs.EnlDivs);
      end;
    finally
    fmAdvanced.Free;
    end;
  end;

  procedure TempSaveCurrentProfile;
  var
    i : integer;
  begin
    if l^.tempprofilesCreated then begin
      l^.tempprofiles.Items.Clear;
    end
    else begin
      l^.tempprofiles.Items := tProfileItems.Create;
      l^.tempprofilesCreated := true;
    end;
    if l^.ProfileArraysCreated then begin
      l^.tempprofiles.refpnt := l^.profile.refpnt;
      l^.tempprofiles.Modifier := l^.profile.Modifier;
      for i := 0 to l^.profile.Items.Count-1 do begin
        temppfl := l^.profile.Items[i];
        l^.tempprofiles.Items.Add(temppfl);
      end;
    end;
  end;

  procedure RestoreFromTempProfile;
  var
    i : integer;
  begin
    if l^.profile.Items.Count > 0 then begin      {unhilite new selection}
      for i := 0 to l^.profile.Items.Count-1 do
        if (l^.profile.Items[i].ProfileType = p_entity) and ent_get (ent, l^.profile.Items[i].adr) then
          Hilite (ent, 0);
    end;
    if l^.tempprofilesCreated then begin
      l^.profile.refpnt := l^.tempprofiles.refpnt;
      l^.profile.Modifier := l^.tempprofiles.Modifier;
      l^.profile.Items.Clear;
      for i := 0 to l^.tempprofiles.Items.Count-1 do begin
       temppfl := l^.tempprofiles.Items[i];
       l^.profile.Items.Add(temppfl);
      end;
    end;
  end;

  procedure ShowStartRotOption;
  var
    temps : shortstring;
  begin
    if realequal (l^.StartRotation, zero) then begin
      temps := '';
    end
    else begin
      cvangst (l^.StartRotation, temps);
      temps := ' (' + temps + ')';
    end;
    temps := 'Set Profile rotation at start of path' + temps;
    lblsett (16, 'Start Rot', not realequal (l^.StartRotation, zero), temps);
  end;

  procedure ToggleEnlXY (fkey : integer);
  begin
    case fkey of
      f7 : begin
             case l^.EnlXY of
               EnlBoth : l^.EnlXY := EnlY;
               EnlX    : l^.EnlXY := EnlNone;
               EnlNone : l^.EnlXY := EnlX
               else      l^.EnlXY := EnlBoth;
             end;
           end;
      f8 : begin
             case l^.EnlXY of
               EnlBoth : l^.EnlXY := EnlX;
               EnlX    : l^.EnlXY := EnlBoth;
               EnlNone : l^.EnlXY := EnlY
               else      l^.EnlXY := EnlNone;
             end;
           end;
    end;
    SaveInt (dhExtEnlXY, ord(l^.EnlXY), false);
    if l^.EnlProfFormCreated then
      l^.fmEnlargeProfile.DrawProfile;
  end;

  procedure EnlPflRetainedMsg;
  begin
    if not isnil (l^.EnlProfile) then
      wrterr ('Prev Pfl retained - use F0 Reset to clear it');
  end;

  procedure ShowHelp;
  begin
    fmAbout := tfmAbout.Create(nil);
    fmAbout.lblVersion.Caption := ver;
    fmAbout.ShowModal;
    fmAbout.Free;
  end;


begin
  l := PextrudeL(pl);
  if act = aagain then begin
     case l^.state of
        {$region '_Profile'}
        _profile :
          if l^.getp.result = res_escape then begin
            case l^.getp.key of
              f1 : l^.pflsel := s_entity;
              f2 : l^.pflsel := s_group;
              f3 : l^.pflsel := s_area;
              f6 : PGsavevar.srch := not PGsavevar.srch;
              f8 : begin
                    tempstr1 := '';
                    if GetSymbolFile(tempstr1, string(l^.sympath), l^.dcHandle) then begin
                       MoveProfileToPrevious;
                       profile.ProfileType := p_symbol;
                       profile.name := tempstr1;
                       l^.profile.Items.Add(profile);
                       setpoint (l^.profile.refpnt, 0);
                       l^.profile.Modifier := [];
                       l^.state := _path;
                       if l^.ProfileFormCreated then begin
                         ProfileToArray (false);
                         l^.FmProfile.DrawProfile;
                       end;
                       l^.sympath := symstr(ExtractFilePath (string(tempstr1)));
                       SaveStr (dhExtSymPath, string(l^.sympath), true);
                     end;
                   end;
              f9 : begin
                     FmPolyParam := TFmPolyParam.Create(nil);
                     try
                       if l^.profile.Items.Count > 0 then case l^.profile.Items[0].ProfileType of
                         p_circle : begin
                             FmPolyParam.rbCircle.Checked := true;
                             FmPolyParam.dceCircRad.NumValue := l^.profile.Items[0].radius;
                             FmPolyParam.rgPortion.ItemIndex := ord(l^.profile.Items[0].cportion);
                           end;
                         p_polygon : begin
                             FmPolyParam.rbPolygon.Checked := true;
                             FmPolyParam.seNumSides.Value := l^.profile.Items[0].numsides;
                             FmPolyParam.dcePlyRadius.NumValue := l^.profile.Items[0].plyradius;
                             FmPolyParam.rgPortion.ItemIndex := ord(l^.profile.Items[0].pportion);
                           end;
                         p_rectangle : begin
                             FmPolyParam.rbRectangle.Checked := true;
                             FmPolyParam.dceWidth.NumValue := l^.profile.Items[0].width;
                             FmPolyParam.dceHeight.NumValue := l^.profile.Items[0].height;
                             FmPolyParam.rgPortion.ItemIndex := ord(l^.profile.Items[0].rportion);
                           end;
                       end;

                       if FmPolyParam.ShowModal = mrOK then begin
                         MoveProfileToPrevious;
                         l^.profile.Modifier := [];
                         with FmPolyParam do
                           if rbCircle.Checked then begin
                             Profile.ProfileType := p_circle;
                             Profile.radius := dceCircRad.NumValue;
                             Profile.cPortion := tPortion (rgPortion.ItemIndex);
                           end
                           else if rbPolygon.Checked then begin
                             Profile.ProfileType := p_polygon;
                             Profile.plyradius := dcePlyRadius.NumValue;
                             Profile.numsides := seNumSides.Value;
                             Profile.pPortion := tPortion (rgPortion.ItemIndex);
                           end
                           else begin
                             Profile.ProfileType := p_rectangle;
                             Profile.width := dceWidth.NumValue;
                             Profile.height := dceHeight.NumValue;
                             Profile.rPortion := tPortion (rgPortion.ItemIndex);
                           end;
                         l^.profile.Items.Add(Profile);
                         l^.state := _Path;
                       end;
                     finally
                       SaveFormPos(TForm(FmPolyParam));
                       FmPolyParam.Free;
                       if l^.ProfileFormCreated then begin
                         ProfileToArray (false);
                         l^.FmProfile.DrawProfile;
                       end;
                     end;
                   end;
              f0 : if l^.lastprofile.Items.Count > 0 then begin
                     l^.tempprofiles := l^.profile;
                     l^.profile := l^.lastprofile;
                     l^.lastprofile := l^.tempprofiles;
                     l^.state := _Path;
                     KeepTwistEnlargePrompt;
                     if l^.ProfileFormCreated then begin
                       l^.FmProfile.plnWarningGiven := true;
                       ProfileToArray (false);
                       l^.FmProfile.DrawProfile;
                     end;
                   end;

              s6 : l^.state := _StartRotPfl;

              s8 : GetAdvancedOptions;
              s9 : ShowHelp;
              s0 : begin
                     if l^.profile.Items.Count > 0 then
                       l^.state := _path
                     else begin
                       l^.state := _Exiting;
                       PrepareToExit;
                     end;
                   end;
              else sysUtils.Beep;
            end;
            SaveInt(dhExtProfSel, ord(l^.pflsel));
          end
          else begin
            if l^.pflsel in [s_entity, s_group] then begin
              {$region 'Entity, Group processing'}
              mode_init1 (mode);
              mode_enttype (mode, entlin);
              mode_enttype (mode, entarc);
              mode_enttype (mode, entcrc);
              mode_enttype (mode, entell);
              mode_enttype (mode, entbez);
              mode_enttype (mode, entbsp);
              mode_enttype (mode, entpln);
              mode_enttype (mode, entply);
              if ent_near (ent, l^.getp.curs.x, l^.getp.curs.y, mode, false) then begin
                TempSaveCurrentProfile;
                MoveProfileToPrevious;
                if l^.pflsel = s_entity then begin
                  ent_get (ent, ent.addr);
                  profile.ProfileType := p_entity;
                  profile.adr := ent.Addr;
                  Hilite (ent, clrltgray);
                  l^.profile.Items.Add(profile);
                end
                else begin
                  mode_init1 (mode);
                  mode_group (mode, ent);
                  adr := ent_first (mode);
                  while ent_get (ent, adr) do begin
                    adr := ent_next (ent, mode);
                    if ent.enttype in [entlin, entln3, entarc, entcrc, entell, entbez, entbsp, entcnt, entpln, entply] then begin
                      profile.ProfileType := p_entity;
                      profile.adr := ent.Addr;
                      Hilite (ent, clrltgray);
                      l^.profile.Items.Add(profile);
                    end;
                  end;
                end;
                l^.state := _ProfileRef;
              end
              else begin
                wrterr ('No suitable entity found within miss distance');
                sysUtils.Beep;
              end;
              {$endregion}
            end
            else begin
              l^.pnt := l^.getp.curs;
              l^.state := _Profile2
            end;
          end;
        {$endregion '_Profile'}
        {$region '_Profile2'}
        _Profile2 :
            if l^.getp.result = res_escape then begin
              if l^.getp.key = s0 then
                l^.state := _Profile
              else
                sysUtils.Beep;
            end
            else begin
              TempSaveCurrentProfile;
              MoveProfileToPrevious;
              mode_init1 (mode);
              mode_box (mode, l^.pnt.x, l^.pnt.y, l^.getp.curs.x, l^.getp.curs.y);
              adr := ent_first (mode);
              (*len := l^.profile.Items.Count;*)
              while ent_get (ent, adr) do begin
                adr := ent_next (ent, mode);
                if ent.enttype in [entlin, entln3, entarc, entcrc, entell, entbez, entbsp, entpln, entply] then begin
                  profile.ProfileType := p_entity;
                  profile.adr := ent.Addr;
                  l^.profile.Items.Add(profile);
                  Hilite (ent, clrltgray);
                end;
              end;
              (*if l^.profile.Items.Count > len then begin
                for i := 1 to len do
                  l^.profile.Items.Delete(0);
                l^.state := _ProfileRef;
              end
              else begin *)
              if l^.profile.Items.Count > 0 then begin
                l^.state := _ProfileRef;
              end
              else begin
                RestoreFromTempProfile;
                wrterr ('No suitable entities found inside defined area');
                MessageBeep(MB_ICONERROR);
              end;
            end;
        {$endregion '_Profile2'}
        {$region '_ProfileRef'}
        _ProfileRef :
            begin
              if (l^.getp.result = res_escape) then begin
                case l^.getp.key of
                  f0 : begin
                        refpnt := PflMiddle(l^.profile);
                        l^.state := _Path;
                        KeepTwistEnlargePrompt;
                       end;
                  s0 : begin
                         RestoreFromTempProfile;
                         l^.state := _Profile;
                       end;
                  else sysUtils.Beep;
                end;
              end
              else if (l^.getp.result = res_normal) then begin
                refpnt := l^.getp.curs;
                l^.state := _Path;
                KeepTwistEnlargePrompt;
              end
              else
                sysUtils.Beep;
              if l^.state = _Path then begin   // state will be set to _Path if a point has been specified
                if l^.profile.Items.Count > 0 then begin
                  for i := 0 to l^.profile.Items.Count-1 do
                    if (l^.profile.Items[i].ProfileType = p_entity) and ent_get (ent, l^.profile.Items[i].adr) then
                      Hilite (ent, 0);
                end;
                l^.profile.refpnt := refpnt;
                l^.profile.Modifier := [];
                if l^.ProfileFormCreated then begin
                  l^.FmProfile.plnWarningGiven := false;
                  ProfileToArray (false);
                  l^.FmProfile.DrawProfile;
                end;
              end;
            end;
        {$endregion '_ProfileRef'}
        {$region '_Path'}
        _Path :
            if l^.getp.result = res_escape then begin
              {$region 'Process path keystrokes'}
              wrterr ('');
              case l^.getp.key of
                f1 : begin
                      l^.pathsel := s_entity;
                      SaveInt (dhExtPathSel, ord(s_entity), false);
                      setlength(l^.tracepnts, 0);
                     end;
                f2 : begin
                      l^.pathsel := s_group;
                      SaveInt (dhExtPathSel, ord(s_group), false);
                      setlength(l^.tracepnts, 0);
                     end;
                f3 : begin
                      l^.pathsel := s_area;
                      SaveInt (dhExtPathSel, ord(s_area), false);
                      setlength(l^.tracepnts, 0);
                     end;
                f4 : begin
                      l^.state := _Trace;
                      l^.traceClosed := NotClosed;
                     end;
                f6 : PGsavevar.srch := not PGsavevar.srch;
                f7 : begin
                      l^.ShowOptions := not l^.ShowOptions;
                      SaveBool(dhExtShwOptn, l^.ShowOptions, true);
                     end;
                f9 : l^.state := _Z;

                s1 : begin
                      l^.Cap := not l^.Cap;
                      SaveBool (dhExtCapEnd, l^.Cap, true);
                     end;
                s2 : begin
                      l^.VerticalEnds := not l^.VerticalEnds;
                      SaveBool (dhExtVertEnd, l^.VerticalEnds, true);
                     end;

                s3 : l^.state := _Enl;
                s4 : l^.state := _Twist;
                s5 : l^.state := _ExtrThick;

                s7 : l^.state := _Profile;
                s8 : GetAdvancedOptions;
                s9 : ShowHelp;
                s0 : begin
                      PrepareToExit;
                      l^.state := _Exiting;
                     end;
                f5,f8,f0,s6 : sysUtils.Beep;
                6054 : MessageDlg ('Exit the macro before selecting the Shader', mtInformation, [mbOK], 0);
                else begin
                  l^.prevstate := l^.state;
                  l^.state := _Esc;
                end;
              end;
              {$endregion 'Process path keystrokes'}
            end
            else begin
              if l^.pathsel in [s_entity, s_group] then begin
                {$region 'Entity, Group processing'}
                mode_init1 (mode);
                if ent_near (ent, l^.getp.curs.x, l^.getp.curs.y, mode, false) then begin
                  l^.pathEnts.Clear;
                  if (l^.pathsel = s_entity) then begin
                    if (ent.enttype in ValidPathEnts) then
                      l^.pathEnts.Add(ent)
                    else
                      wrterr ('Not a suitable type of entity');
                  end
                  else if l^.pathsel = s_group then begin
                    if ent_get (firstent, ent.addr) and (firstent.enttype in ValidPathEnts) then
                      l^.pathEnts.Add(firstent);  // ensure the entity actually selected is the first added to pathEnts
                    mode_init1 (mode);
                    mode_group (mode, ent);
                    adr := ent_first (mode);
                    while ent_get (ent, adr) do begin
                      if (ent.enttype in ValidPathEnts) and not addr_equal(ent.addr, firstent.addr) then
                        l^.pathEnts.Add(ent);
                      adr := ent_next (ent, mode);
                    end;
                  end;
                  ProcessPath;
                end
                else
                  WrtErr ('The point entered is too far from an entity');
                {$endregion 'Entity, Group processing'}
              end
              else begin //area
                l^.state := _Path2;
                l^.pnt := l^.getp.curs;
              end;
            end;
        {$endregion '_Path'}
        {$region '_Path2'}
        _Path2 :
            if l^.getp.result = res_escape then begin
              if l^.getp.key = s0 then
                l^.state := _path
              else
                sysUtils.Beep;
            end
            else begin
              mode_init1 (mode);
              mode_box (mode, l^.pnt.x, l^.pnt.y, l^.getp.curs.x, l^.getp.curs.y);
              adr := ent_first (mode);
              l^.pathEnts.Clear;
              while ent_get (ent, adr) do begin
                adr := ent_next (ent, mode);
                if ent.enttype in ValidPathEnts then begin
                  l^.pathEnts.Add(Ent);
                end;
              end;
              if l^.PathEnts.Count = 0 then begin
                wrterr ('No suitable entities found inside defined area');
                MessageBeep(MB_ICONERROR);
              end
              else
                ProcessPath;
            end;
        {$endregion '_Path2'}
        {$region '_Z'}
        _Z : begin
            if l^.getp.result = res_escape then begin
                case l^.getp.key of
                  f1 : begin
                         if hite in l^.zTreatment then
                           exclude (l^.zTreatment, hite)
                         else
                           include (l^.zTreatment, hite);
                         exclude (l^.zTreatment, slope);
                         exclude (l^.zTreatment, revslope);
                         exclude (l^.zTreatment, zProfile);
                         exclude (l^.zTreatment, BasePlus);
                         exclude (l^.zTreatment, zMid);
                         SaveZTreatment;
                       end;
                  f2 : begin
                         if base in l^.zTreatment then
                           exclude (l^.zTreatment, base)
                         else
                           include (l^.zTreatment, base);
                         exclude (l^.zTreatment, slope);
                         exclude (l^.zTreatment, revslope);
                         exclude (l^.zTreatment, zProfile);
                         exclude (l^.zTreatment, BasePlus);
                         exclude (l^.zTreatment, zMid);
                         SaveZTreatment;
                       end;
                  f3 : begin
                         if slope in l^.zTreatment then
                           l^.zTreatment := []
                         else
                           l^.zTreatment := [slope];
                         SaveZTreatment;
                       end;
                  f4 : begin
                         if revslope in l^.zTreatment then
                           l^.zTreatment := []
                         else
                           l^.zTreatment := [RevSlope];
                         SaveZTreatment;
                       end;
                  f5 : begin
                         if zMid in l^.zTreatment then
                           l^.zTreatment := []
                         else
                           l^.zTreatment := [zMid];
                         SaveZTreatment;
                       end;
                  f6 : begin
                         if BasePlus in l^.zTreatment then
                           l^.zTreatment := []
                         else begin
                           l^.zTreatment := [BasePlus];
                           l^.state := _zBasePlus;
                         end;
                         SaveZTreatment;
                       end;
{*                  f8 : begin
                         l^.state := _ZProfile;
                         SaveZTreatment;
                       end;          *}
                  f0 : begin
                         if verticals in l^.zTreatment then
                           exclude (l^.zTreatment, verticals)
                         else begin
                           include (l^.zTreatment, verticals);
                           exclude (l^.zTreatment, slope);
                           exclude (l^.zTreatment, revslope);
                           exclude (l^.zTreatment, zProfile);
                           exclude (l^.zTreatment, zMid);
                           exclude (l^.zTreatment, BasePlus);
                         end;
                         SaveZTreatment;
                       end;
                  s0 : begin
                         l^.state := _Path;
                         if l^.zTreatment = [] then begin
                           wrterr ('No Z Setting selected: Z-Base enabled by default');
                           SysUtils.Beep;
                           l^.zTreatment := [base];
                         end;
                       end
                  else sysUtils.Beep;
                  if (not (zProfile in l^.zTreatment)) and l^.ZProfFormCreated then
                    l^.fmZProfile.Visible := false;
                end;
              end
              else
                sysUtils.Beep;
            end;
        {$endregion '_Z'}
        _zBasePlus : begin
              l^.state := _Z;
              SaveRl (dhZBsPlusDis, l^.ZBasePlus, false);
            end;
        {$region '_ZProfile'}
        _ZProfile : begin
              if l^.getp.result = res_escape then begin
                if l^.getp.key = s0 then
                  l^.state := _Z
                else
                  sysUtils.Beep;
              end
              else begin
                mode_init1 (mode);
                mode_enttype (mode, entpln);
                if ent_near (ent, l^.getp.curs.x, l^.getp.curs.y, mode, false) and (ent.enttype = entpln) then begin
                  if CheckPolyline (ent, 'Z') then begin
                    l^.state := _Z;
                    wrtmsg ('');
                    l^.zTreatment := [zProfile];
                    l^.zProfile := ent.addr;
                    // ensure any existing zProfile attributes are cleared
                    MyModeInit (mode);
                    mode_atr (mode, dhExtZProfl);
                    adr := ent_first (mode);
                    while ent_get (ent, adr) do begin
                      adr := ent_next (ent, mode);
                      if atr_entfind (ent, dhExtZProfl, atr) then
                        atr_delent (ent, atr);
                    end;
                    // add attribute to zProfile entity;
                    if ent_get (ent, l^.zProfile) then begin
                      atr_init (atr, atr_int);
                      atr.name := dhExtZProfl;
                      atr.int := 0;
                      atr_add2ent (ent, atr);
                    end;
                    if l^.WindowFlags[_WindowZProfile] <> _DoNotShow then begin
                      // display Z Profile form
                      if not l^.ZProfFormCreated then begin
                        l^.fmZProfile := TfmZProfile.Create(nil);
                        l^.fmZProfile.SetZProfile(@l^.zProfile);
                        l^.ZProfFormCreated := true;
                        SetFormPos (TForm(l^.fmZProfile));
                      end;
                      l^.fmZProfile.ShowZProfile;
                      l^.fmZProfile.Show;
                    end;
                  end;
                end
                else begin
                  wrterr ('No polyline found within miss distance');
                  sysUtils.Beep;
                end
              end;

            end;
        {$endregion '_ZProfile'}
        {$region '_PathDivs'}
(*        _PathDivs : begin
              if l^.pathdivs < 6 then
                l^.pathdivs := 6
              else if l^.pathdivs > 128 then
                l^.pathdivs := 128;
              SaveInt (dhExtPthDivs, l^.pathdivs, false);
              l^.state := _Path;
            end;                *)
        {$endregion '_PathDivs'}
        {$region '_Enl'}
        _Enl : begin
              if l^.getp.result = res_escape then begin
                case l^.getp.key of
                  f1 : l^.state := _EnlStart;
                  f2 : l^.state := _EnlEnd;
                  f3 : l^.state := _EnlBoth;
                  f5 : l^.state := _EnlProfile;
                  f7, f8 : ToggleEnlXY (l^.getp.key);
                  f0 : begin
                        l^.StartEnl := 1.0;
                        l^.EndEnl := 1.0;
                        l^.EnlXY := EnlBoth;
                        SaveRl(dhExtEnlEnd, l^.EndEnl, false);
                        SaveRl(dhExtEnlStrt, l^.StartEnl, false);
                        SaveInt (dhExtEnlXY, ord(l^.EnlXY), false);
                        if not isnil (l^.EnlProfile) then begin
                          if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then
                            atr_delent (ent, atr);
                          setnil (l^.EnlProfile);
                        end;
                        if l^.EnlProfFormCreated then
                          l^.fmEnlargeProfile.Hide;
                       end;
                  s9 : ShowHelp;
                  s0 : begin
                        if (l^.EnlXY = EnlNone) and
                           ((not RealEqual (l^.StartEnl, one)) or
                            (not RealEqual (l^.EndEnl, one)) or
                            (not isnil (l^.EnlProfile))) then begin

                          if MessageDlg(ENL_WARN, TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0)= mrYes then
                            l^.state := _path;;
                        end
                        else
                          l^.state := _path;
                       end;
                  f4,f6, f9, s1..s8 : SysUtils.Beep;
                  6054 : MessageDlg ('Exit the macro before selecting the Shader', mtInformation, [mbOK], 0);
                  else begin
                    l^.prevstate := l^.state;
                    l^.state := _Esc;
                  end;
                end;
              end
              else begin
                SysUtils.Beep;
              end;

            end;
        {$endregion '_Enl'}
        {$region '_EnlStart, _EnlEnd'}
        _EnlStart, _EnlEnd, _EnlBoth : begin
              if l^.getr.result = res_normal then begin
                if (l^.StartEnl < 0) or (l^.EndEnl < 0) then begin
                  sysUtils.Beep;
                  wrterr ('Enlargement may not be less than zero');
                  l^.StartEnl := GetSvdRl(dhExtEnlStrt, 1.0);
                  l^.EndEnl := GetSvdRl(dhExtEnlEnd, 1.0);
               end
                else if (l^.StartEnl = 0) and ((l^.EndEnl = 0) or (l^.state = _EnlBoth)) then begin
                  sysUtils.Beep;
                  wrterr ('Beginning and ending enlargement cannot both be zero');
                  l^.StartEnl := GetSvdRl(dhExtEnlStrt, 1.0);
                  l^.EndEnl := GetSvdRl(dhExtEnlEnd, 1.0);
                end
                else begin
                  if l^.state = _EnlBoth then
                    l^.EndEnl := l^.StartEnl;
                  SaveRl(dhExtEnlEnd, l^.EndEnl, false);
                  SaveRl(dhExtEnlStrt, l^.StartEnl, false);
                  l^.state := _Enl;
                  if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then
                    atr_delent (ent, atr);
                  setnil (l^.EnlProfile);
                  if l^.EnlProfFormCreated then
                    l^.fmEnlargeProfile.Visible := false;
                end;
              end
              else begin
                d := 0;
                case l^.getr.key of
                  f3 : d := 0.1;
                  f4 : d := 0.25;
                  f5 : d := 0.5;
                  f6 : d := 0.75;
                  f7 : d := 1.0;
                  f8 : d := 2.0;
                  f9 : d := 3.0;
                  f0 : d := 4.0;
                  s1 : d := 5.0;
                  s2 : d := 6.0;
                  s3 : d := 7.0;
                  s4 : d := 8.0;
                  s5 : d := 9.0;
                  s6 : d := 10.0;
                  s0 : l^.state := _Enl;
                  else sysUtils.Beep;
                end;
                if (d <> 1) and not isnil (l^.EnlProfile) then begin
                  if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then
                    atr_delent (ent, atr);
                  setnil (l^.EnlProfile);
                end;
                if d > 0 then begin
                  if l^.state = _EnlStart then begin
                    l^.StartEnl := d;
                    SaveRl(dhExtEnlStrt, d, false);
                  end
                  else begin
                    l^.EndEnl := d;
                    SaveRl(dhExtEnlEnd, d, false);
                  end;
                end;
              end;
            end;
        {$endregion '_EnlStart, _EnlEnd'}
        {$region '_EnlProfile'}
        _EnlProfile : begin
              if l^.getp.result = res_escape then begin
                case l^.getp.key of
                  s0 : begin
                        l^.state := _Enl;
                        EnlPflRetainedMsg;
                       end;
                  F7, F8 : ToggleEnlXY (l^.getp.key)
                  else sysUtils.Beep;
                end;
              end
              else begin
                mymodeinit (mode);
                if PGsavevar.srch then
                  mode_lyr (mode, lyr_on)
                else
                  mode_lyr (mode, lyr_curr);
                mode_enttype (mode, entpln);
                if ent_near (ent, l^.getp.curs.x, l^.getp.curs.y, mode, false) and
                   CheckPolyline (ent, 'E', true) then begin

                  {remove attrib from old profile entity}
                  if ent_get(tempent, l^.EnlProfile) and atr_entfind (tempent, dhExtEnlProf, atr) then begin
                    if (atr.atrtype = atr_rl) then begin
                      l^.PrevEnlProfile := l^.EnlProfile;
                      l^.PrevEnlProfileRef := atr.rl;
                    end
                    else setnil (l^.PrevEnlProfile);
                    atr_delent (tempent, atr);
                  end
                  else
                    setnil (l^.PrevEnlProfile);

                  {set up new profile entity}
                  l^.EnlProfile := ent.addr;
                  ent_get (ent, l^.EnlProfile);  // required as previous ent_get of tempent can cause ent to be invalid
                  atr_init (atr, atr_rl);
                  atr.name := dhExtEnlProf;
                  ent_extent (ent,minpnt, tmppnt);
                  atr.rl := (minpnt.y + tmppnt.y)/2;
                  atr_add2ent (ent, atr);
                  l^.state := _EnlProfileRef;
                  Hilite(ent, clrltgray);
                  l^.EndEnl := 1.0;
                  l^.StartEnl := 1.0;
                  SaveRl(dhExtEnlEnd, l^.EndEnl, false);
                  SaveRl(dhExtEnlStrt, l^.StartEnl, false);
                  l^.EnlRptDis := abs(tmppnt.x - minpnt.x);
                end;
              end;
            end;
        {$endregion '_EnlProfile'}
        {$region '_EnlProfileRef'}
        _EnlProfileRef : begin
              if l^.getp.result = res_escape then begin
                case l^.getp.key of
                  s0 : begin
                        l^.state := _Enl;
                        if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then begin
                          atr_delent (ent, atr);
                          Hilite(ent, 0);
                        end;
                        if ent_get (ent, l^.PrevEnlProfile) then begin
                          atr_init (atr, atr_rl);
                          atr.name := dhExtEnlProf;
                          atr.rl := l^.PrevEnlProfileRef;
                          atr_add2ent (ent, atr);
                          l^.EnlProfile := l^.PrevEnlProfile;
                          EnlPflRetainedMsg;
                          if l^.EnlProfFormCreated then
                            l^.fmEnlargeProfile.DrawProfile;
                        end
                        else
                          setnil (l^.EnlProfile);
                       end;
                  f1, f2, f3 : if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then begin
                                 ent_extent (ent,minpnt, tmppnt);
                                 case l^.getp.key of
                                    f1 : atr.rl := minpnt.y;
                                    f2 : atr.rl := (minpnt.y + tmppnt.y)/2;
                                    f3 : atr.rl := tmppnt.y;
                                 end;
                                 atr_update (atr);
                                 ShowEnlargementProfile;
                                 Hilite(ent, 0);
                                 l^.state := _Enl;
                               end;
                  F7, F8 : ToggleEnlXY (l^.getp.key)
                  else sysUtils.Beep;
                end;
              end
              else begin
                if l^.getp.curs.y < 0.01 then begin
                  wrterr ('Y value of selected point must be greater than zero');
                  sysUtils.Beep;
                end
                else begin
                  if ent_get (ent, l^.EnlProfile) and atr_entfind (ent, dhExtEnlProf, atr) then begin
                    atr.rl := l^.getp.curs.y;
                    atr_update (atr);
                    Hilite(ent, 0);
                    EntExtent(ent, temparr[1], temparr[2]);
                    l^.EnlRptDis := abs (temparr[2].x - temparr[1].x);
                    SaveRl(dhEnlPRptDis, l^.EnlRptDis, false);
                    ShowEnlargementProfile;
                  end;
                  l^.state := _Enl;
                end;
              end;
            end;
        {$endregion '_EnlProfileRef'}
        {$region '_ExtrThick'}
        _ExtrThick : begin
              l^.state := _Path (*_Profile*);
              SaveRl (dhExtShelTk, l^.ExtrThick, false);
              wrtmsg ('');
              if l^.ProfileFormCreated then  begin
                ProfileToArray (false);
                l^.FmProfile.DrawProfile;
              end;
            end;
        {$endregion '_ExtrThick'}
        {$region '_Twist'}
        _Twist : begin
              if l^.getp.result = res_escape then begin
                 case l^.getp.key of
                  s6 : l^.state := _StartRot;
                  f1 : l^.twisttype := NoTwist;
                  f5,f6 : l^.ClkwiseRot := (l^.getp.key = f5);
                  f3 : begin
                        l^.TwistType := NumTwist;
                        l^.state := _TwistNum;
                       end;
                  f4 : begin
                        l^.TwistType := DistTwist;
                        l^.state := _TwistDis;
                       end;
                  f7 : l^.state := _TwistDivs;
                  s9 : ShowHelp;
                  s0 : l^.state := _path;
                 end;
              end
              else
                  sysUtils.Beep;
            end;
        {$endregion '_Twist'}

        _StartRot, _StartRotPfl : begin
              if not RealEqual(l^.StartRotation, l^.geta.pang^) then  begin
                l^.StartRotation := l^.geta.pang^;
                if l^.ProfileFormCreated then begin
                  ProfileToArray (false);
                  l^.FmProfile.DrawProfile;
                end;
                SaveRl (dhExtStrtRot, l^.StartRotation, false);
              end;
              if l^.state = _StartRotPfl then
                l^.state := _Profile
              else
                l^.state := _Twist;
            end;

        {$region '_TwistNum'}
        _TwistNum : begin
              if (l^.getr.result = res_normal) or (l^.getr.key = s0) then begin
                l^.state := _Twist;
                if RealEqual (l^.TwistNumTurns, 0) then
                  l^.TwistType := NoTwist
                else begin
                  l^.TwistNumTurns := abs (l^.TwistNumTurns);
                  if l^.TwistNumTurns > 100 then begin
                      if MessageDlg(FloatToStr(l^.TwistNumTurns) + ' is a lot of turns.' + sLineBreak +
                                 'Are you sure this is correct?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
                      l^.state := _TwistNum;
                  end;
                end;
              end
              else begin
                l^.state := _Twist;
                case l^.getr.key of
                  f1 : l^.TwistNumTurns := 0.25;
                  f2 : l^.TwistNumTurns := 0.5;
                  f3 : l^.TwistNumTurns := 1;
                  f4 : l^.TwistNumTurns := 2;
                  f5 : l^.TwistNumTurns := 3;
                  f6 : l^.TwistNumTurns := 4;
                  f7 : l^.TwistNumTurns := 5;
                  f8 : l^.TwistNumTurns := 6;
                  f9 : l^.TwistNumTurns := 7;
                  f0 : l^.TwistNumTurns := 8;
                  else begin
                          sysutils.Beep;
                          l^.state := _TwistNum;
                       end;
                end;
              end;

              SaveInt (dhExtTwistTy, ord(l^.TwistType));
              SaveRl (dhExtTwistNu, l^.TwistNumTurns, false);
            end;
        {$endregion '_TwistNum'}
        {$region '_TwistDis'}
        _TwistDis : begin
              if RealEqual (l^.TwistDistance, 0) then
                l^.TwistType := NoTwist;
              l^.TwistDistance := abs (l^.TwistDistance);
              l^.state := _Twist;
              SaveInt (dhExtTwistTy, ord(l^.TwistType));
              SaveRl (dhExtTwistDi, l^.TwistDistance, false);
            end;
        {$endregion '_TwistDis'}
        {$region '_TwistDivs'}
        _TwistDivs : begin
              if l^.Divs.twist < 6 then begin
                l^.Divs.Twist := 6;
                wrterr ('Divisions changed to minimum value (6)');
              end
              else if l^.Divs.Twist > 128 then begin
                l^.Divs.Twist := 128;
                wrterr ('Divisions changed to maximum value (128)');
              end;
              SaveInt (dhExtTwistDv, l^.Divs.Twist, true);
              l^.state := _Twist;
            end;
        {$endregion '_TwistDivs'}
        {$region '_Trace'}
        _Trace :
            if l^.getp.result = res_escape then begin
              wrterr ('');
              case l^.getp.key of
                f1 : l^.traceZtype := trBase;
                f2 : l^.traceZtype := trUser1;
                f3 : l^.traceZtype := trUser2;
                f4 : l^.traceZtype := trHite;
                f5 : l^.state := _TraceZ;
                f7 : if l^.OffsetByPoint then
                      l^.state := _OffsetDis
                     else
                      l^.state := _KeyOffsDis;
                f8 : begin
                      DrawTracePath (drmode_black);
                      l^.Fillet := not l^.Fillet;
                      DrawTracePath (drmode_white);
                      if l^.Fillet then
                        l^.state := _FilletRad;
                     end;
                f0 : begin
                      DrawTracePath(drmode_black);
                      if l^.traceClosed = ClosedByOption then
                        l^.traceClosed := NotClosed
                      else if l^.traceClosed = ClosedByPoints then
                        l^.traceClosed := ForceNotClosed
                      else
                        l^.traceClosed := ClosedByOption;
                      OffsetTracePnts;
                      DrawTracePath (drmode_white);
                     end;
                s4 : l^.state := _User1;
                s5 : l^.state := _User2;
                s7 : if length(l^.tracepnts) > 1 then begin
                      DrawTracePath (drmode_black);
                      setlength (l^.tracepnts, length(l^.tracepnts)-1);
                      if l^.TraceClosed = ClosedByPoints then
                        l^.TraceClosed := NotClosed;
                      OffsetTracePnts;
                      DrawTracePath (drmode_white);
                     end
                     else
                      sysUtils.beep;
                s8 : if length(l^.tracepnts) > 1 then begin
                      DrawTracePath(drmode_black);
                      setlength(l^.tracepnts, 0);
                      l^.state := _path;
                     end
                     else
                      sysUtils.beep;

                s0 : begin
                      if length (l^.tracepnts) > 1 then begin
                        if (length(l^.tracepnts) > 2) and
                           PointsEqual (l^.tracepnts[0].pnt, l^.tracepnts[length(l^.tracepnts)-1].pnt) and
                           (l^.traceClosed = NotClosed) then begin
                          l^.traceClosed := ClosedByPoints;
                        end;
                        if (not (l^.traceClosed in [NotClosed, ForceNotClosed])) and (length(l^.tracepnts) > 2) then begin
                          OffsetTracePnts;
                          setlength (l^.OffsTracePnts, length(l^.OffsTracePnts)+1);
                          l^.OffsTracePnts [length(l^.OffsTracePnts)-1] := l^.OffsTracePnts [0]
                        end;

                        DrawTracePath (drmode_black, drmode_white);

                        ProcessPath;
                      end;
                      l^.state := _Path;
                     end;
                F6, F9, S1, S2, S3, S6, S9 : sysUtils.beep;
                else begin
                      l^.prevstate := l^.state;
                      l^.state := _Esc;
                     end;
              end;
            end
            else begin
              DrawTracePath (drmode_black);
              if (l^.TraceClosed = ClosedByPoints) and
                 not PointsEqual (NewPnt2D(l^.getp.curs),
                                  l^.tracepnts[length(l^.tracepnts)-1].pnt) then
                l^.TraceClosed := NotClosed;
              setlength (l^.tracepnts, length(l^.tracepnts)+1);
              l^.tracepnts[length(l^.tracepnts)-1].pnt := NewPnt2D(l^.getp.curs);
              l^.tracepnts[length(l^.tracepnts)-1].ofs := l^.offset;


              //draw trace path
              OffsetTracePnts;
              DrawTracePath (drmode_white);
            end;
        {$endregion '_Trace'}
        {$region '_OffsetDis'}
        _OffsetDis : begin
              wrterr ('');
              wrtmsg ('');
              if l^.getp.result = res_escape then begin
                wrterr ('');
                case l^.getp.key of
                  f1 : l^.OffsTraceAll := not l^.OffsTraceAll;
                  f3 : l^.state := _KeyOffsDis;
                  f8 : begin
                        DrawTracePath (drmode_black);
                        l^.Offset := zero;
                        if l^.OffsTraceAll then for i := 0 to length(l^.TracePnts)-1 do
                          l^.TracePnts[i].ofs := l^.Offset
                        else
                          l^.TracePnts[length(l^.TracePnts)-1].ofs := l^.Offset;
                        OffsetTracePnts;
                        DrawTracePath (drmode_white);
                        l^.state := _trace;
                       end;
                  s0 : l^.state := _trace;
                  else beep;
                end;
              end
              else begin
                l^.OffsetByPoint := true;
                SaveBool (dhExtOfByPnt, true);

                // undraw existing traced path
                DrawTracePath (drmode_black);

                // get new offset distance
                l^.Offset := sqrt (dis_from_line (NewPoint(l^.tracepnts[length(l^.tracepnts)-2].pnt),
                                                  NewPoint(l^.Tracepnts[length(l^.tracepnts)-1].pnt),
                                                  l^.getp.curs));
                d := crossz (NewPoint(l^.tracepnts[length(l^.tracepnts)-2].pnt),
                             NewPoint(l^.Tracepnts[length(l^.tracepnts)-1].pnt),
                             l^.getp.curs);
                if realequal (d, zero) then begin
                  l^.offset := zero;
                  wrterr ('Offset cancelled (set to zero)');
                  l^.tracepnts[length(l^.tracepnts)-2].ofs := zero;
                  l^.state := _trace;
                end
                else begin
                  cvdisst (l^.offset, tempstr1);
                  if d < 0 then begin
                    l^.offset := -l^.offset;
                    tempstr1 := tempstr1 + ' to left'
                  end
                  else begin
                    tempstr1 := tempstr1 + ' to right';
                  end;
                  wrterr ('Offsetting ' + tempstr1);
                  l^.tracepnts[length(l^.tracepnts)-2].ofs := l^.offset;
                  l^.state := _trace;
                end;
                l^.tracepnts[length(l^.tracepnts)-1].ofs := l^.tracepnts[length(l^.tracepnts)-2].ofs;
                if l^.OffsTraceAll then for i := 0 to length(l^.TracePnts)-1 do
                  l^.TracePnts[i].ofs := l^.Offset;

                //draw new trace path
                OffsetTracePnts;
                DrawTracePath (drmode_white);
              end;
            end;
        {$endregion '_OffsetDis'}
        {$region '_KeyOffsDis'}
        _KeyOffsDis :
            if l^.dgetd.result = res_escape then begin
              case l^.dgetd.key of
                f1 : l^.OffsTraceAll := not l^.OffsTraceAll;
                f3 : l^.state := _OffsetDis;
                f8 : begin
                      DrawTracePath (drmode_black);
                      l^.Offset := zero;
                      if l^.OffsTraceAll then for i := 0 to length(l^.TracePnts)-1 do
                        l^.TracePnts[i].ofs := l^.Offset
                      else
                        l^.TracePnts[length(l^.TracePnts)-1].ofs := l^.Offset;
                      OffsetTracePnts;
                      DrawTracePath (drmode_white);
                      l^.state := _trace;
                     end;
                s0 : l^.state := _trace;
                else beep;
              end;
            end
            else begin
              SaveBool (dhExtOfByPnt, false);
              l^.OffsetByPoint := false;
              DrawTracePath(drmode_black);
              if realequal (l^.TempRl, zero) then begin
                l^.offset := zero;
                wrterr ('Offset cancelled (set to zero)');
                if length(l^.tracepnts) = 2 then
                  l^.tracepnts[length(l^.tracepnts)-2].ofs := zero;
                l^.tracepnts[length(l^.tracepnts)-1].ofs := zero;
                l^.state := _trace;
              end
              else begin
                l^.offset := l^.TempRl;
                cvdisst (abs(l^.offset), tempstr1);
                if l^.offset < 0 then begin
                  tempstr1 := tempstr1 + ' to left'
                end
                else begin
                  tempstr1 := tempstr1 + ' to right';
                end;
                wrterr ('Offsetting ' + tempstr1);
                if l^.OffsTraceAll then for i := 0 to length(l^.TracePnts)-1 do
                  l^.TracePnts[i].ofs := l^.Offset
                else begin
                  if length(l^.tracepnts) = 2 then
                    l^.tracepnts[length(l^.tracepnts)-2].ofs := l^.offset;
                  l^.tracepnts[length(l^.tracepnts)-1].ofs := l^.offset;
                end;
                l^.state := _trace;
              end;
              OffsetTracePnts;
              DrawTracePath(drmode_white);
            end;
        {$endregion '_KeyOffsDis'}
        {$region '_FilletRad'}
        _FilletRad : begin
              DrawTracePath (drmode_black);
              l^.FilletRad := l^.TempRl;
              SaveRl (dhExtFltRad, l^.TempRl, false);
              OffsetTracePnts;
              DrawTracePath (drmode_white);
              l^.state := _Trace;
              wrtmsg ('');
            end;
        {$endregion '_FilletRad'}
        {$region '_User1, User2'}
        _User1, _User2 : begin
              l^.state := _Trace;
              wrtmsg ('');
            end;
        {$endregion '_User1, User2'}
        {$region '_TraceZ'}
        _TraceZ : begin
            l^.state := _Trace;
            SaveRl (dhExtTraceZ, l^.traceZ, false);
            l^.traceZtype := trSpecified;
            wrtmsg ('');
          end;
        {$endregion '_TraceZ'}
        _Esc : begin
            l^.state := l^.prevstate;
            if l^.state = _trace then
              DrawTracePath (drmode_white);
          end
        else begin
              PrepareToExit;
              l^.state := _Exiting;
            end;
     end;
  end;
  if act = afirst then begin
    l^.state := _Profile;

    if not InitSettings('Extrude') then begin
      result := XDone;
      exit;
    end;

    s := GetStrFromMainIni(dhExtLicVer+ver.Substring(15), 'N');
    if s <> 'Y' then begin
      fmLicence := TfmLicence.Create(nil);
      try
        fmLicence.ShowModal;
        if fmLicence.ModalResult = mrOK then
          SaveStrToMainIni (dhExtLicVer+ver.Substring(15), 'Y')
        else
          l^.state := _Exiting;
      finally
        fmLicence.Free;
      end;
    end;
    if l^.state <> _Exiting then begin
      wrterr(ver + Copyright, true);

      l^.dcHandle := GetForegroundWindow;    // get DataCADs handle so it can be moved back to the top when file dialogs exit.

      l^.WindowFlags[_WindowProfile] := byte (GetSvdInt(dhExtSPflWdw, _ToTopWithMacroAction));
      l^.WindowFlags[_WindowEnlarge] := byte (GetSvdInt(dhExtEPflWdw, _ToTopWithMacroAction));

      l^.Divs.PflEqDivs := GetSvdBool (dhExtPflEqDv, true);
      l^.divs.Pflcrc := GetSvdInt(dhExtPflDivs, 36);
      l^.Divs.PflBez := GetSvdInt(dhExtPflBDiv, 1);

      l^.Divs.Crc := GetSvdInt(dhExtCrcDivs, 36);
      l^.Divs.Bez := GetSvdInt(dhExtBezDivs, 1);
      l^.Divs.Ent3D := GetSvdInt(dhExt3DDivs, 0);
      l^.Divs.PthEqDivs := GetSvdBool (dhExtEqDivs, l^.Divs.Ent3D=0);

      l^.Divs.EnlEqDivs := GetSvdBool (dhExtEnlEqDv, true);
      l^.Divs.EnlDivs := GetSvdInt (dhExtEnlDivs, 36);

      i := GetSvdInt( dhExtProfSel, PGSaveVar.Select);
      if (i > ord(s_area)) or (i < ord(s_group)) then
        l^.pflsel := s_entity
      else
        l^.pflsel := tSelectnType (i);
      i := GetSvdInt( dhExtPathSel, PGSaveVar.Select);
      if (i > ord(s_area)) or (i < ord(s_group)) then
        l^.pathsel := s_entity
      else
        l^.pathsel := tSelectnType (i);
      getpath (l^.sympath, pathsym);
      l^.sympath := symstr (getsvdStr (dhExtSymPath, string(l^.sympath)));
      l^.ShowOptions := GetSvdBool(dhExtShwOptn, true);
      l^.Cap := GetSvdBool (dhExtCapEnd, false);
      l^.VerticalEnds := GetSvdBool (dhExtVertEnd, false);
      l^.StartEnl := GetSvdRl(dhExtEnlStrt, 1.0);
      l^.EndEnl := GetSvdRl(dhExtEnlEnd, 1.0);
      l^.EnlXY := tEnlXY (getsvdint (dhExtEnlXY, ord(enlboth)));
      l^.TwistType := tTwistType(GetSvdInt (dhExtTwistTy, ord(NoTwist)));
      l^.TwistNumTurns := GetSvdRl (dhExtTwistNu, one);
      l^.TwistDistance := GetSvdRl (dhExtTwistDi, 0);
      l^.Divs.Twist := GetSvdInt (dhExtTwistDv, 36);

      if (l^.TwistType = NumTwist) and RealEqual(l^.TwistNumTurns, 0) then
        l^.TwistType := NoTwist
      else if (l^.TwistType = DistTwist) and RealEqual(l^.TwistDistance, 0) then
        l^.TwistType := NoTwist;

      tempstr1 := str255(GetSvdStr (dhExtZTrtmt, 'B       ', false));
      if length(tempstr1) < 8 then
        tempstr1 := tempstr1+'        ';
      l^.zTreatment := [];
      if tempstr1[1] = 'B' then
        include (l^.zTreatment, Base);
      if tempstr1[2] = 'H' then
        include (l^.zTreatment, Hite);
      if tempstr1[3] = 'S' then
        l^.zTreatment := [Slope];
      if tempstr1[4] = 'R' then
        l^.zTreatment := [RevSlope];

      if tempstr1[5] = 'P' then begin
        l^.zTreatment := [zProfile];
        MyModeInit (mode);
        mode_atr (mode, dhExtZProfl);
        adr := ent_first (mode);
        if ent_get (ent, adr) and (ent.enttype = entply) then
          l^.zProfile := adr
        else
          l^.zTreatment := [];
      end;

      if tempstr1[6] = 'V' then
        include (l^.zTreatment, verticals);
      if tempstr1[7] = 'M' then
        include (l^.zTreatment, zMid);
      if tempstr1[8] = '+' then
        include (l^.zTreatment, BasePlus);
      if l^.zTreatment = [] then
        l^.zTreatment := [Base];

      l^.ZBasePlus := GetSvdRl (dhZBsPlusDis, zero);

      l^.ExtrThick := GetSvdRl(dhExtShelTk, 0);

      l^.Divs.PflCrc := GetSvdInt(dhExtPflDivs, pGSv^.circldiv1);
      if (l^.Divs.PflCrc < 6) or (l^.Divs.PflCrc > 128) then
        l^.Divs.PflCrc := pGSv^.circldiv1;

      l^.EnlProfFormCreated := false;
      l^.EnlRepeat := GetSvdBool(dhExtEnlRpt, false);
      l^.PrevEnlRpt := l^.EnlRepeat;
      MyModeInit(mode);
      mode_lyr (mode, lyr_all);
      mode_atr (mode, dhExtEnlProf);
      if ent_get (ent, ent_first (mode)) and CheckPolyline (ent, 'E', false) then begin
        if atr_entfind (ent, dhExtEnlProf, atr) then begin
          if (atr.atrtype = atr_rl) and (atr.rl > 0) then begin
            l^.EnlProfile := ent.addr;
            EntExtent(ent, temparr[1], temparr[2]);
            l^.EnlRptDis := GetSvdRl(dhEnlPRptDis, abs(temparr[2].x - temparr[1].x));
            ShowEnlargementProfile;
          end
          else begin
            atr_delent (ent, atr);
            setnil (l^.EnlProfile);
          end;
        end
        else
          setnil (l^.EnlProfile);
      end
      else
        setnil (l^.EnlProfile);

      l^.ClkwiseRot := GetSvdBool (dhExtClkRot, true);
      l^.StartRotation := GetSvdRl(dhExtStrtRot, zero);

      l^.traceZ := GetSvdRl (dhExtTraceZ, zero);


      l^.initialised := true;
      l^.profile.Items := tprofileItems.Create;
      l^.lastprofile.Items := tprofileItems.Create;
      l^.pathEnts := tEntities.Create;

      l^.ProfileArrays := TList<tProfileArray>.Create;
      l^.ProfileArraysCreated := true;
      if l^.WindowFlags[_WindowProfile] <> _DoNotShow then begin
        l^.FmProfile := TFmProfile.Create(nil);
        l^.FmProfile.SetProfileData(@l^.Profile, @l^.ExtrThick, @l^.sympath, @l^.ProfileArrays, l^.dcHandle);
        l^.fmProfile.Visible := false;
        l^.ProfileFormCreated := true;
      end
      else
        l^.ProfileFormCreated := false;

      l^.ZProfFormCreated :=false;

      LoadPreviousProfile;

      l^.prevstate := _NoPrevState;

      l^.offset := zero;
      l^.OffsTraceAll := true;
      l^.fillet := false;
      l^.FilletRad := GetSvdRl(dhExtFltRad, zero);
      l^.OffsetByPoint := GetSvdBool(dhExtOfByPnt, false);
    end;

  end    //act = afirst
  else if act = alsize then begin
    SetLocalSize(sizeof(l^));
  end
  else if act = Alast then begin
    PrepareToExit;
    l^.state := _Exiting;
  end;
  if act <> alsize then begin
    ProcessFormVisibility;
    if  l^.EnlRepeat <> l^.PrevEnlRpt then
      SaveBool(dhExtEnlRpt, l^.EnlRepeat, false);

    case l^.state of
      {$region '_Profile'}
      _Profile : begin
          if l^.tempprofilesCreated then begin
            l^.tempprofiles.Items.Free;
            l^.tempprofilesCreated := false;
          end;
          wrtlvl  ('Section Profile');
          lblsinit;
          lblsett (1, 'Entity', l^.pflsel=s_entity, 'Select entity to define cross section profile of the extrusion');
          lblsett (2, 'Group', l^.pflsel=s_group, 'Select group to define cross section profile of the extrusion');
          lblsett (3, 'Area', l^.pflsel=s_area, 'Select first corner of area to define cross section profile of the extrusion');
          lblsett(6, 'Layer Search', PGsavevar^.srch);
          lblset (8, 'Symbol File', 'Choose a symbol file to define cross section profile of the extrusion');
          lblset (9, 'Circle/Plygn', 'Define a circle or simple polygon by parameters');
          if l^.lastprofile.Items.Count > 0 then begin
            lblset (10, 'Prev Profile');
            if (l^.lastprofile.Items[0].ProfileType = p_symbol) then begin
              lblmsg (10, 'Use ' + l^.lastprofile.Items[0].name)
            end
            else if l^.lastprofile.Items[0].ProfileType = p_entity then begin
              if l^.lastprofile.Items.Count > 1 then
                lblmsg (10, 'Use previously selected entities')
              else
                lblmsg (10, 'Use previously selected entity');
            end
            else if l^.lastprofile.Items[0].ProfileType = p_circle then begin
              cvdisst (l^.lastprofile.Items[0].radius, tempstr1);
              lblmsg (10, tempstr1 + ' radius circle');
            end
            else if l^.lastprofile.Items[0].ProfileType = p_polygon then begin
              cvdisst (l^.lastprofile.Items[0].plyradius, tempstr1);
              lblmsg (10, tempstr1 + ' radius polygon with ' + shortstring(inttostr (l^.lastprofile.Items[0].numsides)) );
            end
            else begin
              cvdisst (l^.lastprofile.Items[0].width, tempstr1);
              cvdisst (l^.lastprofile.Items[0].height, tempstr2);
              lblmsg (10, tempstr1 + ' x ' + tempstr2 + ' rectangle');
            end;
          end;

          ShowStartRotOption;

          lblset (18, 'Advanced', 'Show Advanced Options (Pfl Divisions = ' + shortstring(IntToStr(l^.Divs.PflCrc)) + ')');
          lblset (19, HelpAboutLbl);
          if l^.profile.Items.Count = 0 then
            lblset(20, 'Exit')
          else
            lblset (20, 'Cancel', 'Return to path menu without changing profile');
          lblson;
          wrtmsg ('Select Profile to be extruded');
          getpoint(l^.getp, retval);
        end;
      {$endregion '_Profile'}
      {$region '_Profile2'}
      _Profile2 : begin
            lblsinit;
            lblset(20, 'Cancel');
            lblson;
            wrtmsg ('Select 2nd corner of profile area');
            rubbx^ := true;
            setrubpnt (l^.pnt);
            getpoint(l^.getp, retval);
          end;
      {$endregion '_Profile2'}
      {$region '_ProfileRef'}
      _ProfileRef : begin
           lblsinit;
           lblset(10, 'Middle', 'Set Reference Point to middle of the bounding rectangle');
           lblset (20, 'Cancel', 'Cancel Section Profile Definition');
           lblson;
           wrtmsg('Select Reference Point for Profile');
           getpoint(l^.getp, retval);
         end;
      {$endregion '_ProfileRef'}
      {$region '_Path'}
      _Path : begin
           if l^.tempprofilesCreated then begin
             l^.tempprofiles.Items.Free;
             l^.tempprofilesCreated := false;
           end;
           wrtlvl('Extrude Path');
           lblsinit;
           lblsett (1, 'Entity', l^.pathsel=s_entity, 'Select entity to define extrusion path');
           lblsett (2, 'Group', l^.pathsel=s_group, 'Select group to define extrusion paths');
           lblsett (3, 'Area', l^.pathsel=s_area, 'Select first corner of area containing entities that define extrusion path');
           lblset (4, 'Trace', 'Select points to define extrusion path');
           lblsett(6, 'Layer Search', PGsavevar^.srch);

           lblsett (7, 'Confirm Dlg', l^.ShowOptions, 'Show a confirmation dialog before creating extrusion');

           tempstr1 := 'Currently  extruding at ';
           if Hite in l^.zTreatment then tempstr1 := tempstr1 + 'Z-Hite,';
           if Base in l^.zTreatment then tempstr1 := tempstr1 + 'Z-Base,';
           if Slope in l^.zTreatment then
            tempstr1 := tempstr1 + 'Hite/Base Slope,'
           else if RevSlope in l^.zTreatment then
            tempstr1 := tempstr1 + 'Base/Hite Slope,';
           if zProfile in l^.zTreatment then tempstr1 := tempstr1 + 'specified profile,';
           if verticals in l^.zTreatment then tempstr1 := tempstr1 + 'verticals';
           if (string(tempstr1)).EndsWith(',') then
              setlength (tempstr1, length(tempstr1)-1);
           lblset (9, 'Path Z', tempstr1);

           lblsett (11, 'Cap Ends', l^.Cap, 'Cap Ends');
           lblsett (12, 'Vertical Ends', l^.VerticalEnds, 'Model the ends of sloped extrusions vertically. Applicable to 2D paths only');
           if ((l^.StartEnl = 1) and (l^.EndEnl = 1)) and isnil(l^.EnlProfile) then begin
             enlLblState := 2;
             tempstr1 := '(not set)';
           end
           else
             case l^.enlXY of
               enlNone : begin
                          enlLblState := 11;
                          tempstr1 := '(set, but disabled in both directions)';
                         end;
               enlBoth : begin
                          enlLblState := 1;
                          if not isnil(l^.EnlProfile) then
                            tempstr1 := '(profile selected)'
                          else
                            tempstr1 := '(' + shortstring(FloatToStr(l^.StartEnl)) + ' --> ' +
                                        shortstring(FloatToStr(l^.EndEnl)) + ')';
                         end;
               enlX    : begin
                          enlLblState := 3;
                          tempstr1 := '(set for pfl X-axis only)';
                         end;
               enlY    : begin
                          enlLblState := 3;
                          tempstr1 := '(set for pfl Y-axis only)';
                         end
               else      begin  // should never happen
                           enlLblState := 2;
                           tempstr1 := '';
                         end;

             end;

           lblsettf (13, 'Enlarge', enlLblState,
                        'Specify enlargement applied to section pfl along sweep path ' + tempstr1);

           lblsett (14, 'Twist', (not RealEqual(l^.StartRotation, zero) or (l^.TwistType <> NoTwist)),
                        'Specify Twist/Rotation to be applied to the Section Profile');

            if abs(l^.ExtrThick) > abszero then begin
              cvdisst (l^.ExtrThick, tempstr1);
              if l^.ExtrThick < 0 then
                tempstr1 := '-' + tempstr1;
              tempstr1 := ' (' + tempstr1 + ')';
            end
            else
              tempstr1 := '';
           lblsett (15, 'Thickness', abs(l^.ExtrThick) > abszero, 'Add a thickness' + tempstr1 + ' to the Section Profile.');

           lblset (17, 'Chg Profile', 'Select a new Profile');
           lblset (18, 'Advanced', 'Show Advanced Options');
           lblset (19, HelpAboutLbl);
           lblset(20, 'Exit');
           lblson;
           wrtmsg('Select Extrusion Path');
           setlength (l^.tracepnts, 0);
           setlength (l^.OffsTracePnts, 0);
           if l^.pathsel=s_area then
             getpointp (vmode_orth, false, l^.getp, retval)
           else
             getpointp (vmode_all, false, l^.getp, retval);
         end;
      {$endregion '_Path'}
      {$region '_Path2'}
      _Path2 : begin
            lblsinit;
            lblset(20, 'Cancel');
            lblson;
            wrtmsg ('Select 2nd corner of path area');
            rubbx^ := true;
            setrubpnt (l^.pnt);
            getpoint(l^.getp, retval);
          end;
      {$endregion '_Path2'}
      {$region '_Z'}
      _Z : begin
            wrtlvl ('Path Z');
            wrtmsg ('Select Z options');
            lblsinit;
            lblsett (1, 'At Z-Height', Hite in l^.zTreatment, 'Create extrusion at Z-Height of applicable path entities');
            lblsett (2, 'At Z-Base', Base in l^.zTreatment, 'Create extrusion at Z-Base of applicable path entities');
            lblsett (3, 'Slope Bs/Ht', Slope in l^.zTreatment, 'Extrusion will slope from Z-Base to Z-Height of applicable path entities');
            lblsett (4, 'Slope Ht/Bs', RevSlope in l^.zTreatment, 'Extrusion will slope from Z-Height to Z-Base of applicable path entities');
            lblsett (5, 'Mid Z', zMid in l^.zTreatment, 'Extrusion will be at midpoint between Z-Height and Z-Base of applicable path entities');
            if BasePlus in l^.zTreatment then begin
              cvdisst (l^.ZBasePlus, tempstr1);
              if l^.ZBasePlus < 0 then
                tempstr1 := 'Bs-'+tempstr1
              else
                tempstr1 := 'Bs+'+tempstr1;
              lblsett (6, tempstr1, true, 'Extrusion will be at Z-Base of applicable entities PLUS Specified Distance');
            end
            else
              lblsett (6, 'Base PLUS', BasePlus in l^.zTreatment, 'Extrusion will be at Z-Base of applicable entities PLUS Specified Distance');
    {*        lblsett (8, 'Z Profile', zProfile in l^.zTreatment, 'Specify a z-profile for the extrusion height of applicable entities');   *}
            lblsett (10, 'Verticals', verticals in l^.zTreatment, 'Create vertical extrusions at corners of applicable entities');
            lblset (20, 'Exit');
            lblson;
            getpointp(vmode_all, true, l^.getp, retval);
          end;
      {$endregion '_Z'}
      {$region '_zBasePlus'}
      _zBasePlus : begin
            wrtlvl ('Base PLUS');
            lblsinit;
            lblset (20, 'Exit');
            lblson;
            wrtmsg ('Distance above Z-Base of Path Entities: ');
            callGetDis (0, l^.ZBasePlus, false, l^.getd, retval);
          end;
      {$endregion '_zBasePlus'}
      {$region '_ZProfile'}
      _ZProfile : begin
            wrtlvl ('Z Profile');
            lblsinit;
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Select polyline defining z profile (y value along line will be z value of extrusion)');
            getpoint(l^.getp, retval);
          end;
      {$endregion '_ZProfile'}
      {$region '_PathDivs'}
      _PathDivs  : begin
(*            wrtlvl ('Path Divs');
            wrtmsg ('Enter number of circle divisions for path (6-128)');
            getint (l^.pathdivs, l^.geti, retval);   *)
          end;
      {$endregion '_PathDivs'}
      {$region '_Enl'}
      _Enl : begin
          wrtlvl ('Enlargement');
          wrtmsg ('Select Enlargement Option');
          lblsinit;
          lblset (3, 'Set Both', 'Set Starting and Ending Enlargement to the same value');
          lblsett (7, EnlXLbl, l^.EnlXY in [EnlBoth, EnlX], EnlXHint);
          lblsett (8, EnlYLbl, l^.EnlXY in [EnlBoth, EnlY], EnlYHint);
          lblset (10, 'Reset', 'Reset enlargement to 1');
          if (not isnil(l^.EnlProfile)) and ent_get(ent, l^.EnlProfile) and (ent.enttype = entpln) then begin
            lblsett (1, 'Enlrg Strt', false, 'Disable Enlargement Profile and set Enlargement factor at start of path');
            lblsett (2, 'Enlrg End', false, 'Disable Enlargement Profile and set Enlargement factor at end of path');
            lblsett (5, 'Enlrg Pfl', true, 'An enlargement profile has been specified');
          end
          else begin
            if l^.StartEnl = 1 then
              lblset (1, 'Enlrg Strt', 'Enlargement factor at start of path (' + shortstring(FloatToStr(l^.StartEnl)) + ')' )
            else
              lblsett (1, 'Enlrg Strt', true, 'Enlargement factor at start of path (' + shortstring(FloatToStr(l^.StartEnl)) + ')' );
            if l^.EndEnl = 1 then
              lblset (2, 'Enlrg End', 'Enlargement factor at end of path (' + shortstring(FloatToStr(l^.EndEnl)) + ')' )
            else
              lblsett (2, 'Enlrg End', true, 'Enlargement factor at end of path (' + shortstring(FloatToStr(l^.EndEnl)) + ')' );

            lblsett (5, EnlPflLbl, false, EnlPflHint);
          end;
          lblset (19, HelpAboutLbl);
          lblset (20, 'Exit');
          lblson;
          getpointp (vmode_all, false, l^.getp, retval);
         end;
      {$endregion '_Enl'}
      {$region '_EnlStart, EnlEnd'}
      _EnlStart, _EnlEnd, _EnlBoth : begin
            lblsinit;
            lblset (3, '0.1');
            lblset (4, '0.25');
            lblset (5, '0.5');
            lblset (6, '0.75');
            lblset (7, '1.0');
            lblset (8, '2.0');
            lblset (9, '3.0');
            lblset (10, '4.0');
            lblset (11, '5.0');
            lblset (12, '6.0');
            lblset (13, '7.0');
            lblset (14, '8.0');
            lblset (15, '9.0');
            lblset (16, '10.0');
            lblset (20, 'Cancel');
            lblson;

            if l^.state = _EnlStart then begin
              wrtlvl ('Start Enlarge');
              wrtmsg ('Starting Enlargement Factor:');
              calldgetReal (l^.StartEnl, 6, l^.getr, retval);
            end
            else if l^.state = _EnlBoth then begin
              wrtlvl ('Enlargement');
              wrtmsg ('Enlargement Factor:');
              calldgetReal (l^.StartEnl, 6, l^.getr, retval);
            end
            else begin
              wrtlvl ('End Enlarge');
              wrtmsg ('Ending Enlargement Factor:');
              calldgetReal (l^.EndEnl, 6, l^.getr, retval);
            end;

          end;
      {$endregion '_EnlStart, EnlEnd'}
      {$region '_EnlProfile'}
      _EnlProfile : begin
            WrtLvl ('Enl Profile');
            lblsinit;
            lblsett (7, 'Enlrg X', l^.EnlXY in [EnlBoth, EnlX], 'Enlarge X-axis of section profile');
            lblsett (8, 'Enlrg Y', l^.EnlXY in [EnlBoth, EnlY], 'Enlarge Y-axis of section profile');
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Select polyline to define enlargement profile (relative to x-axis)');
            getpoint(l^.getp, retval);
          end;
      {$endregion '_EnlProfile'}
      {$region '_EnlProfileRef'}
      _EnlProfileRef : begin
            WrtLvl ('Enl Pfl Ref');
            lblsinit;
            lblset (1, 'Pfl Min', 'Set min Y of profile entity as 1');
            lblset (2, 'Pfl Mid', 'Set 1 as midway between min and max Y falues of profile entity');
            lblset (3, 'Pfl Max', 'Set max Y of profile entity as 1');
            lblsett (7, 'Enlrg X', l^.EnlXY in [EnlBoth, EnlX], 'Enlarge X-axis of section profile');
            lblsett (8, 'Enlrg Y', l^.EnlXY in [EnlBoth, EnlY], 'Enlarge Y-axis of section profile');
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Select a point representing enlargement factor of 1 (relative to y=0)');
            getpoint(l^.getp, retval);
          end;
      {$endregion '_EnlProfileRef'}
      {$region '_Twist'}
      _Twist : begin
          wrtlvl ('Twist');
          wrtmsg ('Select Rotation Options');
          lblsinit;
          lblsett (1, 'No Twist', l^.TwistType=NoTwist, 'Do not twist profile');
          lblsett (3, 'Num Twist', l^.TwistType=NumTwist, shortstring('Number of 360̊ twists along path (' + l^.TwistNumTurns.toString() + ')'));
          lblsett (4, 'Twist Dis', l^.TwistType=DistTwist, shortstring('Path distance for a 360̊ Twist (' + DisStr(l^.TwistDistance) + ')'));
          lblsett (5, 'Clkwise', l^.ClkwiseRot, 'Rotate profile in a clockwise direction');
          lblsett (6, 'CntrClk', not l^.ClkwiseRot, 'Rotate profile in a counterclockwise direction');
          lblset (7, 'Twist Divs', 'Twist divisions per complete rotation (' + shortstring(IntToStr(l^.Divs.Twist)) + ')');

          ShowStartRotOption;
          lblset (19, 'Help/About');
          lblset (20, 'Exit');
          lblson;
          getpointp (vmode_all, false, l^.getp, retval);
         end;
      {$endregion '_Twist'}
      {$region '_StartRot'}
      _StartRot, _StartRotPfl : begin
           wrtlvl ('Start Angl');
           wrtmsg ('Starting Profile Rotation: ');
           l^.geta.len := 10;
           l^.geta.rel := false;
           l^.temprl := l^.StartRotation;
           getang (l^.temprl, l^.geta, retval);
         end;
      {$endregion '_StartRot'}
      {$region '_ExtrThick'}
      _ExtrThick : begin
            lblsinit;
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Enter Extrusion thickness: ');
            callGetDis (0, l^.ExtrThick, false, l^.getd, retval);
          end;
      {$endregion '_ExtrThick'}
      {$region '_TwistNum'}
      _TwistNum : begin
            wrtlvl ('Num Twist');
            lblsinit;
            lblset (1, '1/4');
            lblset (2, '1/2');
            lblset (3, '1');
            lblset (4, '2');
            lblset (5, '3');
            lblset (6, '4');
            lblset (7, '5');
            lblset (8, '6');
            lblset (9, '7');
            lblset (10, '8');
            lblset (20, 'Exit');
            lblson;
            wrtmsg ('Num rotations along extrude length: ');
            calldgetReal (l^.TwistNumTurns, 8, l^.getr, retval);
          end;
      {$endregion '_TwistNum'}
      {$region '_TwistDis'}
      _TwistDis : begin
            lblsinit;
            wrtlvl ('Twist Dis');
            lblset (20, 'Exit');
            lblson;
            wrtmsg ('Path Length per Complete Rotation: ');
            callGetDis (0, l^.TwistDistance, false, l^.getd, retval);
          end;
      {$endregion '_TwistDis'}
      {$region '_TwistDivs'}
      _TwistDivs: begin
            wrtlvl ('Twist Divisions');
            lblsinit;
            lblset (20, 'Exit');
            lblson;
            wrtmsg ('Enter Twist divisions per full rotation: (6-128)');
            getint (l^.Divs.Twist, l^.geti, retval);
          end;
      {$endregion '_TwistDivs'}
      {$region '_Trace'}
      _Trace : begin
        wrtlvl ('Trace Path');
        lblsinit;
        lblsett (1, 'Z-Base', l^.traceZtype = trBase, 'Create extrusion a z-base');
        lblsett (2, 'Z-User 1', l^.traceZtype = trUser1, 'Create extrusion a z-User1');
        lblsett (3, 'Z-User 2', l^.traceZtype = trUser2, 'Create extrusion a z-user2');
        lblsett (4, 'Z-Height', l^.traceZtype = trHite, 'Create extrusion a z-height');
        tempstr1 := 'Specify a z for creation of extrusion';
        if l^.traceZtype = trSpecified then begin
          cvdisst (l^.traceZ, tempstr2);
          lblsett (5, 'Z='+tempstr2, true, tempstr1);
        end
        else
          lblset (5, 'Specify Z', tempstr1);
        if (length (l^.tracepnts) >= 2) then begin
          if  realequal (l^.offset, zero) then
            lblset (7, 'Offset', 'Do extrusion at an offset from path that you trace')
          else begin
            cvdisst (abs(l^.offset), tempstr1);
            lblsett (7, 'Off ' + tempstr1, true, 'Currently offsetting by ' + tempstr1);
          end;
          if l^.Fillet then
            cvdisst (l^.FilletRad, tempstr1)
          else
            tempstr1 := '';
          lblsett (8, 'Fillet', l^.Fillet, 'Fillet Corners ' + tempstr1);
        end;
        if l^.TraceClosed = ClosedByPoints then begin
          if length(l^.tracepnts) <= 2 then
            l^.TraceClosed := NotClosed
          else if not PointsEqual (l^.tracepnts[0].pnt, l^.tracepnts[length(l^.tracepnts)-1].pnt) then
            l^.TraceClosed := NotClosed;
        end;

        lblsett (10, 'Closed', not (l^.traceClosed in [NotClosed, ForceNotClosed]), 'Specify if extrusion path is closed or open');
        lblset (14, 'Set Z-User 1', 'Set Z-User 1 height');
        lblset (15, 'Set Z-User 2', 'Set Z-User 2 height');
        if length (l^.tracepnts) < 2 then
          lblset (20, 'Exit', 'Return to Extrude Path menu')
        else begin
          lblset (17, 'Backup', 'Undo the last point entered');
          lblset (18, 'Cancel', 'Return to Extrude Path menu');
          lblset (20, 'Complete', 'Process extrusion for points entered');
        end;
        lblson;
        if length (l^.tracepnts) < 1 then begin
          wrtmsg ('Select first point of trace path');
        end
        else begin
          wrtmsg ('Select next point of trace path');
          if (l^.traceClosed = ClosedByOption) and (length(l^.tracepnts)> 1) then
            drag2Pt (NewPoint(l^.tracepnts[0].pnt), NewPoint(l^.tracepnts[length(l^.tracepnts)-1].pnt), true, linecolor)
          else begin
            rubln^ := true;
            setrubpnt (NewPoint(l^.tracepnts[length(l^.tracepnts)-1].pnt));
          end;
        end;
        getpointp(vmode_orth, false, l^.getp, retval);
      end;
      {$endregion '_Trace'}
      {$region '_OffsetDis'}
      _OffsetDis : begin
            lblsinit;
            wrtlvl ('Offset');
            lblsett (1, 'SetAll', l^.OffsTraceAll, 'Set offset for all existing trace segments');
            lblset (3, 'Key Dis', 'Enter offset distance on keyboard');
            lblset (8, 'Clear', 'Set offset to zero');
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Select point at offset distance from traced line');
            getpoint(l^.getp, retval);
          end;
      {$endregion '_OffsetDis'}
      {$region '_KeyOffsDis'}
      _KeyOffsDis : begin
           wrtlvl ('Offset Dis');
           lblsinit;
           lblsett (1, 'SetAll', l^.OffsTraceAll, 'Set offset for all existing trace segments');
           lblset (3, 'Point', 'Select a point to specify the offset distance');
           lblset (8, 'Clear', 'Set offset to zero');
           lblset (20, 'Cancel');
           lblson;
           wrtmsg ('Offset Distance (+ve to right, -ve to left)');
           l^.TempRl := l^.offset;
           callDGetDis (l^.TempRl, l^.dgetd, retval);
         end;
      {$endregion '_KeyOffsDis'}
      {$region '_FilletRad'}
      _FilletRad : begin
           wrtlvl ('Fillet Radius');
           if RealEqual(l^.FilletRad, zero) then
             l^.TempRl := abs (l^.offset)
           else
            l^.TempRl := l^.FilletRad;
           wrtmsg ('Fillet Radius');
           callGetDis (0, l^.TempRl, true, l^.getd, retval);
         end;
      {$endregion '_FilletRad'}
      {$region '_User1'}
      _User1 : begin
            lblsinit;
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Enter new Z-User 1 value: ');
            callGetDis (0, PGSv^.userz1, false, l^.getd, retval);
          end;
      {$endregion '_User1'}
      {$region '_User2'}
      _User2 : begin
            lblsinit;
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Enter new Z-User 2 value: ');
            callGetDis (0, PGSv^.userz2, false, l^.getd, retval);
          end;
      {$endregion '_User2'}
      {$region '_Trace_Z'}
      _TraceZ : begin
            lblsinit;
            lblset (20, 'Cancel');
            lblson;
            wrtmsg ('Enter z value for extrusion: ');
            callGetDis (0, l^.traceZ, false, l^.getd, retval);
          end;
      {$endregion '_Trace_Z'}
      _esc : begin
            callGlobalesc (l^.getp.key, l^.GlobEsc, retval);
          end
      else retval := XDone;
    end;
  end;

  if l^.PrevState = _NoPrevState then begin
    l^.prevstate := _Profile;
    if GetSvdBool (ExtRegPrompt, true, false, true) then begin
      FmRegPrompt := TRegistationPrompt.Create(nil);
      try
        FmRegPrompt.ShowModal;
        if FmRegPrompt.chbNotAgain.Checked then
          SaveBool(ExtRegPrompt, false, true);
      finally
        FmRegPrompt.Free;
      end;
      CheckHelpFile;
    end;
  end;




  Result := retval;
end;

function Main(dcalstate : asint; act : action; pl, pargs : Pointer) : wantType; stdcall;
begin
  case dcalstate of
    XDcalStateBegin : Result := extrude_main(act, pl, pargs);
    else Result := XDone; { Necessary }
  end;
end;


exports
  Main;

begin

end.

